# The Art of Doing Science and Engineering by Richard W. Hamming

- "Realize, as a general rule, it is not the same job you should do with a machine, but rather an equivalent one"
    - "Machine" could be interchanged with any product, service, or business and maps to the idea of having a 10x improvement in at least one dimension such that a solution is far superior (in at least one area) compared to present solutions
- Maintenance is always to be considered in a design (be it physical or digital)
- Remember, computers manipulate symbols, not information
- In n-dimensional space "the volume is almost all on the surface... This had importance in design; it means almost surely the optimal design will be in the surface and not inside."
- The abstraction of details is what gives breadth of application
- "Get down to the basics every time!"
    - Regarding fourier transforms in digital filtering as example of first principles thinking
    - Often young scientists, mathematicians, and engineers (everyone really) consumes what they're told and rarely questions it. For this reason there is still much to discover and improve as we continually learn that the "status quo" is lacking.
- When something is claimed to be "new", it may be a great opportunity to make significant contributions
- To the extent you can choose, work on problems you think will be important
- "I strongly advise, when possible, to start working the simple simulation and evolve it to a more complete, more accurate, simulation later so the insights can arise early"
    - This maps to working in small incremental steps such that the learning cycle time is as short as possible (something Sid deeply believed in and professed that additionally maps to the Lean Startup learn loop)
- Be weary when providing simulations upon others' request as you may be contributing to propaganda
- "What you learn from others you can use to follow; what you learn for yourself you can use to lead"
- Lab equipment often needs "fine tuning" to produce consistent results. In doing so the tester is adjusting for low variance which the statistician plugs for high reliability numbers. This is common lab practice! No wonder data is seldom as accurate as claimed.
- When data points or decisions are made based on averages, be very weary as averages are only meaninguful in homogenous groups (with respect to outcomes, effects, or downstream actions)
- Regarding units of work, importance of it, not the volume of it, is often what truly matters in the long run
- Rules of system engineering:
    1. If you optimize the components, you wil probably ruin the system performance
    2. Part of systems engineering design is to prepare for changes that can be gracefully made and still not degrade the other parts
    3. The closer you meet specifications the worse performance will be when overloaded
        - This intentionally or not maps to Sid's reinforcement at GitLab to KISS and doing work in small incremental steps. The example in the book is "the truth of this is obvious when building a bridge to carry a certain load; the slicker the design to meet the prescribed load, the sooner the collapse of the bridge when the load is exceeded"
- People are clever and will solve for the task at hand (local optimization). Be very weary of setting objectives that you think are chunking/helping when in fact others may interpret this differently
