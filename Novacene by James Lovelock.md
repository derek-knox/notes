# Novacene by James Lovelock

- *Gaia theory* - proposes that living organisms interact with their inorganic surroundings on Earth to form a synergistic and self-regulating, complex system that helps to maintain and perpetuate the conditions for life on the planet
    - Many think of inorganic (man-made creations) as unnatural, but they too are byproducts of nature and are thus natural (though admittedly a different class)
- James' focus on photosynthesis as a precursor step for more advanced evolutionary products is the first time I could really see humanity as a similar stepping stone "technology" to successors.
    - My instinct (that was also touched on in the book) that any electronic life (cyborgs) would still be dependent on us (or at least organic life in the long term) still seems true. For this reason a coexistence seems most probable. Potentially in similar relationships we have with plants and pets.
- Thomas Newcomen invented the steam-powered pump ("atmospheric steam engine") and James identifies this invention as the catalyst of the Industrial Revolution and birth of the Anthropocene. 
- A cyborg intelligence *could* be 1M times quicker at information processing/transmission
    - This is due to the 30cm/nanosec signal travel time over an electric conductor (copper wire) compared to 30cm/millisec for nervous conduction along a neuron
    - James argues this 1M figure is unrealistic where 10K is more probable (the same delta between us and plants)
