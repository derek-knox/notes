# Big O Notation
Big O notation is used to classify algorithms according to how their running time or space requirements grow as the input size grows.

| Type              | Order of...      | Example  |
| ---               | ---              | --- |
| Constant          | O(1)             | direct access by index |
| Logarithmic       | O(log n)         | binary search, heap sort |
| Linear            | O(n)             | for loop |
| Log Linear        | O(n log n)       | merge sort, binary insertion sort |
| Quadratic         | O(n^2)           | double for loop, insertion sort |
| Cubic             | O(n^3)           | triple for loop, Nussinov RNA folding |
| Exponential       | O(c^n)           | ... |
| Factorial         | O(n!)            | ... |
