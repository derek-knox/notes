# Hooked - How to Build Habit-forming Products by Nir Eyal

## Intro

- Habits are defined as "behaviors done with little or no conscious thought"
- Businesses that create customer habits gain a significant competitive advantage
- *The Hook Model* describes an experience designed to connect the user's problem to a solution frequently enough to form a habit. It has four phases:
    1. Trigger - actuator of behavior
        - When users start to automatically queue their next behavior, the new habit becomes part of their *routine*
    2. Action - behavior done in anticipation of a reward
        - Two aspects increase likelihood of an action occurring:
            1. Ease of performing an action
            2. Psychological motivation to do it
    3. Variable reward - variability as opposed to a simple feedback loop
        - This births intrigue and desire
    4. Investment - implies an action improves the product/service for future use
        - Self-controlled impact on their own future UX

## The Habit Zone

- "A habit is when not doing an action causes a bit of pain" - aka scratch-the-itch desires
    - So a good habit forming product provides *relief*, often the quicker and easier, the better
- Frequency and perceived utility enable habits to form
- Designing habit-forming products is a form of manipulation
    - Ensure you are building healthy habits, not unhealthy addictions

## Trigger

- To assist in forming habits, identify customer frustrations or pain points in emotional terms rather than product features
- To understand the best trigger(s) for a product/service a clear description of users—their desires, emotions, context in which product is used, etc—is paramount to building the right solution. One way to do this is to ask "why?" as many times as it takes to get to an *emotion* (often the 5th-ish)
- Triggers queue the user to take action and they come in two types:
    1. External - place information in users' environment (encouraging, suggesting, and/or implying action to be taken)
    2. Internal - inform user what to do next based on associations stored in memory (negative emotions frequently serve as internal triggers)
- To build habit-forming products/services you must understand which user emotions are tied to internal triggers and know how to leverage external triggers to drive user action

## Action

- To initiate action, *doing* must be easier than *thinking*
- `B = MAT` - behavior = motivation > ability > trigger
    - Behavior occurs when MAT are present simultaneously and in sufficient degrees
- Motivation - "the energy for action" is driven by three core motivations. All humans seek:
    1. Pleasure and avoid pain
    2. Hope and avoid fear
    3. Social acceptance and avoid rejection
- Innovation can be boiled down into three simple steps:
    1. Understand the reason(s) people use a product/service
    2. Layout steps customers take to get the job done
    3. Once task intention to outcome is understood, simply start removing (or refactoring) steps until the simplest process remains
- There are six "Elements of Simplicity"
    1. Time - how long to complete an action
    2. Money - fiscal cost of taking an action
    3. Physical effort - amount of labor in taking action
    4. Brain cycles - level of mental effort/focus required to take action
    5. Social deviance - acceptance level of behavior by others
    6. Non-routine - matches vs. disrupts existing routines
- "How can technology facilitate the simplest actions in anticipation of reward?"
- There are four core heuristics that can influence a user's actions (cognitive shortcuts):
    1. Scarcity Effect - appearance of scarcity affects perception of value. Going from scarce to abundant decreases perceived value where abundant to scarce increase perceived value
    2. Framing Effect - context shapes perception
    3. Anchoring Effect - a single (small amount) of information is used to make a decision when often times more information reveals a different truth
    4. Endowed Progress Effect - a phenomenon where motivation increases as the perception of reaching a goal nears
- Steps to increase desired behavior:
    1. Ensure clear trigger is present
    2. Increase ability by making action easier to do
    3. Align with the right motivator

## Variable Reward

- Three types:
    1. The Tribe - social reward driven by connectedness with people (tied to core motivator where humans seek social acceptance and avoid rejection)
        - We seek rewards making us *feel* accepted, attractive, important, and included
    2. The Hunt - pursuit of resources and information compel us to persevere through the pursuit itself
        - In gambling, when winning is entirely out of our control, the prospect of a jackpot can entice a prolonged pursuit
    3. The Self - personal gratification, often a pursuit to completion to validate competency
        - We withstand pain intentionally at times in an effort to gain competency. Elements of mystery can make a pursuit more enticing. This concept is the core reason of the popularity of video games
- By understanding what truly matters to users, you can properly determine the right variable reward(s) to their intended behavior. Rewards must fit into the narrative of *why* the product/service is used and align with the user's internal triggers and motivation(s)
- "But you are free to accept or refuse" added to the end of a proposition reaffirms our ability to choose but increases likelihood of persuasion. The "but you are free" portion disarms our instinctive rejection of being told what to do.
- Experiments with finite variability become less engaging because they eventually become predictable. Thus it's advantageous to provide elements of mystery
- Maintaining a sense of autonomy is a requirement for repeat engagement

## Investment

- Concerns the anticipation of rewards in the future as opposed to immediate gratification delivered in the action phase
- Investment in a product creates preferences because our tendency to:
    1. *Overvalue* our work (sunk cost fallacy can surface here too)
    2. Be consistent w/past behaviors
    3. Avoid *cognitive dissonance* (the irrational manipulation of our own perception to justify why we can't attain something)
- Investment must come *after* the variable reward phase and clearly communicate future value in the product/service
- The accrual of stored value (content, data, followers, reputation, skill, etc.) increases likelihood of return
    - Load the next trigger to increase likelihood of returned use just after an investment (implant future value in mind as to achieve an internal trigger relationship)

## Manipulation Morality

- Manipulator types fall into four categories:
    1. Facilitators - use their own product and believe it can materially improve peoples' lives
    2. Peddlers - don't use their product but believe it can materially improve peoples' lives
    3. Entertainers - use their product but do not believe it improves peoples' lives (often lack staying power)
    4. Dealers - don't use thier product and don't believe it can improve peoples' lives
- All four can be successful, though being a facilitator is ideal

## Habit Testing

- *The Hook Model* aids a product designer in an intitial prototype for a habit-forming technology
- *Habit Testing* includes three steps:
    1. Identify - dig into data to determine how users car using your product/service
    2. Codify - study actions and paths taken by devoted users to better understand habitual users
    3. Modify - change product to influence more users to follow same actions/paths as habitual users
- Identifying areas where new tech makes cycling through the hook model faster, more frequent, and/or more rewarding provides fertile ground for developing new habit-forming products/services
- New interfaces lead to transformative behavior change and business opportunity (both software and physical interfaces) as they can massively impact the *ease of taking an action*
