# The 22 Immutable Laws of Marketing by Al Ries and Jack Trout

## Law of Leadership

- It's better to be first than it is to be better
- The leading brand in any category is almost always the first brand into the prospect's mind
- A main reason the first brand maintains its leadership position is that the name often becomes generic
- Marketing is a battle of perception, not products

## Law of Category

- If you can't be first in a category, set up a category you can be first in
- Don't ask yourself "how is this product better than the competition?" ask "what category is this new product first in?"
- Everyone is interested in what's new. Few are interested in what's better.
- When you're first in a category, *promote the category*. Prospects need to buy into the category and since you're first in it, they'll naturally pick your product

## Law of the Mind

- It's better to be first in the *mind* than to be first in the *marketplace*
- Being first in the marketplace is only important to get you first in the mind
- The single most wasteful thing you can do in marketing is try to *change the mind*

## Law of Perception

- Marketing is not a battle of products, but a battle of perception
- Once a perception is formed, whether accurate or not, it is almost impossible to change it

## Law of Focus

- The most powerful concept in marketing is *owning a word* in the prospect's mind
- It's always better to focus on one word or benefit rather than multiple
- The one word often has a halo effect where other words are implied automatically by prospects
- Most successful brand/companies are the ones that *own a word* in the prospect's mind
- The essence of marketing is narrowing focus. you become stronger when you reduce the scope of your operations.
- You an't narrow the focus with a word/idea that doesn't have proponents for the opposite point of view (ex. "quality", no one is going to promote them selves as "unquality")

## Law of Exclusivity

- Two companies can't own the same word
- You can't change peoples' minds once they're made up, in fact you can often reinforce a competitors position when attempting to

## Law of the Ladder

- The strategy you use depends on which rung you occupy on the ladder
- For each category, there is a product ladder in the prospect's mind so respect your position and let what differentiates you help propel you
- *Before* starting any marketing campaign ask, "Where are we on the ladder in the prospect's mind?". Then deal realistically with your ladder position

## Law of Duality

- In the long run, every market becomes a two-horse race
- Customers believe that marketing is a battle of products, they think, "they must be the best, they're the leaders". This again is why it's important to be the leader, to create your own category, and to hold that top position in the mind.

## Law of the Opposite

- If you're shooting for second place, your strategy is determined by the leader
- If you want to hold a firm position of second on the ladder, study the firm above you. Where is it strong? How do you turn that strength into a weakness?
- Discover the *essence* of the leader and present prospects with the *opposite* (be different by design)
- By positioning yourself against the leader, you take business away from all the alternatives to number one
- Be the alternative to the leader

## Law of Division

- Over time, a category will divide and become two or more categories
- It's better to be early than late as you can't get into the prospect's mind first unless you're prepared to spend time waiting for things to develop

## Law of Perspective

- Marketing effects take place over an extended period of time
- Short term gains can become long term losses. Be wary of sales, coupons, etc. and what this communicates to prospects

## Law of Line Extension

- There's an irresistible pressure to extend the equity of a brand, but remember what the brand already means to prospects, it may not be worth it
- This is the single most broken rule that results in a company's downfall in market share
- Invariably, the leader in any category is the brand that is not line extended (ties back to Law of Category and the Law of Focus)
- Less is more, you have to narrow your focus to build a position in a prospects mind

## Law of Sacrifice

- You have to give up something in order to get something
- There are three things that can be sacrificed:
    1. Product line - be a specialist not a generalist
    2. Target market - be specific, don't target everyone
    3. Constant change - have a position and stick with it, good things come to those that sacrifice

## Law of Attributes

- For every attribute, there is an opposite, effective attribute
- If not the leader, you need to find an opposite attribute for prospects to associate with your product

## Law of Candor

- When you admit a negative, the prospect will give you a postive
- Every negative statement you make about yourself is accepted as truth. You have to *prove* a positive statement to a prospect's satisfaction, *no proof is needed for a negative statement that's twisted to a positive*

## Law of Singularity

- In each situation, only one more will produce substantial results
- History teaches that the only thing that works in marketing is the single, bold stroke

## Law of Unpredictability

- Unless you write your competitor's plans, you can't predict the future

## Law of Success

- Success often leads to arrogance and arrogance to failure
- Don't get blinded by success, it's important to get real opinions from users, not telephoned information from research

## Law of Failure

- Failure is to be expected and accepted
- Recognize failure and cut your losses
- An idea gets rejected not because it isn't sound, but because no one in top management will personally benefit from its success

## Law of Hype

- The situation often is opposite of what it appears in the press
- When you need the hype it usually means you're in trouble

## Law of Acceleration

- Successful programs are not built on fads, they're built on trends
- The most successful entertainers are the ones who control their appearances. they don't over extend themselves, they're not all over the place, they don't wear out their welcome
- To maintain a long-term demand for your product, never totally satisfy the demand

## Law of Resources

- Without adequate funding, an idea won't get off the ground
- You have to use your idea to find the money
- First get the idea and second get the money to exploit it

## Formula (summary)

1. It's better to be ***first*** than it is to be ***better***
2. Don't ask "how is this product ***better*** than the competition?", but instead, "what category is this product ***first*** in?"
3. When first in a category, ***promote the category***. Prospects need to buy into the category and since you're first they'll be naturally drawn to your solution
4. The most powerful concept in marketing is owning a single ***word*** in the prospect's mind
5. Narrow focus, reduce scope, don't fall into the "everybody" trap
6. Before starting any marketing campaign ask "where are we on the ladder in the prospect's mind?". Then realistically deal with your position.
7. If not the leader, discover the **essence** of the leader and present the prospect with the **opposite** (be **different by design**)
8. Failure is to be expected and accepted, recognize it and know when to cut your losses
9. To maintain long-term demand for your product, never totally satisfy the demand
10. First get the idea and second get the money to exploit it
