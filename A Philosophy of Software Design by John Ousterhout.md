# A Philosophy of Software Design by John Ousterhout

## The Nature of Complexity

- *complexity* - anything related to system structure that makes it more difficult to:
    1. understand
    2. modify
- "complexity is what a developer experiences at a particular point in time when trying to achieve a particular goal"
- symptoms of complexity:
    1. *change amplification* - the amount of file, class, module, and signature changes required for a system modification
        - combatted via SRP and DRY
    2. *cognitive load* - the amount of information a developer must consider to implement a bug-free modification
        - combatted via good abstractions, deep modules, and information hiding
    3. *unknown unknowns* - hidden (non-obvious) information that should instead be visible (obvious)
        - combatted via proper information hiding/visibility and obviousness
            - a dev should be able to intuit and guess the APIs and their call sequence for a particular result and have high confidence that their guess is correct
- causes of complexity:
    1. *dependencies* - when a piece of code cannot be understood and modified in isolation
        - manifests as change amplification and cognitive load
    2. *obscurity* - important information is not obvious
        - manifests as cognitive load and unknown unknowns
- complexity is incremental and the only way to properly combat it is for **every contributor to adopt a zero-tolerance philosophy**
    - every "it's OK for now" and PR approval where I share the proper solution but let slide for "simplicity for now" and "less file changes" is increased complexity in the long run
        - typically "the more right" solution just requires a new file and at least one method instead of letting the one method be in a "less right" place

## Working Code Isn't Enough (Tactical vs. Strategic Programming)

- There are two core modes of programming:
    1. *tactical* - make it *work*
        - typically less experienced engineers not knowing how and/or not taking the time to do the right and better steps
            - the lack of experience means they simply are unaware of superior designs (existing patterns, APIs, and proper abstractions)
    2. *strategic* - make it *right* and *better*
        - typically more experienced engineers whose "make it work" already aligns with the right and better
            - the improvement iterations for the solution at hand have been previously paid by past experience and knowledge thus shortcutting to better designs
- "the most effective approach is one where every engineer makes continous small improvements in good design"
    - there is always another crunch and a crunch after that so letting tactical work hit prod without explicitly setting up the time (or having the correct culture of engineers) to improve it leads to a more complex system
    - code review processes and approvals should be robust as this is the single best line of defense against incremental system complexity

## Modules Should Be Deep

- *module* - a class, object, container, or encapsulation adhering to SRP
    1. *deep module* - one whose interface is simple relative to the functionality it provides
        1. simple to grok interface
            - cost: low cognitive load
        2. powerful functionality
            - benefit: high understanding and system modification potential
        3. correct defaults (optimized for the most common use cases)
            - formal: signature names, their payloads, and their return values
            - informal: characteristics relating to asynchronicity, immutability, and performance
    2. *shallow module* - one whose interface is complicated relative to the functionality it provides
        1. difficult to grok interface
        2. minimal functionality
        3. incorrect defaults (unoptimized for the most common use cases)
- a module is composed of two core parts:
    1. *interface* - the module's "what" information (module consumer DOES need to know)
        1. formal - explicit manifestations (a method's signature, a class's API, and inheritance hierarchies)
        2. informal - implicit manifestations (side effects and designer-intended call sequence(s))
    2. *implementation* - the module's "how" information (module consumer DOES NOT need to know)
- *abstraction* - a simplified model/view which intentionally omits information
    - poor
        - *information leakage* - includes unimportant information (leakage)
        - *false abstraction* - omits important information (obscurity)
    - good
        1. *information hiding* - excludes unimportant information (hiding)
            - "can often be improved by making a class slightly larger"
                - I think a better alternative (depending on the size increase) would be to explicitly only slightly increase the class's public API while still delegating the implementation to another class (be it an instance or static) 
        2. *true abstraction* - includes important information while using designs minimizing what is important

## Define Errors out of Existence

- Exceptions and special cases make code more complex so take special care before implementing exceptions
- Approaches to leverage to remove (or limit) exception cases:
    1. `return` early - simply return nothing instead of throwing an exception
    2. delay - official execution (deletion w/file lock difference between Windows and Unix) 
    3. forgiving APIs - automatically provide the next best option, infer alternative, and/or implement retries that the consumer of the API doesn't need to know about
    4. exception aggregation - when exception handling is required try to aggregate the handling
    5. crash - (last resort) but ensure diagnostics are recorded to aid bug fixing to prevent the issue resurfacing
