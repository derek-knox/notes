# Learning JavaScript Design Patterns by Addy Osmani

## Anti-patterns

- Anti-patterns describe a *bad*/*poor*/*subpar* solution to a problem that has an objectively better solution
    - Anti-patterns awareness (code smells) allows developers to recognize that their is likely a design pattern (or patterns) that could be implemented as the objectively better solution
- Examples in JavaScript include:
    - Polluting the global namespace
    - Passing strings rather than functions to `setTimeout`/`setInterval` (triggers `eval` use)
    - Modifying default prototypes
    - `document.write` when native DOM alternatives are available

## Design Pattern Categories

1. Behavioral - facilitate object *communication*
    - Iterator, Mediator, Observer, Visitor
2. Creational - facilitate object *creation*
    - Abstract, Builder, Constructor, Factory, Prototype, Singleton
3. Structural - facilitate object *composition* and relationships
    - Adapter, Decorator, Facade, Flyweight, Proxy

## Behavioral Patterns

- Command
    - Actions (verbs and thus functions) encapsulated instead as objects such that managers and other higher level code can invoke said actions more flexibly (via queues, undo/redo, action-swapping, etc.)
- Mediator
    - A unified interface for disparate object to facilitate communication in a loosely coupled way when compared to each having and managing communication manually with every other object of interest
- Observer (pub-sub)
    - A subject (object) manages a list of observers (objects) such that the observers can automatically be notified of state changes

## Creational Patterns

- Constructor
    - Used to create specific types of objects where all properties of the object's prototype are made available
- Factory
    - An object with a generic API designed for creating specific (concrete) types of objects based on a configuration payload (via id, string, or config object)    
- Module
    - When a function returns an object literal that provides an API
- Prototype
    - A base definition (template) that is the foundation for more unique and concrete object types    
- Revealing Module
    - A variation on the Module pattern where the API's values references scoped (often by different names) variables and functions
- Singleton
    - A pattern for enforcing a single instance encapsulating and centralizing a set of behaviors

## Structural Patterns

- Decorator
    - An approach to augment and/or create a new object that leverages existing behavior of one or more other objects
- Facade
    - An API that standardizes behavior (abstraction) through encapsulating implementation variation (conditional execution) against the underlying environment and/or system
- Flyweight
    - An approach for sharing base model data and/or base functionality without duplicating it in other objects
- Mixin
    - An approach for augmenting an object's behavior by "mixing" in the behavior of another object

## JavaScript MV* Patterns

- MVC (Model, View, Controller)
    - An architecture enforcing the isolation of business data (models as *flyweights*) from user interfaces (views) using a *mediator* (controller) to manage business logic (of the models) and user input (of the views)
- MVP (Model, View, Presenter)
    - An architecture identical to MVC except that the controller is replaced by a presenter such that a combination of patterns facilitates communication between the models and views (via *prototype*, *observer*, and *mediator*) 
- MVVM (Model, View, View, Model)
    - An architecture where model subsets (aka ViewModels) contain subsets of state and logic such that views aren't concerned with entire models
