# Brilliant - Computational Biology

## Biological Numeracy

An *atom* is a particular combination of neutron, proton, and electron count where each atom type is referred to as an *element* in the periodic table of elements. Multiple atoms of similar or dissimilar type bind to form certain molecules. A particular set of molecules comprise the blueprint for life as we know it manifesting as *DNA*.

*DNA* is simply a string sequence of a base four molecule language whose letters are A (adenine), T (thymine), C (cytosine), and G (guanine). Each species shares common substrings where certain sections vary lending to the differentiation within a particular species. Additionally, each species has different string lengths and common substring variations which in aggregate are its *genome*. Genomes are compared giving rise to an accurate tree of life and evolutionary hierarchy.

## Order and Information

A DNA string can further be split into three-sequence substrings via *transcription* which later can map to 20 amino acids via *translation*. Certain amino acid chains have different functions in the form of proteins. A *gene* represents a DNA substring that maps (via transcription then translation) to a particular function or trait.

Transcription is the process where DNA maps to RNA via the A<=>T and C<=>G pairings. RNA is simply a cloned/transcribed DNA string whose letters are instead its paired letter (with the caveat that *t*hymine is replaced with *u*racil). The DNA double helix is composed of a *coding strand* and a *template strand*. *RNA polymerase* is the enzyme that executes the transcription function of the template strand such that the resulting RNA matches the coding strand (again with *u* replacing *t*). A particular nucleotide sequence (often `TATAAAA`) acts as a header such that the RNA polymerase knows where to land/connect before transcribing. The direction of transcription is known as the *sense* order (`5' -> 3'` : sense/coding strand :: `3' -> 5'` : antisense/non-coding strand)

The single stranded RNA result is later *translated* into an amino acid chain via the aforementioned three-sequence substring amino acid mapping. The *ribosome* is the macromolecular machine that executes this translation function. In order to work properly a few of the twenty nucleotide-to-amino-acid triplet mappings (known as *codons*) encode for "start" and "stop" points. This ensures the ribosome can precisely translate a substring of the RNA to produce specific products (most often proteins).

This amino acid chain manifests in four progressive structures toward becoming a protein:
1. primary - amino acid chain (unbound chain)
2. secondary - initial binding/folding (`beta sheets : zig zag :: alpha helices : helix`)
3. tertiatry - intertwined binding/folding (`native fold : ideal fold :: non-native fold : subpar fold`)
4. quaternary - multi-protein binding/folding

Different proteins have different functions for living organisms and there are seven groups of them:
1. antibodies
2. contractile proteins
3. enzymes
4. hormonal proteins
5. structural proteins
6. storage proteins
7. transport proteins

### Evolution

An individual cell or organism (denoted by its genome) has genetic mutations. If such a mutation benefits reproduction in any way then it is an *adaptation* that improves its *fitness*. If adaptations in aggregate of a population persist, then the genome of the population is said to have *evolved*. So evolution is the process of improving the fitness distribution within a population over time. This genomic population has a maximum known as the *carrying capacity* (which will naturally differ depending on environmental constraints).

Due to environmental change being the only constant, it's said that all living cells and organisms are in a constant struggle dubbed the *Red Queen hypothesis*.

Evolutionary population changes over long periods of time can be visualized as *phylogenetic tree*s to showcase evolutionary relationships of organisms.

## Genomics

The DNA -> RNA -> Protein flow results in *phenotype*s, specific characteristics and traits that manifest from protein interaction. The *genotype* refers to the gene(s) that influence a phenotype.

A common DNA sequencing process is called *polymerase chain reaction* (PCR) and it refers to the heating (so a DNA helix splits) and cooling (so DNA polymerase creates complementary strands for each split strand) of DNA such that mass DNA replication occurs. This process can be further tweaked via the *Sanger sequencing* method that additionally dopes strands with tagged nucleotides resulting in truncated strands instead. These truncated strands can then be sorted via the *electrophoresis* process to determine the nucleotide order. With this order we can thus sequence a specific individual's DNA.

Naturally, once we've sequenced a particular individual's DNA, we can compare it against other DNA. Comparing against specific single nucleotide polymorphisms (SNPs) is called *genotyping* and the process is heavily used in forensics and ancestory.

## Molecular Folding

RNA structures interact with and control the assembly of DNA and proteins leading to the *RNA world* theory. It suggests that RNA predates today's life which is based on DNA, RNA, and proteins. Due to RNAs functional breadth, a prefix is often used to denote a specific type:
- *mRNA* (messenger) maps to a particular gene whose ephemeral presence acts as a short-lived signal for the production of a specific protein
- *tRNA* (transfer) the anticodon mapping of an amino acid for an mRNA codon
- *rRNA* (ribosomal) a primary component of ribosomes thus enabling the translation of mRNA and tRNA as an amino acid (peptide) chain

Lengthy RNA naturally results in an increase in palindromic binding potentials of which the optimal is the *native fold*. This native structure is optimal due to the minimization of chemical energy manifesting as the least wasteful atomic binding patterns in aggregate. Binding results in *stems* (bindings) and *hairpin loops* (non-bindings) where the latter act as potential binding sites for other molecules to form macromolecules.

Predicting or calculating RNA native folds is very difficult and computationally expensive with a brute force approach. Nussinov's recursive algorithm turned an otherwise combinatorial solution space to cubic, enabling a much faster calculation. The algorithm independently scores nested subsequences in search of maximizing the number of RNA base pair bindings. This approach is further improved with the probabilities of *Watson-Crick base pairs* (common) against *wobble base pairs* (uncommon/rare). Combined, this enables powerfully accurate computational folding predictions. The probabilities result from *compensatory mutations* which "fix" mutations due to the selection pressure of maintaining a functional structure. Not all mutations are fixed however which leads to the wobble pairs.
