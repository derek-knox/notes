# Mycelium Running by Paul Stamets

## Mycelium as Nature's Internet

- *mycelium* are the thread-like single-cell thick branching filaments of fungi
    - *hyphae* as singular filament strand where mycelium is the aggregate structure
    - *rhizomorph* - a thick stringlike strand of mycelium from which smaller strands emerge
- Fungi outnumber plants ~6+:1
- Fungi DNA is closer to animals than plants
 
 ## The Mushroom Lifecycle

 - Spores -> germinate -> hyphae (threads) -> mycelium mat (convergence of disperate hyphae) -> cells aggregate into *primordium* (baby mushrooms) -> mushrooms fruit/grow
    - In ideal conditions this process can occur in a few days
- Mushrooms are either:
    1. predeterminant - gills, stem, and cap preform in the premordial state
        - Majority
        - Deformities at this stage are reflected in adulthood
    2. indeterminant - envelop sticks and twigs during growth and can correct deformities
        - Minority
        - Deformities can be overcome
- *basidia* - clubshaped spore emitting structure of fungi
- *polypore* - large fruit body mushrooms that expel spores via fine pores on their underside

## Mushrooms in their Natural Habitats

- Mushrooms are categorized in four ways based on their nourishment strategy:
    1. Saprophytic - grow from decomposition of already dead material
        - Both organic (plant and animal) and inorganic (rock)
    2. Parasitic - grow on dying or injured material
    3. Mycorrhizal - grow at tree bases and can interconnect tree species via their mycylial mat thus providing bidirectional nutrient transmission
        - Trees increase their range and rate of capturing nutrients and the mushrooms recieve plant-based sugars
    4. Endophytic - grow between living plant cells
- As a synergistic system to plants, fungi as:
    - saprophytes decompose material into nutrients manifesting as soils and nutrients for the habitat's lifeforms
    - mycorrhizae supercharge and interconnect root systems of like and unlike tree species
    - endophytes repair blights and protect against infections
- In Oregon, there is a parasitic mycelial mat of honey mushrooms covering 2400 acres that's ~2200yrs old

## The Medicinal Mushroom Forest

- Various mushrooms have many powerful properties:
    - Lion's Mane promotes neurogenesis
    - Reishi has antibacterial, anti-inflamatory, antioxident, antitumor, and antiviral properties among others 

## Mycofilteration

- *mycofilteration* - is the intentional introduction of fungi to a habitat to promote a desired outcome based on our knowledge of differences between the categories
    - Saprophytes for promoting decomposition to enrich soils and mycorrhizae for limiting erosion are two examples

## Mycoforestry

- *mycoforestry* - is the use of fungi to sustain forest communities
- Adding spores to the oils of logging equipment (chainsaws and chippers) can more quickly and directly promote fungal growth as part of a more sustainable restoration strategy when logging

## Mycoremediation

- *mycoremediation* - is the use of fungi to remove or degrade toxins from the environment
- Certain mushrooms can absorb and/or breakdown toxins 10-10,000x bacteria and other organic material
- Heavy metals from industry waste and radioactive materials are prime candidates for mushrooms as mycoremediators

## Mycopesticides

- *mycopesticides* - is the use of fungi as pesticides
- cordyceps are dimorphic as they can express in two forms:
    1. mold
    2. mushroom

## Inoculation Methods

- *substrates* - are the source material that spores and/or mycelium are inoculated with
    - wood chips, sawdust, straw, wood plugs, logs, etc. are ideal substrates
- *spore slurries* - elder mushrooms and water mixes that mimic natural rain spore dispersals
- *spore print* - a process for capturing a mushroom's spore emission "fingerprint"
    1. Collect mushroom
    2. Separate stem from cap
    3. Place mushroom (spore-side down, thus cap side up) on paper (white paper if dark-gilled, colored if white-gilled)
        - Glass panes are great print targets too
    4. Leave for 12+ hours
    5. Spore print (this process can repeat up to ~6x before the mushroom dries out)
- Logs can be drilled into creating "caves" where plug spawn can be inoculated
