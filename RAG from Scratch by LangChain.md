# [RAG from Scratch by LangChain](https://www.youtube.com/watch?v=wd7TZ4w1mSw&list=PLfaIDFEXuae2LXbO1_PKyVJiQ23ZztA0x)

## Summary

Retrieval augmented generation (RAG) is a pipeline for improving LLM model runtime inference performance. RAG exists because a model's training set will always lack data (private, current, etc.). RAG adds data by acting as _dynamic short term memory_ where an LLM model's weights act as _static long term memory_ over the training set. RAG works by dynamically _augmenting_ an LLM context window with dynamically _retrieved_ documents. These documents are both complete and partial while persisted and embedded in various flavors of database (relational, graph, vector, etc.).

## Overview

- RAG surfaced as a strategy to improve model runtime inference performance as a direct result of model context window size increasing
- RAG manifests as three core steps between a user prompt and an LLM's response
    1. *indexing* - full documents (raw contents) or partial documents (chunked contents) stored in databases (preprocessing step)
    2. *retrieval* - query generation to attain relevant documents from databases (runtime step)
    3. *generation* - original user prompt augmented with retrieval (runtime step)
- RAG implementation pipeline consists of six specific steps:
    1. *query translation* - user prompt processing aiding downstream steps
    2. *routing* - shepherding user prompt processing to correct source(s)
    3. *query construction* - query generation based on shepherded source(s)
    4. *indexing* - preprocessing step enabling constructed queries database matches
    5. *retrieval* - ranking and filtering of indexed document(s)
    6. *generation* - LLM context window updated from retrieval

## Query Translation

The intuition for query translation is translating a user prompt—typically by creating variants—in an effort to improve retrieval robustness.

All the approaches below use an embedding model to embed the prompt variants at runtime against the document embeddings created at build time. The exception is HyDE that instead does a runtime hypothetical document embedding under the premise that document-to-document embeddings should perform better than prompt-to-document embeddings due to embedding likeness.

- *multi-query* - conversion of user prompt to multiple versions of prompt where their retrivals are `union`ed 
- *RAG fusion* - same as multi-query except the retrievals pass through `reciprocalRankFusion` instead of `union`
- *decomposition* - decompose user prompt into sub prompts and sequentially interleave each generation as context for next sequential generation
    - *sequential solve* - increases relevancy via sequence
    - *Interleaved Chain of Thought (IR-CoT)* - increases relevancy via interleaving sequence result into next sequence generation
- *step-back* - conversion of user prompt—typically via few shot—to variants reflecting increased perspectives and/or abstractness
- *HyDE* - creation of hypothetical documents to use document-to-document retrieval over prompt-to-document retrieval

## Routing

The intuition for routing is shepherding the query translation result to the correct source (LLM model, relational vs. graph vs. vector database, etc.)

- *logical* - give LLM knowledge of sources for it to reason about desired source (akin to reasoning about tools use)
- *semantic* - use build time prompt embeddings against runtime prompt embedding to inform desired source

## Query Construction

The intuition for query construction is leveraging the routing source to inform a structured query (query syntax and shape is database dependent)

The structured query is generated via function calling tools and structured outputs (text-to-sql, text-to-cypher, self-query retriever)

## Indexing

The intuition for indexing is knowing runtime performance goals so database type(s) and payload shape(s) inform database population

- *chunk optimization* - intelligent document splitting (aka chunking)
- *multi-representational* - chunking summaries
- *specialized embeddings* - domain-specific and/or advanced embeddings
- *hierarchical* - variations of hierarchy
    - *Recursive Abstractive Processing for Tree-Oriented Retrieval (RAPTOR)* - tree of document summaries at varying abstraction levels
    - *ColBERT* - token-based vs. document-based embedding

## Retrieval

The intuition for retrieval is that it is a post-processing step over the upstream pipeline result—manifesting as ranking, filtering, and refinement

## Generation

The intuition for generation is that it is the final step in a pipeline run which combines the upstream pipeline result with the original user prompt to create an LLM query (which can further be routed to a base model, fine-tuned model, etc.)
