# [3Blue1Brown - Essence of Linear Algebra by Grant Sanderson](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)

## Summary Nuggets

- *linear intuition* - "linear" relates to the fact that if you lock all but one scalar while changing the remaining, the resulting points follow a straight line
- *vector vs. point intuition* - arrow : vector alone :: points : vector set
  - **value** - mental visualization helper
- *linear combination intuition* - the scaling and addition of vectors
  - **value** - fundamental computation enabling space conversion
- *linear transformation intuition* - vector transformation : Matrix * Vector :: space transformation : Matrix * Span
  - **value** - mental visualization where "grid lines remain parallel and evenly spaced"
- *matrices intuition* - "transformations of space"
  - **value** - valuable in 2D and 3D graphics and games to convert spaces
- *composition intuition* - n linear transformations compressed as one linear transformation
  - **value** - performance win, aka matrix * matrix
- *determinant intuition* - non-zero means there is an inverse matrix
  - **value** - useful in linear system of equations to solve for unknowns

## Vectors | Chapter 1, Essence of linear algebra

- Three core perspectives of vectors:
  1. Physics - arrows pointing in space with length and direction
  2. Computer Science - ordered list of numbers
  3. Mathematician - seeks to generalize where a vector is anything where addition and multiplication rules apply
- A 4th perspective, geometric, is that a vector is an arrow inside a coordinate system where its tail sits at the origin
- adding vectors together results in a single resulting vector (vector addition)
- *scalar* - just a number used to scale vectors (vector multiplication)
- Vectors from the physics and computer science perspectives provide a language to describe space and the manipulation of space in a visual way while using numbers a computer can crunch

## Linear combinations, span, and basis vectors | Chapter 2, Essence of linear algebra

- Think of each vector component (coordinate) as the scalar to apply to the basis vectors of that coordinate system
  - Commonly the basis vectors of 2D are the unit vectors (x as i-hat, y as j-hat)
- *basis vectors* - the base vectors of a coordinate system which are most commonly the unit vectors (2D as i|x & j|y, 3D as i|x & j|y & k|z, etc.) aka 1 as unit but technically can be any scalar for each dimension
- *linear combination* - the scaling (using the basis vectors) and addition (vector addition) of vectors
- *span* - set of all possible vectors of all linear combinations (basis vectors over all real numbers)
  - 3 core span types:
    - *surface* - most common, all possible points in the space
      - *linearly independent* - each vector adds dimension to the span
    - *line* - least common, all possible points on a line in 2D, plane in 3D, etc.
      - *linearly dependent* - each vector doesn't add a dimension to the span (vector redundancy or "dimension drop")
    - *empty* - all possible points are just at origin

## Linear transformations and matrices | Chapter 3, Essence of linear algebra

- *linear transformation* - transformation as function called on the span
  - `function Transformation(input: Vector) : Vector`
  - All points of a span are *moved* to new points (aka spaceA to spaceB)
  - "linear" guarantees two things for the transformation:
    1. All grid lines remain parallel
    2. Origin remains fixed
- *matrix* - number grid encoding a transformation type to apply to a vector or span
  - space converter
  - each column represents the coordinates of where each dimension's basis vector lands (original space to transformed space)
  - matrix multiplication (the linear transformation) of vector(s) defines the resulting vector(s) coordinates in the new space
- *matrix-vector product* - each vector coordinate is the scalar to apply to its matrix column (coordinate of each dimension's basis vector)
- "linear transformations allow us to move from space to space where grid lines remain parallel and evenly spaced"
- *linear transformation calculation* - "matrix : left finger row-wise :: vector : right finger column-wise"
- *4x4 translation matrix* - in computer graphics the 4x4 matrix often encodes translation via:
  - rotation and scaling - upper 3x3 submatrix
  - translation - 4th column vector
  - homogeneity - 4th row as hack making non-square 3x4 a 4x4 (easing matrix multiplication and computation as square matrix)

### Example matrices

3x3 Identity
```
[1 0 0]
[0 1 0]
[0 0 1]
```

3x3 Rotation
```
[ cos(θ)  -sin(θ)  0 ]
[ sin(θ)   cos(θ)  0 ]
[   0        0     1 ]
```

3x3 Scale
```
[ x  0  0 ]
[ 0  y  0 ]
[ 0  0  1 ]
```

3x3 Translation
```
[ 1  0  x ]
[ 0  1  y ]
[ 0  0  1 ]
```

4x4 Identity
```
[1 0 0 0]
[0 1 0 0]
[0 0 1 0]
[0 0 0 1]
```

4x4 Rotation X
```
[ 1      0      0     0 ]
[ 0   cos(θ) -sin(θ)  0 ]
[ 0   sin(θ)  cos(θ)  0 ]
[ 0      0      0     1 ]
```

4x4 Rotation Y
```
[  cos(θ)  0  sin(θ)  0  ]
[    0     1    0     0  ]
[ -sin(θ)  0  cos(θ)  0  ]
[    0     0    0     1  ]
```

4x4 Rotation Z
```
[  cos(θ)  -sin(θ)  0  0  ]
[  sin(θ)  cos(θ)   0  0  ]
[    0        0     1  0  ]
[    0        0     0  1  ]
```

4x4 Scale
```
[ x  0  0  0 ]
[ 0  y  0  0 ]
[ 0  0  z  0 ]
[ 0  0  0  1 ]
```

4x4 Translation
```
[ 1  0  0  x ]
[ 0  1  0  y ]
[ 0  0  1  z ]
[ 0  0  0  1 ]
```

## Matrix multiplication as composition | Chapter 4, Essence of linear algebra

- *composition* - the resulting linear transformation of 2 or more linear transformations
  - ex. rotation then shear as having distinct matrices result in a single matrix defining both as one transformation
  - computationally speaking this is more performant and thus preferred in computer programs
  - a composition is a matrix multiplied by a matrix
- *matrix multiplication* - linear transformation column by column of right-most matrix
  - aka the computation of a composition
  - associative but not commutative (order matters) 

## Three-dimensional linear transformations | Chapter 5, Essence of linear algebra

- 2x2 matrix of 2D becomes 3x3 matrix of 3D
  - dimensionality reaches ∞

## The determinant | Chapter 6, Essence of linear algebra

- *determinant* - the scaling factor of the basis area (2D), volume (3D), etc.
  - a shorthand for how much space is stretched or squished
  - absolute value = scaling factor
  - -determinant means space inversion
- The determinant aids normals and lighting calculations, back-face culling, and collision detections by informing mirroring scenarios:
   - `determinant(someMatrix) > 0` = no mirroring (has inverse)
   - `determinant(someMatrix) < 0` = mirroring (has inverse)
   - `determinant(someMatrix) === 0` = dimsion collapse (lacks inverse)

## Inverse matrices, column space and null space | Chapter 7, Essence of linear algebra

- *linear system of equations* - two or more equations whose structure can be morphed to a linear transformation
  - _coefficients_ as matrix
  - _variables_ as input vector as unknown scalars
  - _constants_ as output vector as known scalars
  - ```
    2x + 5y + 3z = -3    [2 5 3][x]   [-3]
    4x + 0y + 8z =  0  = [4 0 8][y] = [ 0]
    1x + 3y + 0z =  2    [1 3 0][z]   [ 2]
    ```
- *inverse matrix* - simply invert each component
- *rank* - number of dimensions in transformation output
  - when rank is less than the starting matrix dimension, the solution space is constrained
  - aka "the number of dimensions in the columns space"
  - *full rank* - rank number matches matrix dimension
- *column space* - set of possible matrix outputs aka the "span of matrix columns"
  - tells us when a solution even exists
- *null space* - set of vectors that land on the origin
  - aka "kernel"

## Nonsquare matrices as transformations between dimensions | Chapter 8, Essence of linear algebra

- column : dimension :: row : coordinate (same as square matrices)
- differing nonsquare matrix multiplication is a dimension change
- nonsquare matrix multiplication is only valid if column count of matrixA (width) matches row count of matrixB (height) resulting in a `matrixA.rowCount x matrixB.columnCount`

## Dot products and duality | Chapter 9, Essence of linear algebra

- *dot product* - vector * vector and sum each component result (project one on the other)
  - `result > 0` = vectors point in same general direction
  - `result === 0` = vectors are perpendicular to each other
  - `result < 0` = vectors point in opposite direction
- *duality* - natural correspondence between two mathematical "things"/truths/situations
- *vector projection* - reveals how much of vector A lands on vector B which is useful for:
  - diffuse and specular lighting calculations
  - shadow mapping
  - path movement and planning

## Cross products | Chapter 10, Essence of linear algebra

- *cross product* - `vec1 X vec2 = vec3` that's perpendicular to both (think normal calculation) and whose length is the `vec1` `vec2` parallelogram
- The determinant of the two input vectors of the cross product is the magnitude of the orthogonal vector result  

## Cross products in the light of linear transformations | Chapter 11, Essence of linear algebra

## Cramer's rule, explained geometrically | Chapter 12, Essence of linear algebra 

- uses knowns from a linear system of equations and the fact that the determinant scales space uniformly to plug in to an equation to attain the unknowns
- *orthonormal* - set of vectors that are both orthogonal (perpendicular to each other) and normalized (having unit length)
  - the standard basis is a canonical example
    - 2D - `(1,0)` & `(0,1)`
    - 3D - `(1,0,0)` & `(0,1,0)` & `(0,0,1)`

## Change of basis | Chapter 13, Essence of linear algebra

- *coordinate system* - a system that uses one or more numbers (coordinates) to uniquely determine the position of a point (or other geometric element) in a given space
  - any system enabling translation between vectors and sets of numbers
- coordinate transformation is achieved via the change of basis matrix inverse applied to any vector to get where the vector lands in the different space

## Eigenvectors and eigenvalues | Chapter 14, Essence of linear algebra

- *eigenvector* - any vector that has the same span before and after a linear transformation
  - canonical eigenvectors in 3D are the axis of rotation where its eigenvalue is 1
- *eigenvalue* - the scalar that a given eigenvector is scaled by due to the linear transformation
- *eigenbasis* - a basis consisting of eigenvectors of a linear operator (typically a matrix)
- *diagonal matrix* - a matrix that has nonzero entries only on its main diagonal which are the eigenvalues corresponding to the eigenvectors that form the eigenbasis

## A quick trick for computing eigenvalues | Chapter 15, Essence of linear algebra

## Abstract vector spaces | Chapter 16, Essence of linear algebra

- *abstract vector space* - anything that functions as vector-like in that additivity and scaling rules apply
  - linear transformation of functions like canonical calculus derivative converting one function into another
- *vector space* - points in space, grids of numbers, functions—anything—that adheres to additivity and scaling axioms
- *homogenous coordinates* - an extension of Cartesian coordinates using 𝑛+1 coordinates for an 𝑛-dimensional space
  - a matrix hack enabling translation, rotation, and scaling as a single matrix and thus single linear transformation calculation
  - enable lockable translation, mid-way translation, and perspective
  - homogenous as one matrix

### 2D Examples

#### 2x2 Matrix (Without Homogeneous Coordinates)

Generic Linear Transformation
```
[ a  b ]
[ c  d ]
```

#### 3x3 Matrix (With Homogeneous Coordinates)

Translation
```
[ 1  0  tx ]
[ 0  1  ty ]
[ 0  0  1  ]
```

Scaling
```
[ sx  0   0 ]
[ 0   sy  0 ]
[ 0   0   1 ]
```

Rotation by θ
```
[ cosθ  -sinθ  0 ]
[ sinθ  cosθ   0 ]
[ 0     0      1 ]
```

### 3D Examples

#### 3x3 Matrix (Without Homogeneous Coordinates)

Generic Linear Transformation 
```
[ a  b  c ]
[ d  e  f ]
[ g  h  i ]
```

#### 4x4 Matrix (With Homogeneous Coordinates)

Translation
```
[ 1  0  0  tx ]
[ 0  1  0  ty ]
[ 0  0  1  tz ]
[ 0  0  0  1  ]
```

Scaling
```
[ sx  0   0   0 ]
[ 0   sy  0   0 ]
[ 0   0   sz  0 ]
[ 0   0   0   1 ]
```

Rotation about Z-axis by θ
```
[ cosθ  -sinθ   0    0 ]
[ sinθ  cosθ    0    0 ]
[ 0     0       1    0 ]
[ 0     0       0    1 ]
```