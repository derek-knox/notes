# Evals - Neural Network Evaluation Best Practices

## [AlignEval: Building an App to Make Evals Easy, Fun, and Automated by Eugene Yan](https://eugeneyan.com/writing/aligneval/)
 
- https://aligneval.com/
- KISS, just use binary pass/fail labels (accuracy, simplicity, speed, low cognitive load, etc.)
- "The more familiar you are with your data, the better your evals will be."

## [Task-Specific LLM Evals that Do & Don't Work by Eugene Yan](https://eugeneyan.com/writing/evals/)

### Classification: Recall, precision, ROC-AUC, PR-AUC, separation of distributions

- *classification* - assigning predefined labels (sentiment, topics, etc.)
  - *extraction* - extracting specific information (names, dates, locations, etc.)
  - Example:
    ```python
    # Text input
    "Alice loves her iPhone 13 mini that she bought on September 16, 2022."

    # Classification and extraction output
    {
        "sentiment": "positive",    # Sentiment classification
        "topic": "electronics",     # Topic classification
        "toxicity_prob": "0.1",     # Toxicity classification
        "names": [                  # Name extraction
            "Alice",
            "iPhone 13 mini"
        ],
        "dates": [                  # Date extraction
            "September 16, 2022"
        ]
    }
    ```
- aggregate statistics
    - *recall* - proportion of true positives that were correctly identified
      - If there were 100 positive instances in our data and the model identified 80, recall = 0.8
    - *precision* - proportion of the model’s positive predictions that were correct
      - If the model predicted positive 50 times but only 30 were truly positive, precision = 0.6
    - *false positive* - model predicted positive but actually negative
    - *false negative* - model predicted negative but actually positive 
- > It gets interesting when our models can output probabilities instead of simply categorical labels (e.g., language classifiers, reward models). Now we can evaluate performance across different probability thresholds, using metrics such as ROC-AUC and PR-AUC.
  - *AUC* - area under curve
  - *Receiver Operating Characteristic (ROC-AUC)* - plots the true positive rate against the false positive rate at various thresholds, visualizing the performance of a classification model across all classification thresholds
    - robutst to class imbalance
    - performance evaluated across not one, but all thresholds
    - scale invariant (robust to model's predictions being skewed)
  - *Precision Recall (PR-AUC)* - plots the trade-off between precision and recall across all thresholds
    - higher thresholds lead to higher precision (fewer false positives) but lower recall (more false negatives), and vice versa
  - > Examining the separation of distributions is valuable because a model can have high ROC-AUC and PR-AUC but still not be suitable for production. For example, if a chunk of the predicted probabilities fall between 0.4 and 0.6 (below), it’ll be hard to choose a threshold—getting it wrong by merely 0.05 could lead to a big drop in precision or recall. Examining the separation of distributions gives you a sense of this.

### Summarization: Consistency via NLI, relevance via reward model, length checks

- *extractive summary* - summarization that lifts literal copy from source document
- *abstractive summary* - summarization capturing concise key ideas from source document
- Four key dimensions of abstractive summary evaluation
    - *fluency* -  Are sentences in the summary well-formed and easy to read?
      - We want to avoid grammatical errors, random capitalization, etc.
    - *coherence* -  Does the summary as a whole make sense?
      - It should be well-structured and logically organized, and not just a jumble of information.
    - *consistency* -  Does the summary accurately reflect the content of the source document?
    -   We want to ensure there’s no new or contradictory information added.
    - *relevance* -  Does the summary focus on the most important aspects of the source document?
      - It should include key points and exclude less relevant details.
    - > I seldom see grammatical errors or incoherent text from a decent LLM (maybe 1 in 10k). Thus, no need to invest in evaluating fluency and coherence.

### Translation: Quality measures via chrF, BLEURT, COMET, COMETKiwi

- *machine translation* - task of automatically converting text from one language to another
- *chrF* - > The idea behind chrF is to compute the precision and recall of character n-grams between the machine translation (MT) and the reference translation
- *sacreBLEU* - > provides a standardized implementation of chrF (and other metrics), ensuring consistent results across different systems and tasks.

### Copyright: Exact regurgitation, near-exact reproduction

- ...

### Toxicity: Proportion of toxic generations on regular and toxic prompts

- ...

### Nonetheless, we still need human evaluation

- More examples via expanded label collection to increase:
  - precision - Select instances that the model predicts as positive with high probability and annotate them to identify false positives
  - recall - Select instances that the model predicts have low probability and check for false negatives
  - confidence - Select instances where the model is unsure (e.g., probability between 0.4 to 0.6) and collect human labels for finetuning
- > This can also be applied to evals like factual consistency and relevance since they can be binary decisions. Another reason why simplifying evals to a binary metric helps.
- > As a data point, the typical factual inconsistency/irrelevance rate is 5 - 10%, even after grounding via RAG and good prompt engineering. And from what I’ve learned from LLM providers, it may be prohibitively hard to go below 2%. (This is why we need factual inconsistency guardrails on LLM output.)
