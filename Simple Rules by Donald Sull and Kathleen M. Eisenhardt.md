# Simple Rules by Donald Sull and Kathleen M. Eisenhardt

- The four common traits defining simple rules:
    1. No more than a handful (focus)
    2. Tailored to the individuals who use them, not one-size-fits-all
    3. Applied to single well-defined activity or decision (ideally an activity/decision that represents bottlenecks to accomplishing a goal)
    4. Give concrete guidance without being overly prescriptive (allows for creativity and to pursue unanticipated opportunities)
- Strategy of simple rules:
    1. Figure out what moves the needle
    2. Choose a bottleneck
    3. Craft the rules
- A company's ultimate objective should be to create economic value over time and capture it as profits
- Economic value - the difference between what a customer is willing to pay for a product and the cost of all the inputs required to produce it. The top needle is the payment value and the bottom needle is the cost value
    - *What will move the needles?*
- Economic value creation:
    1. Who will we target as customers?
    2. What product/service will we offer?
    3. How will we provide this product at a profit?    
        - `1 + 2` = What game are we playing? and `3` = How will we win?
        - Simple rules applied to a critical bottleneck embed insights about value creation and shape day-to-day activities
- A bottleneck is an activity or decision that hinders moving the needles. The best bottlenecks to focus on have the following traits:
    1. Direct and significant impact on value creation
    2. Represent *recurring* decisions (not one-offs)
    3. Opportunities exceed available resources
- Personal rules:
    1. Decide what will move your personal needles and increase the gap between what energizes you and what stresses you out
    2. Identify a bottleneck that keeps you from creating personal value
    3. Develop simple rules that work for you
