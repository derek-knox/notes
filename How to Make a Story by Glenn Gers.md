# How to Make a Story by Glenn Gers

https://www.youtube.com/watch?v=uL0atQFZzL8

- "I (the character(s)) have a thing I'm trying to accomplish and that's what story is. It's a more interesting story if something is in the way"
- What actions they take to overcome the obstacles is what adds tension to a story

Answer the following six questions:

1. Who is it about?
2. What do they want?
3. Why can't they get it?
4. What do they do about that?
5. Why doesn't that work?
6. How does it end?
