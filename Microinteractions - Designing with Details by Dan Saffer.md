# Microinteractions - Designing with Details by Dan Saffer

- The difference between a product we love and a product we just tolerate are often the microinteractions we have with it
- When there is feature parity, it's the microinteractions that influence the user experience, adoption, and brand loyalty
- "The button meant for the first time the result of human motion could be completely difference from the motion (it creates) itself" - Bill DeRouchey
- The goal for microinteractions is to minimize choice and instead provide smart defaults with minimal/limited choices. The control you choose should reflect this philosophy
- The most discoverable triggers (most to least discoverable) are:
  1. A moving object (pulsing icon)
  2. An object w/affordance and a label (labeled button)
  3. An object w/label (labeled icon)
  4. An object alone (icon)
  5. Label only (menu item)
  6. Nothing (invisible trigger)
- A trigger is whatever initiates a microinteraction and there are two types:
  1. Manual (control, icon, form, voice, touch, or gesture)
  2. System (certain conditions met—timing, states, modes, etc.)
- _"Bring the data forward"_ - the trigger itself can reveal information (icon w/# of emails, icon revealing next song title, etc.)
- The best, most elegant microinteractions are often those that allow users a variety of verbs with the fewest possible nouns
- Any object a user can interact with can have (at least) these states:
    1. Invitation/default
    2. Activated
    3. Updated
- _"Don't start from zero"_ - after the trigger has been initiated, ask what do I know about the user and context?
  - The goal is to use the context and previous behavior (if any) to predict or enhance the microinteraction
- Computers are often better than humans in handling complexity so let the system handle it when:
  1. Rapidly performing computation and calculations
  2. Doing multiple tasks simultaneously
  3. Unfailingly remembering things
  4. Detecting complicated patterns
  5. Searching through large datasets for particular item(s)
- Limit options and provide smart defaults
- When choosing controls, you have the choice between _operational simplicity_ (every command its own control) or _perceived simplicity_ (simple control has many actions)
  - Account for how often actions are taken, there needed quickness, and no chance of error when deciding
- _Requisite variety_ - the ability to survive under varied conditions
- Err on the side of perceived simplicity to do more with less
- Use the rules fo prevent errors. Errors should be rare and made only by the system (latency, network, lack of data, etc.) and human errors should be impossible
- Feedback messaging types:
    1. Something has happened
    2. You did something
    3. A process has started
    4. A process has ended
    5. A process is ongoing
    6. You can't do that
- Once you know which of the six messages you want to send, the decision of how those messages manifest is the only remaining decision. The feedback is dependent on the hardware and can be visual, haptic, audible, etc.
- Neurologically, errors improve performance; how humans learn is when expectation doesn't meet the outcome. Ensure users aren't penalized for trial and error and experimentation.
- Improving or creating a microinteraction:
    1. Should their be a signature moment?
        - If intentionally memorable, use rich controls (maybe even custom) + feedback
    2. Am I starting from zero?
        - What information (user or context) would improve the microinteraction?
    3. By "bringing data forward", what piece is most important?
        - What does the user need to know at a _glance_?
    4. Is a custom control appropriate?
        - Custom UI virtually ensures prominence
    5. Am I preventing human errors?
        - How can we automatically prevent users from making errors?
    6. Am I using what is overlooked?
        - Could other UI or hardware be doing more?
    7. Can I make an invisible trigger for advanced users?
        - Do hidden shortcuts (gesture or command) get deeper into rules faster?
    8. Are the text and icons human?
        - Does microcopy sound like something someone would actually say out loud? Does a comedic bent make sense?
    9. Can I add animation to make less static?
        - Do visual transitions make sense? Does a non-intrusive next-step indicator exist?
    10. Can I add additional channels of feedback?
        - Is it valuable to add sound, haptics, etc.?
    11. What's the difference between the microinteraction 1st and 100th use?
        - What should the long loop be like?
- A process for testing microinteractions:
    1. Prior to showing the test user, ask what _single_ thing they want to accomplish by using the microinteraction?
    2. Is there anything you'd want to know prior to using the microinteraction?
       - This may prove no microinteraction is needed.
    3. Let them use it _unaided_ and have them speak aloud their impressions and decisions. Once complete, ask them how it works (the rules) to validate its robust and intuitive.
    4. If they came back later, what would they want the UI/system to remember?
    5. Ask what one thing should be fixed?
