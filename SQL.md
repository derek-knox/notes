# SQL
- keys
    - primary - unique key for each record in a table
    - foreign - value matching primary key of another table

- Read
    1. `SELECT` column(s) (*required*)
        - get data (aggregate or not)
        - `DISTINCT` column(s) (*optional*)
            - distinct column values
    1. `FROM` table (*required*) `Var` (*optional*)
        - set data location
        - `Var` entity instance hook useful in a `JOIN`'s `ON` condition
    1. `JOIN` table (*optional*)
        - merge with other data location
            - `INNER`
                - match both from of `ON` condition
            - `LEFT`
                - all from first table, `NULL` insert for second lacking
            - `RIGHT`
                - all from second table, `NULL` insert for first lacking
            - `FULL`
                - all from both tables, `NULL` insert for both lacking
    1. `ON` condition
        - filtering condition for `JOIN`
    1. `WHERE` non-aggregate filter condition(s) (*optional*)
        - filter data (not aggregate)
    1. `ORDER_BY`/`GROUP_BY` coalescing (*optional*)
        - coalesce data
    1. `HAVING` aggregate filter condition(s) (*optional*)
        - filter data (aggregate)

- Write
    1. `INSERT INTO` table (each, column, name) (*required*)
    1. `VALUES` (each, column, value) (*required*)

- aggregates ignore records with a `NULL` value

- an intermediate table holds 2+ primary keys of other tables in an effort to link the data

- `WHERE` vs `HAVING`
    - `WHERE` filters during pull
    - `HAVING` filters during push due to aggregate used as filter condition