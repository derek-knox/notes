# But How Do It Know? by J. Clark Scott

- Computers are dependent on the presence and absence of electricity
- Computers are fast because electricity is fast (186K mi/sec to be exact—looping earth ~7x in one second)
- The presence and absence equates to a *bit*. Small circuits are representative of bits (bit like light switch)
- So a bit has two states where only one state is possible at a given point in time
- A small circuit that represents a bit is called a *gate*. There are many types of gates possible but the most common is a NAND gate (negative AND). As such, there is an AND gate. Also, there is a NOT gate among others.
- Gates typically have two inputs and a single output. The type of circuit within each gate determines the output. The output is always a bit or a collection of bits.
- When a collection of bits comes in an eight pack it is called a *byte*
- A byte can represent 256 states (2^8)
- Humans arbitrarily map codes to these states to represent something meaningful
- A byte results in a binary 8-set where a 1 is presence of electricity (on) and a 0 is the absence of it (off)
  - Technically there is a voltage threshold that maps to the 1 and 0
- Humans then reason about the sequence of each bit in a byte to mean something (character in a written language using ASCII code, number from 0-255, hexadecimal character, etc.)
- Unique combinations of gates allow the output bits to map to our reasoned code. As a result we can get more powerful constructs like memory, arithmetic, and logic
- A *memory bit* for example is composed of 4 NAND gates where there are two inputs into the circuit and one output. Eight of these equate to a byte of memory
- Building further we can create a byte composed of 8 AND memory bits which is called an *enabler*. The memory byte and enabler byte can be combined to form a *register*. A register can store state (via memory byte) and pass it along via the enabler.
- In order for state to be passed around we need to set up a track of 8 wires. These tracks are called a *bus*.
- A register hooked up to a bus is what allows a computer to communicate state (which humans use to map to a code allowing us to extract meaning). Still all of this is just electricity moving on the bus and through registers where the on/off of the outputs are interpreted as something meaningful in another part of the computer
