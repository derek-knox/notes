# Algorithms to Live By by Brian Christian and Tom Griffiths

## Optimal Stopping

- **37% Rule** - *Secretary Problem*
    - Given a predetermined amount of options or time, pick the next best after 37%
        - aka *look then leap*
            - *look* phase: `async read` to 37%
            - *leap* phase: `sync write` at next best found
    - This rule only applies when you:
        - can't "go back" and pick a prior
        - can't entertain all options (often a constraint on time or resources is the forcing function)
    - 37% is optimal because the mathematical chance of picking the best option converges without (1) *passing over* and (2) *never considering*
- **Threshold Rule** - is a variant of the 37% rule where you have "full information" such that you can calculate the relative odds against a uniform metric.
    - Starting at option one, sized against a uniform metric each option adheres to, you can attain a 58% chance of finding the best option.
    - Predetermine a threshold prior to considering options and *leap* when the threshold is passed.
        - If the threshold is never hit you must calculate the odds of a remaining option exceeding the current option
