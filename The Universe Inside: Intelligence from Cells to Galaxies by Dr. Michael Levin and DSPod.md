# [The Universe Inside: Intelligence from Cells to Galaxies by Dr. Michael Levin and The DemystifySci Podcast](https://www.youtube.com/watch?v=Q7SDA1h1sjg&t=3s)

- [Classical Sorting Algorithms as a Model of Morphogenesis: self-sorting arrays reveal unexpected competencies in a minimal model of basal intelligence
](https://arxiv.org/abs/2401.05375)
  - The field of Diverse Intelligence aims to understand and formalize common behavioral competencies across various systems, focusing on simple systems that exhibit unexpected capabilities like memory, decision-making, or problem-solving.
  - The study uses classical sorting algorithms as a model to explore these competencies, challenging assumptions of top-down control and fully reliable hardware.
  - Novel analyses show that sorting algorithms, when applied to arrays of autonomous elements with some elements failing, exhibit more reliable and robust sorting than traditional methods.
  - The study reveals emergent problem-solving abilities in simple algorithms, including the capacity to navigate around defects and clustering behavior in chimeric arrays with elements using different algorithms.
  - This research contributes new insights into how basal forms of intelligence can emerge in simple systems without explicit encoding in their mechanics.
- **basal Intelligence** - the fundamental or basic level of cognitive ability that supports and influences higher-level thinking and problem-solving
- **chimera** (original) - a mythical creature with the body parts of different animals, such as a lion's head, goat's body, and serpent's tail
- **chimera** (modern) - a hybrid or composite entity, often involving a mix of different elements or concepts
- prokaryotic vs. eukaryotic
  - **prokaryotic** - cells without a nucleus or other membrane-bound organelles
    - Genetic material is located in a nucleoid region
      - Examples: bacteria and archaea
  - **eukaryotic** - cells with a nucleus and membrane-bound organelles
    - Genetic material is enclosed within the nucleus
      - Examples: plants, animals, fungi, and protists
- **clustering** - the organization of cells or biological structures into distinct groups or patterns
  - This is crucial for the proper formation and function of tissues and organs
- **algotype** - specific types of cellular or tissue patterns that arise during the process of morphogenesis
  - Often associated with particular genetic or environmental conditions that influence developmental outcomes
  - The curve he suggested prior to detailing the experiment suggests to me that similar algotypes aggregate and this somehow has the effect of learning how a portion of the system works before it goes back and intermingles
- **biobot** - a general term for any robot that incorporates biological components or is designed to interact closely with biological systems
    - **xenobot** - a specific type of biobot created using frog cells to form programmable organisms capable of movement and other behaviors
    - **anthrobot** - a hybrid organism that merges human biological elements with robotic systems
      - typically designed for research or practical applications that require both human and machine capabilities
    - **nanobot** - a microscopic robot designed to perform tasks at a nanoscale
      - often used in medical applications such as targeted drug delivery or surgery
    - **cyborg** - a being with both organic and biomechatronic body parts
      - often referring to humans enhanced with robotic or electronic devices
- **frog hybrid** - an experimental organism created by combining genetic material or cells from frogs with those from other species
  - often used to study developmental processes, genetic interactions, or regenerative capabilities
- Michael's final comment after the 20-minute mark suggests he is exploring an alternative to evolution, proposing a new scientific approach that does not rely on evolutionary processes due to the lack of time for natural selection
- hypothesis then theory
  - **hypothesis** - an initial, testable prediction or idea that can be _used to build or refine theories_
  - **theory** - a comprehensive and widely accepted explanation supported by extensive evidence
- _surprise minimalization_ related theories
  - **predictive coding** - the brain constantly generates and updates predictions about the world to minimize the discrepancy between expected and actual sensory input
  - **bayesian inference** - a statistical method used to update the probability of a hypothesis as more evidence becomes available, effectively minimizing surprise by refining beliefs based on new data
  - **information theory** - particularly the concept of **entropy**, which measures uncertainty or surprise in information, focusing on the reduction of surprise through the efficient encoding and transmission of information
- In his example, a single cell to multi-cell transition manifests as a barrier since copies of yourself are familiar (surprise minimalization)—path of least resistance idea follows
- > "biomedicine, essentially drugs today do one thing, they bind and do. Whether that's positive or negative for the individual is unknown. Whereas if you were able to have a biobot that's more contextual, where that is his aim in the sense of their application"
  - rough quote at the 35 minute Mark—he shared an example where birth defects are countered based off contextually aware medicines at a particular growth stage
- **reagent** - a substance used in a chemical reaction to detect, measure, or produce other substances
  - often used in laboratory experiments to facilitate or observe specific reactions
- **morphogenesis** - the process by which organisms develop their shape and structure through the coordination of cellular and molecular mechanisms during growth and development
- > "Complete control over growth and form"
- The four scientific experimental contexts
    1. Biological
        - **in vivo** - Latin for "within the living," referring to experiments or observations conducted within a living organism
        - **in utero** - Latin for "in the womb," referring to studies or observations conducted within a developing embryo or fetus
        - **in planta** - Latin for "in plants," referring to studies or observations conducted directly within plant organisms
    2. Natural location - biological and non-biological
         - **in situ** - Latin for "in its original place," referring to studies or measurements conducted in the natural location of the subject
    3. Synthetic organic
         - **in vitro** - Latin for "in glass," referring to experiments or observations conducted outside of a living organism, typically in a controlled laboratory environment
         - **in chemico** - Latin for "in chemistry," referring to experiments or observations conducted in a chemical context, often involving chemical reactions or interactions
    4. Synthetic inorganic
         - **in silico** - Latin for "in silicon," referring to experiments or simulations conducted using computer models or computational methods
- **gall** - an abnormal growth or swelling on a plant caused by various organisms, including wasps, mites, fungi, or bacteria
  - bug vs. feature
    - _bug_ - from the perspective of the plant, galls can be seen as a harmful effect or a nuisance, disrupting normal growth and development
    - _feature_ - from an ecological perspective, galls can be considered a feature of the interaction between the plant and the gall-inducing organism, showcasing a complex evolutionary adaptation and mutual relationship
- his conjecture on developmental constraints, "connects to the spinal cord or nothing obvious"—regarding third eye growth on frog perception working irrespective of brain wiring
- **autophagy** - a cellular process in which a cell degrades and recycles its own components, effectively "eating itself" to maintain homeostasis and remove damaged or unnecessary parts
- > "Evolution doesn't make solutions to specific problems... it makes problem solving machines"
- **polyploidy** - the condition of having more than two complete sets of chromosomes in an organism
  - This can occur naturally or artificially and can result in an organism with multiple sets of genetic material, such as 2n, 4n, or 6n. Despite these changes, the organism can still exhibit typical characteristics of its species, as seen in some salamanders.
- **agential material** - materials or entities that have the capacity to act or influence outcomes through their inherent properties or behaviors, often used in contexts involving the agency of non-human elements or systems
- [Shuffle Brain: The Quest for the Holgramic Mind](https://www.amazon.com/Shuffle-Brain-Quest-Holgramic-Mind/dp/0395294800) - book about salamanders within the context of a new (early 1980s) theory of memory based on experiments reshuffling brain parts without scrambling its information codes
- **prion** - a misfolded protein that can induce other proteins to also misfold, leading to the accumulation of abnormal proteins in tissues
  - within in the brain, they cause neurodegenerative diseases such as Creutzfeldt-Jakob disease and mad cow disease

