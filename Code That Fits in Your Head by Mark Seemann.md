# Code That Fits in Your Head by Mark Seemann

- The sooner you *automate the build* and *turn on all errors* the better
  - Leverage as much static analysis, linters, etc. tooling as possible if not a prototype
- Work in minimal *vertical slice* PRs (Walking Skeleton)
- Leverage *Arrange, Act, Assert* thinking in non-test code
- Use DTOs (data transfer objects) to properly flag raw data (regardless of format)
- Our short-term memory generally works at ~7 active pieces of information, code accordingly
  - *Cyclomatic complexity* - a useful metric defining the code path count for a given chunk of code (1+ n conditional pathways)
  - *Hex flower* - mental model visual for adhering to the ~7
  - *Fractal architecture* - leveraging cyclomatic complexity and short-term memory constraints to keep code zoom levels simple to grok
- *Command Query Separation* - methods of modules/classes should be separated in two groups:
  - *command* - method(s) that mutate and lack a return value
  - *query* - method(s) that are pure and provide a return value
- Use the *Strangler Pattern* (Strangler Fig) when refactoring. For non-trivial refactors:
    1. Have a side-by-side implementation
       - Likely worth refactoring the old with a postfix of "Old" or "Deprecating" so that the new can retain the desired end name
    2. Transition old uses to new method/class
    3. Finally remove the old implementation once full transition is complete
- Consider the *Decorator* pattern when cross-cutting concerns
- When possible, leverage *Property-based Testing* for tests so input can be automated with breadth
- *Function core, imperative shell* is a useful architecture for ensuring FP is leveraged deep in the app root where imperative code is pushed leafward
