# The Lean Startup by Eric Ries

## Start

- The goal of a startup is to figure out the right thing to build—the thing customers want and will pay for—as quickly as possible
- Vision > Strategy (pivot) > Product (optimization)

## Define

- *Startup* - a human institution designed to create a new product or service under conditions of extreme uncertainty 
- Don't build a society of politicians who fight to sell a single idea, build a society of entrepreneurs who can test and validate numerous ideas quickly

## Learn

- Power of network - "Imagine a world in which you own the only phone; it would have no value. Only when others also have phones does it become valuable
- Commit to launch dates, delays prevent startups form getting the feedback they need
- Value vs. waste - value is something providing benefit to a customer, anything else is waste
- *Validated learning* - running frequent experiments that allow entrepreneurs to test each element of their vision

## Experiment

- If you cannot fail, you cannot learn
- The Lean Startup methodology - all efforts placed on experiments that test its strategy to see which parts are brilliant and which are crazy
    1. Clear hypothesis predicting what is supposed to happen
    2. Test predictions empirically
        - The goal of every experiment is to discover how to build a sustainable business around a vision
- Most important assumptions:
    1. Value hypothesis - test whether a product/service really delivers value to customers once using it
    2. Growth hypothesis - test how new customers wil discover a product/service
- The experiment is the product
    - Not just a theoretical inquiry, it is the first product
    - If successful, continue experiments and iterate
    - Early adopters provide valuable feedback and help determine what should actually be built—where the spec is rooted in usage/feedback of what works, not theory of what might—test it!
- Product development questions:
    1. Do consumers recognize they have the problem you are trying to solve?
    2. If there was a solution, would they buy it?
    3. Would they buy it from us?
    4. Can we build a solution for that problem?
- "Success is not delivering a feature, success is learning how to solve the customer's problem"
- Identify the elements of a plan that are assumptions not fact and figure out ways to test them
- Planning is a tool that only works in the presence of a long and stable operating history... *test*, don't *plan* what will work

## Steer

- Build > product > Measure > data > Learn > ideas > [LOOP]
- Minimize TOTAL time through the loop!
- Planning works in a reverse loop order:
    1. Figure out what we need to learn
    2. Use innovation accounting to figure out what we need to measure to know if we are gaining validated learning
    3. Figure out what product we need to build to run that experiment & get that measurement

## Leap

- Get first-hand experience/exposure, don't rely on reports or others' information. From first-hand experience it is possible to increase probability of gaining unique insight
- Create a customer archetype humanizing the target customer, but ensure this data derives from contact w/customers and collective input

## Test

- The goal of a MVP (minimum viable product) is to get through the *Build-Measure-Learn* loop as fast as possible with the minimum amount of effort (aka rapid learning)
- The MVP is designed to not just answer product design of technical questions, it is to test fundamental business hypothesis
- MVP types/ideas:
    - Video - cheap way to determin interest or validate sign-ups
    - Concierge - face to face interaction and product/service delivery to validate growth model
    - Subset - small prototype that tests a single/small element of the MVP at large
- Low-quality MVP - helps focus on what attributes customers care about most (learn don't speculate).
    - "Customers don't care how much time something takes to build. They care only if it serves their needs."
    - Remove any feature, process, or effort that does not contribute directly to the learning you seek for an MVP

## Measure

- Innovation accounting
    1. Use MVP to establish real data on current company startups
    2. Tune the engine from the baseline toward the ideal
    3. Pivot or persevere
- Use MVP to establish baseline (conversion rates, sign-ups & trial rates, customer lifetime value, etc)
- Test and measure the riskiest assumptions first
- Every product development, marketing, or other initiative that a satrup takes should be to improve one of the drivers of its growth model
- A good design is one that changes customer behavior for the better
- If we're not moving the drivers of our business model, we're not making progress (sure sign of pivot)
- A pivot is successful if the experiments you run are overall more productive than those prior to pivoting
- The rhythm - establish baseline, turn the engine, decide to persevere or to pivot
- 3 "A"s for metrics:
    - Actionable - must demonstrate clear cause and effect
    - Accessible - "metrics are people too" - reports must be simple
    - Auditable - ensure data is credible to employees

## Pivot

- Startup productivity is about aligning our efforts with a business and product that are working to create value and drive growth
- Successful pivots put us on the path toward growing a sustainable business
- Learning milestones don't make a pivoting decision easy, but are aids of relevent info come decision time
- "Runway" - amount of time remaining for a startup to liftoff. Often defined as `moneyInBank / monthlyBurnRate = runway`. It can also be understood as the amount of pivots remaining (the # of opportunities to fundamentally change business strategy).
    - This 2nd outlook can extend the runway by *getting to each pivot faster* (learn fast and cut losses)
- Pivots require courage. Be aware of three core things:
    1. Vanity metrics - ignore them, use actionable metrics to properly determine if pivoting is required
    2. Unclear hypothesis - must be clear otherwise failure is impossible to judge
    3. Fear - you must face fears and be willing to fail (often publically or in a big way), even more reason to continue to validate what works and what doesn't
- When pivoting, it is not necessary to throw everything out and start over. Instead try to repurpose what has been built and what has been learned to find a more positive direction
- Pivot - special kind of change designed to test a new fundamental hypothesis about the product, business model, and engine of growth.
- There are 3 primary engines of growth:
    1. Viral
    2. Sticky
    3. Paid
- Maximize success by answering:
    1. What products do customers really want?
    2. How will our business grow?
    3. Who is our customer?
    4. Which customers do we listen to and which do we ignore?

## Batch

- Small batches provide faster and complete products and allow defects to be spotted instantly in a process reducing rework immensely. They enable entrepreneurs to minimize the waste of time, money, and effort
- The ability to learn faster from customers is the essential competitive advantage that startups must possess
- The startup way: people > culture > process > accountability

## Growth

- Sustainable growth rule (aka "the engines of growth") - new customers come from the actions of past customers
    1. Word of mouth
    2. Side effect of product usage (people see others using a product which can encourage adoption)
    3. Funded advertising - cost of acquisition (marginal cost) is less than revenue generated by customer (marginal revenue). The excess (marginal profit) can be used to acquire more customers, so more profit = faster growth
    4. Repeat purchase/use via subscription and voluntary repurchase (ran out, need more)
- Churn rate - fraction of customers in any period who fail to remain engaged with a product
- Sticky engine of growth - when new acquisitions exceeds churn rate
- Growth speed (rate of compounding) - natural growth rate minus the churn rate
- If acquisitions is equal to churn rate, then work for customer retention (updates, fixes, etc.)
- Focus on the viral coefficient most (how many users does a new user bring?). Small changes have massive impact. Products are typically free to remove friction/adoption which massively impacts this value
- Two ways to increase growth rate for "paid engine of growth":
    1. Increase revenue from each customer
    2. Drive down customer acquisition cost
- CPA - cost-per-acquisition
- LTV - lifetime value
- If CPA < LTV = growth (margin = rapidity, aka marginal profit)
- Only after pursuing one engine of growth thoroughly should a startup pivot to another

## Innovate

- Startup teams require three structural attributes:
    1. Scarce but secure resources
    2. Independent authority to develop their business
    3. Personal stake in the outcome
- Lean steps:
    1. Build ideal model of desired disruption that is based on customer archetypes
    2. Launch MVP to establish baseline
    3. Tune the engine to get MVP toward ideal state
- Build > Measure > Learn - remember the goal is to go *through* the loop as fast as possible to gain validated learning
