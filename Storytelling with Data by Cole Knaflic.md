# Storytelling with Data by Cole Knaflic

## Context
- exploratory vs explantory analysis
    - exploratory as discovery process
    - explanatory as communication of valuable discoveries
- identify specific audience and craft the communication and the story for **them**
- storyboard first (aligns with my low-fidelity sketching belief)

## Visuals that Work
- Preferred visualization types:
    - simple text | Text
    - table | Table
    - heatmap | Table
    - scatterplot | Graph Points
    - line | Graph Line
    - slopegraph | Graph Line
    - vertical bar | Graph Bar
    - vertical bar stacked | Graph Bar
    - horizontal bar | Graph Bar
    - horizontal bar stacked | Graph Bar
    - waterfall | Graph Bar
    - square area | Graph Area
- "Tables interact with our verbal system where graphs interact with our visual system, which is faster at processing information." - Knaflic
- Meltano could distinguish between exploratory and explanatory modes (lightweight GUI toolset in addition to potentially leveraged generated charts like avgs, extremes, etc combined with AI interestingness)
    - Auto inserted avg. dot, line separator, and color application to quickly show above and below avg. (similar to fig. 2.7)
    - summary statistic, avg, forecast point estimate, auto-outliers, other? as layering value automatically
    - we can suggest best visualization approach based on shallow and deep understanding of datasets
        - shallow as visualization type (text for detailing a number or two, line for continuous, bar as categorical, etc)
        - deep as AI interestingness or other algorithm based value extractions
- 3D graphs should be avoided due to perspective skews actually skewing data and alignment of XY tickmarks of a graph
    - My gut says iso 3D may be useful, but still be weary
- pie and donut are frowned upon, but I think her argument is weak once design, preattentive attributes, and the story telling aspects are considered
    - labeling becomes critical, but if labels don't fit cleanly, it makes these approaches subpar likely
    - these are good in my opinion for parts-of-a-whole visualization though simple text may be best

## Clutter is the enemy
- clutter negatively impacts cognitive load
- avoid diagonal lines and especially diagonal text as they increase the time it takes to read/absorb the information
- prefer left-aligned to center-aligned text where right-aligned can be more useful when anchoring an argument/message to data at the right of the graph
- "white space in visual communication is synonymous to a pause in verbal communication" - Knaflic

## Focus attention
- preattentive attributes (color, size, position) enable:
    1. direct attention
    2. hierarchy flow

## Think like a designer
- highlight important aspects (preattentive attributes in combo with preference of bold -> italic -> underline)
- eliminate distraction
- create clear hierarchy
- be intentional

## Lessons in storytelling
- people remember stories easier than data points
- setup -> conflict -> resolution
    - plot -> twists -> call to action
    - what? -> so what? -> now what?

## Recap
1. understand context
    - who are you communicating to?
    - what do they need to know?
    - what's the big idea?
    - what's the three minute story?
1. visual decisions
    - text is best for communicating a number or two
    - line charts are best for communicating continous data
    - bar charts are best for communicating categorical data (must have zero baseline)
    - avoid 3D, donut, and pie (though I disagree a bit as mentioned above)
1. eliminate clutter
    - leverage white space, alignment, and contrast intentionally
1. focus attention
    - leverage preattentive attributes like color, size, and position to focus attention intentionally
1. designer mentality
    - be intentional, logical, and aesthetically pleasing to ease cognitive load and improve speed of understanding
1. storytell
    - leverage story as stories resonate most with humans in comparison to data points alone (setup -> conflict -> resolution)