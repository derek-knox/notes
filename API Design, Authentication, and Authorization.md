# API Design, Authentication, and Authorization

## API Design

  - `/resource/{id}/action`
    - plural vs singular (think in collections)
    - prefer `id` vs `string`/`name`
    - action only when mutation/deletion (`PUT`, `PATCH`, `DELETE`)
        - method inferred for `GET` (`resource/{id}`)
        - query arg as condition to change
    - `PUT` as success/fail response where `PATCH` returns the object (`PUT` subset + `GET`)
  - `/resource/` is `POST` with payload and `return`s obj

## API Authentication and Authorization

- *authentication* - validating a request is from who it says it's from
  - first req/res: **client** _request_ <=> **server** _response_ loop
- *authorization* - validating a request has the correct permissions to access the resource
  - subsequent req/res: **client** _request_ <=> **server** _response_ loops

### Two core types:

1. Stateful (cookie-based, single-server approach, user info on server)
   - Benefits
     - Simple for single-server applications
2. Stateless (token-based, multi-server approach, user info in token)
   - Benefits
     - multi-service auth (ex. a company's bank _and_ retirement web apps)
     - multi-server auth (server crash and load balancer rebalance doesn't kill session)

#### In detail:

1. Stateful (cookie-based, single-server approach, user info on server)
   1. **client** _request_ (username + password via https)
   2. **server** authenticates _request_ (username + password via hashing)
   3. **server** _response_ contains `sessionId` (random hash string) to use as the "validation key" for this **client**'s subsequent requests (aka session)
      - stateful = **server** stores `sessionId` in db or memory
      - one-server approach
   4. **client** handles _response_ and stores its `sessionId` in cookie store (typically browser)
   5. subsequent **client** _request_(s) pass along `sessionId` in `request.headers["Cookie"]`
   6. **server** validates _request_ using `sessionId` until **server** invalidates session (timeout expires so **server** removes `sessionId` from db or memory—whichever was used)
2. Stateless (token-based, multi-server approach, user info in token)
   1. same as stateful
   2. same as stateful
   3. **server** _response_ contains `JWT` (JSON Web Token) to use as the "validation key" for this **client**'s subsequent requests (aka session)
      - stateless = **server** used _secret_ to validate JWT came from this server (or any other controlled server that shares the _secret_)
      - multi-server approach
   4. **client** handles _response_ and stores its `JWT` however it wants (local storage, memory, cookie, etc.)
   5. subsequent **client** _request_(s) pass along `JWT` in `request.headers["Authorization: Bearer"]`
   6. **server** validates _request_ using `JWT` deserialization using _secret_ (to verify untampered) until **server** invalidates session (`eat` expires at timestamp, `exp` expires timestamp)
   
### Examples:

#### Stateful

```typescript
TODO axios
```

```typescript
TODO fetch
```
