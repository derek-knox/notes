# [Let's build GPT: from scratch, in code, spelled out. by Andrej Karpathy](https://www.youtube.com/watch?v=kCc8FmEb1nY)

## Links

- [nanoGPT](https://github.com/karpathy/nanoGPT)
  - > The code itself is plain and readable: `train.py` is a ~300-line boilerplate training loop and `model.py` a ~300-line GPT model definition, which can optionally load the GPT-2 weights from OpenAI. That's it.
- [Attention Is All You Need](https://arxiv.org/abs/1706.03762)
  - Seminal AI paper introducing Transformer
- [SentencePiece](https://github.com/google/sentencepiece)
  - Google's unsupervised tokenizer and detokenizer
- [tiktoken](https://github.com/openai/tiktoken)
  - OpenAI's byte-pair-encoding tokenizer

## Overview

A GPT (generative pretrained transformer) is a neural network leveraging the Transformer (see [Attention Is All You Need](https://arxiv.org/abs/1706.03762)) in its architecture. At it's core, a GPT excels at _next token prediction_ where tokens are often word chunks—in the context of LLMs—but can be anything (pixels, sound data, etc.) based on modality. This fact alone has proven them extremely powerful and useful.

An emergent and valuable property of GPTs is that the _initial tokens can be seeded_. Some common seedings are:
1. Imbued expertise and/or personality (programmer, doctor, Dr. Seuss etc.) which results in valuable output (such as programs, expert advice, creatively toned stories, etc.)
2. RAG (retrieval augmented generation) so an LLM can combine it's pretrained and pre-baked weights and biases as _long-term static memory_ with conditional _short-term dynamic memory_ it didn't have access to during training (structured and unstructured data—private, current, etc.)
3. Tools and tasks so an LLM knows it can leverage external tooling (web search, custom APIs, etc.) and/or task-based strategies (chain-of-thought)

## Terminology

## Tokens & Embeddings

- *token* - unit of vocabulary
  - sub-word in typical LLMs but can be per character, sentence, etc.
  - pixel blocks/kernels, sub-sound wave, etc. for non-natural language based vocabularies
- *vocabulary* - token set
- *tokenize* - process mapping each token to a unique integer
  - `encode`: text to integer
  - `decode`: integer to text
- *tokenization* - tokenize strategy
  1. character - one to one char to integer mapping
  2. subword - character sequence to integer mapping (most common strategy)
     1. *byte-pair-encoding* - iteratively merges the most frequent adjacent character pairs into subword units, balancing vocabulary size and model efficiency (well suited to handle rare and frequent words effectively)
        - Used by Google's [SentencePiece](https://github.com/google/sentencepiece) and OpenAI's [tiktoken](https://github.com/openai/tiktoken)
     2. *unigram language model* - probabilistic model, where a fixed-size vocabulary is optimized by retaining subwords that maximize likelihood (well suited for multilingual models)
        - Used by Google's [SentencePiece](https://github.com/google/sentencepiece)
  3. word - word to integer mapping
- *token embedding table* - a matrix (tensor) where each row corresponds to a dense vector representation of a token in the vocabulary
  - "learned features" result during training and are encoded in each token's embedding
- *embedding* - token as dense vector
- *embedding dimension* - number of dimensions used to encapsulate a token's meaning in the context of the entire training set
  - *channels* - conceptually analogous to embedding dimension as both represent the dimensionality of the features being processed
- vector types:
  1. *dense vector* - encodes compact, often overlapping, information-rich representation—embeddings are dense for this reason
  2. *sparse vector* - encodes high-feature, low-overlapping representation—useful in non-numeric, discreet features (labels, categories, etc.)

## Training & Inference

- *block* - a contiguous segment of tokens within a single example/sequence
  - used to handle long sequences by splitting them into manageable chunks for models with fixed context sizes  
- *block size* - maximum number of tokens (context length) per prediction  
  - an emergent benefit during training is that there are `block_size - 1` training examples per block which additionally trains the model to be more resilient as it must perform with minimal _and_ maximal context
- *batch* - a collection of examples/sequences processed in parallel during training or inference  
  - enables efficient utilization of computational resources, particularly with hardware accelerators like GPUs and TPUs
  - `torch.stack` concatenates a sequence of tensors along a new dimension, creating a larger tensor—the batch
- *batch size* - fixed number of examples/sequences processed in parallel
  - batching benefits scale with hardware (CPU -> GPU -> TPU)
- *logits* - the raw, unnormalized outputs of a neural network—aka the scores
  - typically produced by the final linear layer before applying an activation function like softmax or sigmoid
- *probabilities* - the normalized results after applying the activation function to the logits
  - we train NNs to attain accurate probability proposals during inference

## Tensors

- *Hidden States Tensor* - the intermediate tensor encapsulating the `BxTxC` (**b**atch: batch size, **t**ime: example sequence length, **c**hannel: embedding dimension)
  - aka *Feature Map*, and *Sequence Batch Representation*
  - *Input Tensor* is also synonymous but refers to the initial tensor prior to training mutating the hidden state
- *Logits Tensor* - the encoded unnormalized scores for each token in the vocabulary at every sequence position
  - this results from applying a linear transformation to the Hidden States Tensor: `logits = (hidden states * W^T) + b`

# Transformer

- *self-attention* - attained property of a block at token t to attend to preceding tokens in the block
  - TODO confirm attend def here
- *bag of words* - embedding average over the block context (current and preceding tokens of a block) as a 2D tensor
  - this 2D tensor stores the attention/affinities of previous tokens while masking out future tokens
  - `torch.tril` in combo with normalization and batch matrix multiplication (via `@`) is a mathematical trick/optimization calculating the BoW avg.
    ```python
    # Main version
    weights = torch.tril(torch.ones(T, T))
    weights = weights / weights.sum(1, keepdim=True)
    x = torch.rand(B, T, C)
    bow = weights @ x # (B, T, T) @ (B, T, C) -> weighted (B, T ,C)

    # Softmax version
    tril = torch.tril(torch.ones(T, T))
    weights = torch.zeros((T, T))
    weights = weights.masked_fill(tril == 0, float("-inf"))
    weights = F.softmax(weights, dim=-1)
    bow = weights @ x
    ```
- TODO PLANE OFFLINE - what is the block size of ChatGPT 2, +? is it the same as context size sub the fact that context size is inclusive of input and output tokens?