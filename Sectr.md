# Sectr

A tool for defining spaces (sectors) and the connections (portals) between them that can be dynamically streamed at runtime.

- [Sectr Generation Tool](https://gitlab.com/derek-knox/sectr-generation)

## General

- *Sector* - a collection of GameObjects to stream as a unit
    - Must be marked static for streaming (this saves the bounding volume recalculating)
    - Sector membership is not exclusive. Members may be in multiple Sectors at once, so they may overlap and even be nested in one another, but never parented in one another.
- *Member* - object in a sector    
- *Portal* - a trigger volume used to help determine which sectors to load
    - Should be adjacent to sectors and never parented in them
- *Loader* - a component responsible for streaming the sectors in and out, there are various types:
    - *Neighbor* - a loader target that uses the playable character(s) as the target transform to use against a portal
- *Chunk* - an auto generated scene file that houses all the GameObjects of a particular sector
- *Graph* - the resulting graph that describes the sector and portal relationships
- *Flags* - a mechanism for adding state to a portal, the standard flags are:
    - *Pass Through*: Ignores the geometry when computing visibility.
    - *Closed*: Treats the portal as inactive for the purposes of visibility calculations.
    - *Locked*: Identifies the portal as logically locked. Currently unused.
- *member*
	- The component keeps track every frame of which Sectors it is in currently and publishes the info to any listener interested
	- If an object needs to know about Sectors but is not itself a Sector, then it should have a Member component added to it. The camera, player objects, and enemy objects are the common expected members as they change sectors at runtime
- *hibernator* - allows a global object that resides in a sector to hibernate until that sector is again loaded

## Streaming

- lightmaps can be streamed but light probes and nav meshes cannot (usually not a big memory impact)
- multiple loaders dual requesting streams is OK and Sectr will do the right thing based on the graph
- neighbor loader: Note that a MaxDepth greater than 0 requires that your Sectors be connected by Portals, though the exact geometry of the Portals does not matter
