# The Design of Everyday Things by Don Norman

## The Psychopathology of Everyday Things

- Features arise out of technology progression. The main problem companies and products have with respect to adding features is they don't provide user feedback when a user interaction occurs. Ensure when adding features you also create user feedback for their usage.

## The Psychopathology of Everyday Actions

- Principles of good design:
    1. _Visibility_ - By looking, the user can tell the state of the device and the alternatives for action
    2. _Conceptual Model_ - The designer provides a good conceptual model for the user, with consistency in the:
       1. presentation of operations
       2. results
       3. a coherent, consistent system image
    3. _Mappings_ - It is possible to determine the relationships between:
       1. actions and results
       2. the controls and their effects
       3. the system state and what is visible
    4. _Feedback_ - The user receives full and continuos feedback about the results of actions.
- Ease of use questions - How easily can one:
    1. Determine the function of the device?
    2. Tell what actions are possible?
    3. Determine mapping from interpretation to physical movement?
    4. Perform the action?
    5. Tell if the system is in desired state?
    6. Determine mapping from system state to interpretation?
    7. Tell what state the system is in?

## Knowledge in the Head and In the World

- People have two kinds of knowledge (information):
    1. Declarative (knowledge _of_) - knowledge of facts and rules
    2. Procedural (knowledge _how_) - knowledge manifested through action and procedure (largely subconscious)
- In general, people structure the environment to provide us considerable amount of the info they'll need for something to be remembered
- It is a general property of memory that we store only partial descriptions of the things to be remembered. Descriptions that are precisely viable at the time of learning may not work later on when new experiences have been encountered and entered in memory
- Human memory is knowledge in the head. It exists in a number of categories. Three important ones are:
    1. _Memory of arbitrary things_ - lack meaning and relationship to one another and things already knows
    2. _Memory of meaningful relationships_ - items to be retained from meaningful relationships with themselves and other things already known
    3. _Memory through explanation_ - Not remembered but derived through explanatory mechanism
- Designers should provide users with appropriate mental models when not supplied. People are likely to make up inappropriate/subpar ones.

## Knowing What to Do

- The difficulty of dealing with novel situations is directly related to the number of possibilities
- There are four classes of constraints that can guide action:
    1. _Physical_ - with proper use there should be a limited number of possible actions (ideally one)
    2. _Semantic_ - dependent on knowledge of situation and of the world
    3. _Cultural_ - set of allowable actions for social situations
    4. _Logical_ - spatial or functional layouts or relationships
- Common problems often have simple solutions, the key is to properly exploit affordances and natural constraints


## To Err is Human

- Use the power of the natural and artificial constraints—physical, semantic, cultural, and logical—to guide human behavior
- Use forcing functions and natural mappings to minimize opportunity for error

## The Design Challenge

- "Design is the successive application of constraints until only a unique product is left"
- Be weary of "feature creep". Less is more, but if new additions are necessary (client request, etc.) be intelligent about their organization
- Requirements for designing an explorable system:
    1. _Visibility_ of allowable actions (no "code" needed to execute a task)
    2. _Effect_ of user action must be visible and easily interpreted (allowing user to generate a mental model of action-outcome relationships)
    3. _Cost-free_ actions equate to a user reversing action without losing progress. You can also make the action not "explorable" with clear indication of results
- Make the computer system invisible (system interaction)

## User-centered Design

- Boiled down, design should allow the user to:
    1. Figure out what to do
    2. Tell what is going on
- Good design exploits constraints so that the user feels there is only one possible thing to do (action to take)—the correct one
- The designer must understand and exploit natural constraints, make use of forcing functions, and produce visible outcomes of actions
