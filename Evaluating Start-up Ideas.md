via https://twitter.com/lennysan/status/1275819005568118784

---


What to look for when evaluating a start idea

1. 🤤 Product-Market Fit
2. 🌎 Market size
3. 💥 Why now
4. 👋 Distribution
5. 🤝 Team
6. 🛡️ Moat
7. ⚙️ Business model

(read on)

1/ 🤤 Product-Market Fit

“The only thing that matters is getting to product-market fit.” ー @pmarca

What to look for:
- ✅ High cohort retention
- ✅ Passionate user feedback
- ✅ High MoM and YoY growth rate
- ✅ A product that’s 10x better than the alternative
- ✅ Willingness to pay

2/ 🌎 Market size

“When a great team meets a lousy market, market wins. When a lousy team meets a great market, market wins. When a great team meets a great market, something special happens.” ー @arachleff
2/ What to look for:
- ✅ Is there a $1B/year revenue opportunity
- ✅ A customer base desperate for something new
- ✅ A growing customer base being dramatically underserved
- ✅ Over $10b TAM
- ✅ If the initial market is small, then large adjacent markets

3/ 💥 Why now

“The thing that I like to assume is every startup idea’s been tried … So, the question is not, has my idea been tried before? The question is, is it the right time for my idea to happen?” — @m2jr
3/ What to look for:
- ✅ An inflection in technology
- ✅ An inflection in adoption of a technology
- ✅ A change in regulation
- ✅ A change in a long-held belief
- ✅ A new distribution channel has opened up
- ✅ Costs have fallen or price points risen dramatically
 
4/ 👋 Distribution

“Most businesses actually get zero distribution channels to work. Poor distribution — not product — is the number one cause of failure.” — @peterthiel
4/ What to look for:
- ✅ A clear growth strategy
- ✅ A unique growth strategy
- ✅ Unique access to to the target audience
- ✅ A new untapped distribution channel
- ✅ High LTV/CAC ratio

5/ 🤝 Team

“If you think about how do you get to the big idea, we call it an Earned Secret. You did something in your past, you tried to solve some hard problem, and you learned something about the world that not a lot of people in the world know.” – @bhorowitz
5/ What to look for:
- ✅ A+ founders
- ✅ Founders who deeply understand the customers problem
- ✅ Founders who have a unique insight or an “earned secret” about the opportunity
- ✅ Founders who have worked together before
- ✅ Founders who move fast

6/ 🛡️ Moat

“Don’t be the best. Be the only.” — @m2jr

What to look for:
- ✅ Are there network effects
- ✅ Are there economics of scale
- ✅ Are there high switching costs
- ✅ Is there a potential for a strong brand
- ✅ Is there exclusive supply
- ✅ Is there proprietary technology

7/ ⚙️ Business model

“It’s best to invest in pure tech companies. Ask: How much of every dollar invested is going into technology development and how much is going to things like scaling, customer acquisition, subsidizing rides or deliveries, or some physical product.” — @naval
7/ What to look for:
- ✅ High margins
- ✅ Low CAC, high LTV
- ✅ Short sales cycle
- ✅ High cash flow
- ✅ Positive unit economics
