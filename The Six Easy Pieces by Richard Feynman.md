# The Six Easy Pieces by Richard Feynman

## Atoms in Motion

- Matter is made of atoms so everything is physics
    - Atoms are the "what" and the laws of physics are the "how"
- An *atom* is just a unit comprised of a certain number of protons, neutrons, and electrons
    - An *element* is a specific substance and refers to the specific numbers that define a specific atom
    - An *ion* refers to an atom that has gained or lost an electron
    - An atom's size is an apple compared to the apple scaled to earth
    - Atoms jiggle constantly (*brownian motion*) and their amplitude maps to heat
        - Compression increases atomic interaction and thus increases heat and pressure
- A *molecule* is just two or more atoms chemically joined
    - A *compound* is a specific substance comprised of two or more elements combined
    - Solids, liquids, and gases are molecular patterns that are more to less crystaline (static grid to dynamic gridless)
- A *chemical reaction* is when atoms and ions change combinations forming new molecules

## Basic Physics

- Forces
    - Electro (near-field)
    - Gravity (far-field)
- An *electromagnetic field* is the volumetric medium through which electric waves propogate as to encapsulate the potentiallity for producing a force
    - Magnetism refers to *charges in relative motion*
    - Waves are differntiated by oscillation frequency (think atom:wave::electron-count:oscillation-frequency)
- *Quantum mechanics* - mathematical description of motion and interactions of subatomic particles
    - "The description of the behavior of matter in all its details and, in particular, of the happenings at the atomic scale"

## The Relation of Physics to Other Sciences

- *Chemistry* as the study of the core interaction of physics' particles
    - *Statistical mechanics* as a guide for interaction probability due to atomic jiggle preventing absolute precision in prediction
- *Biology* as the study of the complex molecular patterns in organics (lifeforms)
- *Astronomy* as the study of the basic atoms and molecules in context of universe-scale and nuclear forces
- *Geology* as the study of atoms and molecules comprising inorganic matter and the physicality of planets and rocks
- *Psychology* as the study of atoms and molecules in context of lifeform behavior

## Conservation of Energy

- Refers to the numerical "energy" quantity that does not change, it is constant and is simply "reparented" during reactions

## The Theory of Gravitation

- Gravitation is the macro force that attracts where the electrical force is the micro that repels
    - A *unified theory* represents an equation that accounts for both at once

## Quantum Behavior

- The *uncertainty principle* refers to an observation (via photon contact) of electrons can make one "trigger" such that it constitutes the particle behavior of the particle-wave duality. An electron not "triggered" this way ensures its behavior is the wave of the duality.
    - Technically "One cannot design equipment in any way to determine which of two alternatives is taken, without, at the same time, destroying the pattern of interference."