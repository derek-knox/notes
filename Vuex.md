# Vuex

Vuex provides an API for creating reactive stores that maintain state in an application. The larger the application the more useful having multiple stores (modules) is. A store is comprised of four parts:
1. state
    - the reactive representation of one or more stores' data used in an application
    - A Vue root component's `store` option (enabled via `Vue.use(Vuex)`) injects the store instance in all child components
        - a component can consume a store's state via a `computed` property in conjunction with the injected instance via `this.$store`
        - the `mapState` helper generates computed getter functions for us when a component consumes many store properties
1. mutations
    - tracked and managed synchronous state changes ensuring efficient and automatic UI reactivity and rerendering
    - use `store.commit('myMutationHandlerName', payload)` to commit mutations as they are not called directly
    - prefer initializing state with all properties to track, but use the `Vue.set` API for those appended after initialization
1. actions
    - a store's API for committing synchronous (or an arbitrary amount of asynchronous) mutations
        - use `this.$store.dispatch` from within a Vue component to trigger a store's action
        - the `mapActions` helper maps component methods to `store.dispatch` calls
1. getters
    - a store's API for consuming and rendering computed properties of state
    - computed properties are cached and only reevaluated if a depency changes (optimization benefit of getter approach)
        - the `mapGetters` helper maps store getters to local computed properties

Example Store:
```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  actions: {
    asyncIncrement (context) {
      someApi.getTheThing(someData)
        .then((response) => {
          context.commit('increment')
        });
    }
  },
  getters: {
    totalCountMessage: state => {
      return 'The toal is ' + state.count + '.';
    }
  }
})
```