# The Lessons of History by Will Durant

## History and the Earth
- The influence of geographic factors diminishes as technology grows.

## Biology and History
- War is a nation's way of "eating."
- Life consists of:
  1. Competition
  2. Selection
  3. Breeding
- Economic development specializes functions, differentiates abilities, and makes men unequally valuable to their group. If we knew our fellow men thoroughly, we could select 30% of them whose combined ability would equal that of the rest.

## Race and History
- It is not the race that makes the civilization; it is the civilization that makes the people. Circumstances—geographical, economic, and political—create a culture, and the culture creates a human type.
- "The Englishman does not so much make English civilization as it makes him; if he carries it wherever he goes and dresses for dinner in Timbuktu, it is not that he is creating his civilization there anew, but that he acknowledges even there it has mastery over his soul."

## Character and History
- **Table of Character Elements:**
  1. Action vs. Sleep
  2. Fight vs. Flight
  3. Acquisition vs. Avoidance
  4. Association vs. Privacy
  5. Mating vs. Refusal
  6. Parental care vs. Filial dependence
- History in the large is the conflict of minorities; the majority applauds the victor and supplies the human material for social experiment.

## Morals and History
- Hunting, agriculture, and industry each embodied moral codes that conflict based on the lived experience of each time. One’s predisposition would advantage them in one time and disadvantage them in another.
- The capacities for violence, sex, and love have persisted across time, but their over-indexing advantaged certain groups depending on the time period.

## Religion and History
- Belief in God is a blip within our brief existence in the cosmos.
- Dualism (good and bad spirits), rather than monotheism, has been the prevailing theology throughout history.
- Napoleon: Belief in God has kept the poor from murdering the rich.
- Education, once the sacred province of god-inspired priests, has become the task of men and women shorn of theological robes, relying on reason and persuasion to civilize young rebels who respect only the policeman and may never learn to reason at all.
- Generally, religion and puritanism prevail when laws are feeble and morals must bear the burden of maintaining social order; skepticism and paganism progress as the power of law and government rises, permitting the decline of church, family, and morality without endangering the stability of the state.
- "As long as there is poverty there will be gods."

## Economics and History
- "The men who can manage men manage the men who can manage only things, and the men who can manage money manage all."
- Every economic system must sooner or later rely upon some form of the profit motive to drive productivity. Substitutes like slavery, police supervision, or ideological enthusiasm prove too unproductive, too expensive, or too transient.
- In progressive societies, the concentration of wealth may reach a point where the many poor rival the few rich; the unstable equilibrium generates a critical situation, which history has met by legislation redistributing wealth or by revolution distributing poverty.
- Economic history is the slow heartbeat of the social organism, a vast systole and diastole of concentrating wealth and compulsive recirculation.
- **Solon's Tax:** (confirm if he was the first to implement the concept of a graduated income tax).

## Socialism and History
- The fear of capitalism has compelled socialism to widen freedom, and the fear of socialism has compelled capitalism to increase equality.

## Government and History
- The prime task of government is to establish order; organized central force is the sole alternative to incalculable disruptive force in private hands.
- Monarchies are the historical norm; democracies are hectic interludes.
- Most governments have been oligarchies, ruled by a minority—either by birth, as in aristocracies, or by a religious organization, as in theocracies, or by wealth, as in democracies.
- Wealth is a trust (the "credit system") in men and institutions rather than intrinsic value in paper money or checks; violent revolutions redistribute wealth more by destruction than reallocation. Natural inequality soon recreates an inequality of possessions and privileges, raising a new minority with the same instincts as the old.
- If the economy of freedom fails to distribute wealth as well as it creates it, the path to dictatorship will be open to anyone who can persuasively promise security to all; and a martial government, under charming phrases, will engulf the democratic world.

## History and War
- In the last 3,421 years of recorded history, only 268 have seen no war.
- War is acknowledged as the ultimate form of competition and natural selection in the human species.
- Perhaps humanity is moving toward a higher plateau of competition, where we might eventually encounter ambitious species on other planets or stars. Interplanetary war may be the ultimate unifier for humanity.

## Growth and Decay
- As life overrides death with reproduction, an aging culture hands its patrimony down to its heirs across time and space.

## Is Progress Real?
- History is so richly varied that almost any conclusion can be supported by selective examples.
- **Progress**: the increasing control of the environment by life.
- Once, colleges were luxuries for the male leisure class; today, universities are so numerous that anyone may become a Ph.D.
- History, above all, is the creation and recording of heritage; progress is its increasing abundance, preservation, transmission, and use.
