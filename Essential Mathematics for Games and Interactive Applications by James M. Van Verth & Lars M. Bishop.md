# Essential Mathematics for Games and Interactive Applications by James M. Van Verth & Lars M. Bishop

## Vectors and Points

- *Vector* = change/difference in point(s) (giving it length and direction)
    - *unit vector* = vector with length one
- *Point* = a location in a particular space (typically 2D & 3D space in games)
- *Scalar* = a vector length multiplier
- Rules:
    - Addition
        1. *commutative*: variable order change = same result
        2. *associative*: variable grouping change = same result
        3. *additive*: adding zero = same result
        4. *additive inverse*: adding negation = zero
    - Multiplication
        1. *associative*: variable grouping change = same result
        2. *distributive*: variable distribution = same result
        3. *multiplicative identity*: multiplying one = same result
- *Vector space* = vector set adhering to aforementioned addition and scalar multiplication rules resulting in *closure* (aka vector results stay in the space).
    - Useful for representing:
        1. geometric entities (2D/3D computer graphics)
        2. higher-dimension entities (machine learning)
        3. vector-like entities (matrices and linear transformations)
    - *subspace* - a subset of the parent vector space
- *linear combination* - the resulting vector set of multiplying a scalar set and a vector set (*think `.zip()`*)
    - *span* - all the possible linear combination*s* of a vector set (*think "surface"*)
    - *linearly dependent* - vector set remaining in a span given a particular linear combination (*think "surface parallelism"*)
    - *linearly independent* - vector set where at least one element escapes linear dependence (*think "parallelism escape"*)
        - *basis* - vector set that both spans and is linearly independent (*think "uniform parallelism escape"*) where a *basis vector* is one element. Special because:
            1. any vector in the span is producible via a linear combination of the basis
            2. only one scalar set of a linear combination can produce a given vector in the span
    - *dimension* - required basis count to span (2D = two basis vectors to span the space)
        - *component* - the element slot of a vector (x component = `1` & y component = `0` in `(1,0)`)
        - *standard basis* - for each basis its primary component is unit length and remaining are zero (2D = `(1,0), (0,1)` & 3D = `(1,0,0), (0,1,0), (0,0,1)`)
- *norms* - class of generalized size-measuring functions (*think length/magnitude*)
    - *L1*: *Manhattan distance* - sum of all vector components as absolute values (*think "Manhattan as following street grid*)
    - *L2*: *Euclidean* - pythagorean theorem
    - *normalize* - make unit vector from general vector
- *dot product* - function that multiplies vectors componentwise
    - `dot(v, w) = v0w0 + v1w1 + v2w2`
    - most useful vector operation in 3D games:
        - angle measurement:
            - below 90° - `v * w > 0 = θ < 90°`
            - above 90° - `v * w < 0 = θ > 90°`
            - is 90° - `v * w == 0 = θ == 90°`
            - angle comparison - test if two unit vectors are pointing in same general direction
        - projection - length of vector v pointing in same direction as vector w (*think "projection shadowing*)
            - requires one normalized vector
- *cross product* - function that returns the orthogonal vector of its vector arguments
    - `cross(v, w) = (vx*wz,vz*wx,vx*wx) - (wy*vz,wz*vx,wx*vy)`
    - useful for:
        - determining surface normal
        - like dot product (but typically cheaper computationally) can determine if vectors are parallel
- common coordinate systems:
    - *cartesian* - 2D and 3D points defined by scalar tuples `(x,y)` and `(x,y,z)`
    - *polar* - 2D points defined by radius (`r = sqrt(x,y)`) and one angle (`θ = atan2(y,x)`)
        - `atan2(y,x)` - returns angle `θ` between ray (origin -> `(x,y)`) & positive x-axis
    - *spherical* - 3D points defined by radius (`r = sqrt(x,y,z)`) and two angles (`θ = atan2(y,x)` & `ϕ = atan2(sqrt(x,y),z)`)
- *ray* - a clamped line (typically clamped at zero to infinity)
- *tris* over *quads*:
    - tri vertices guarantee a single plane aka coplanar (unless they're colinear = "degenerate triangle")
    - minimalist in comparison results in simpler downstream parsing/processing
    - are always convex polygons (assuming not colinear verts)
