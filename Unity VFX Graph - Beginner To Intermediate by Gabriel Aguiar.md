# Unity VFX Graph - Beginner To Intermediate by Gabriel Aguiar

## Graph Anatomy (Default *Contexts*)

1. *Spawn*
   - Particle start count
   - Constant rate vs. single burst vs. periodic bursts
2. *Initialize*
   - Particle capacity
   - Spawn attributes
3. *Update*
   - Update attributes continuously
4. *Output*
   - Visual aspects (billboard, mesh, etc)
   - Shader properties

## Notes

- Check the "Use Soft Particle" in the *Update* context so transparent particle blend with intersecting objects vs. having hard clipping
- Use *Simple Particle System* as a template to begin customizing a particle system that has a prebuilt graph of the four anatomical units
- With the "Spawn" container block (other contexts and blocks have this too, just with different settings) selected you can update settings in the Inspector tab:
    1. Loop Duration (Infinite, Constant, Random)
    2. Loop Count (Infinite, Constant, Random)
    3. Delay Mode (None, Before Loop, After Loop, Before And After Loop)
- The Initializer "Bounds" need to be within the camera frustum otherwise the particle system is culled from rendering
- Graph properties are easy to create and can easily be used to modify particle systems as they show up in the Inspector (via `Visual Effect` component with the "Asset Template" matching the graph asset)
- To create tileable textures in Photoshop use "Filter > Other > Offset" to offset the width of the texture and then use the healing brush to clean the seam. Revert the offset and repeat for height.
- Blocks can be copied between graphs by holding ctrl while click and dragging
- *Area of Effect* - AOE is a term referring to an effect along a surface (often the ground)
- Use curves for size, position, etc. and gradients for colors to animate changes over time in a clean and flexible way
- *Set Tex Index Over Life* - block that  ensures a UV Mode set to Flipbook (where Flipbook Blend is smoother as Unity generates inbetween frames) matches the frame by frame animation of a texture sheet
- When using graph properties as inputs to blocks it's often useful to use *Add* and *Multiply* nodes to futher modify the value prior to input of the block (this way you have an SSoT graph property that can feed multiple blocks where overriding certain uses manifests as the add or multiply)
- Use *Set Size|Count|Color|etc.* blocks prior to the equivalents *... Over Lifetime* blocks so you have an initial setting that can then mutate over time
- Select the VFX Graph asset and update the "Output Render Order" to properly modify the render stacking order
- Use *Orient* block with mode "Along Velocity" for particles to angle in their movement direction
- Use *Set Position (Shape Sphere)* for spawning along sphere (this is useful in combo with the "Update" context having a *Conform to Sphere* block for effects where energy is "absorbed in" vs. "emitted out")
  - Additionally a *Kill Sphere* block can be used when a moving target is the source of absorbtion such that the particle dies making it look like it was absorbed
- Use *Turbulence* block for chaotic swarm-like particle motion
- Distortion of a ring is a simple and beautiful effect for drawing attention to the object is surrounds
- A common pattern for random values for blocks:
    1. Have a float graph property
    2. Wire from the graph property directly to the random block `B` input (max)
    3. Create a multiply node
    4. Wire from the graph property to multiply's `A` input with a value of 1
    5. Change the multiply's `B` input to less than 1
    6. Wire the multiply output to the random block `A` input (min)
- The *Random Number* node (with a "Seed" of "Per Particle") greatly improves input value variety
- The *Sample Curve* node is useful for matching particle positions to another single particle whose position is driven by the curve
- Use *Get Attribute: spawnindex* node in combo with a *Divide* node and a *Compare* node to selectively modify specific particles. For example if you had two particles where their rotations of 0 and 180 were useful to create a full circle effect, you could conditionally rotate each to create the circle. Spinning effects using this look awesome where positive and negative distortions can appear like vortex liquids.
