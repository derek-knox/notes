# [Smolagents by Hugging Face](https://huggingface.co/blog/smolagents)

## Agents

- *ai agent* - program where LLM output controls the workflow
  - > "Agentic programs are the gateway to the outside world for LLMs."
  - My original definition was "a program using a specific set of (LLM) model, system prompt, and tools in combo with iteration (looping, recursion, and/or automation)"
    - n number of these in coordination makes it agentic/multi-agent
- *agency* - the level of impact on program control flow (continuous spectrum)
    | Agency Level | Description                                    | How that's called             | Example Pattern                                        |
    |--------------|------------------------------------------------|-------------------------------|-------------------------------------------------------|
    | ☆☆☆          | LLM output has no impact on program flow       | Simple processor              | `process_llm_output(llm_response)`                   |
    | ★☆☆          | LLM output determines basic control flow       | Router                        | `if llm_decision(): path_a() else: path_b()`         |
    | ★★☆          | LLM output determines function execution       | Tool call                     | `run_function(llm_chosen_tool, llm_chosen_args)`     |
    | ★★★          | LLM output controls iteration and program continuation | Multi-step Agent              | `while llm_should_continue(): execute_next_step()`    |
    | ★★★          | One agentic workflow can start another agentic workflow | Multi-Agent                  | `if llm_trigger(): execute_agent()`                  |
- *agentic* - workflow using at least one agent
- *multi-step agent* - workflow iteration
    ```python
    memory = [user_defined_task]
    while llm_should_continue(memory): # this loop is the multi-step part
        action = llm_get_next_action(memory) # this is the tool-calling part
        observations = execute_action(action)
        memory += [action, observations]
    ```
- *multi-agent* - workflow using 2+ agents
  - typically where one can trigger the other

## Agents Usage

- Question: Do I really need flexibility in the workflow to efficiently solve the task at hand?
  - No: hand code (deterministic code flows)
  - Yes: consider agentic (non-deterministic code flows)

## Code Agents

- *code agent* - agent that writes its actions in code (as opposed to "agent being used to write code")
  - an agent that opts for generating and executing code in pursuit of results vs. chatty tool call sequences
  - Tool calls are still used, but they're instead inlined in code and executed (thus optimizing away latency of req/res flows)
- *tools* - > "writing actions as a JSON of tools names and arguments to use, which you then parse to know which tool to execute and with which arguments"
- The argument for code agents is: > "we crafted our code languages specifically to be the best possible way to express actions performed by a computer. If JSON snippets were a better expression, JSON would be the top programming language and programming would be hell on earth."
- Actions-in-code vs. JSON-like snippets benefits:
    1. _Composability_ - could you nest JSON actions within each other, or define a set of JSON actions to re-use later, the same way you could just define a python function?
    2. _Object management_ - how do you store the output of an action like generate_image in JSON?
    3. _Generality_ - code is built to express simply anything you can have a computer do.
    4. _Representation in LLM training data_ - plenty of quality code actions is already included in LLMs’ training data which means they’re already trained for this!

## Smolagents - Making Agents Simple

- *smolagent* - abstraction enabling code agents while still supporting `ToolCallingAgent`
  - Requirements:
    1. _tools_ - a list of tools the agent has access to
    2. _model_ - an LLM that will be the engine of your agent