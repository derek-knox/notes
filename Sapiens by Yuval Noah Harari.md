# Sapiens by Yuval Noah Harari

## Timeline (yrs from present)

- 13.8B Universe forms
- 4.5B Earth forms 
- 3.8B Organisms form
- 6M Humans + chimpanzees last common ancestor
- 2.5M Humans evolve in Africa. Stone tools use.
- 2M Humans migrate. Evolution of different human species.
- 500K Neanderthals
- 300K Daily fire use
- 200K Homo sapiens
- 70K Fictive language tool. First history.
- 45K Sapiens settle Australia. Australian megafauna extinction.
- 30K Neanderthals extinction.
- 16K Sapiens settle America. American megafauna extinction.
- 13K Homo floresiensis extinction. Homo sapiens only human species left.
- 12K Agricultural revolution. Plant/animal domestication. Permanent settlements.
- 5K First kingdoms, script/writing, money. Polytheistic religions.
- 2.5K Invention of coinage. Universal money. Persian empire. Buddhism.
- 2K Han empire in China. Roman empire in Mediterranean. Christianity.
- 1.4K Islam.
- 500 Scientific revolution. Capitalism.
- 200 Industrial revolution. Massive extinction of plants/animals.
- 0 Escape Earth confines. Nuclear war technology. Internet.

## An Animal of No Significance

- 300K yrs ago fire used daily and cooking as technology:
    - Chimpanzees require `5` hours of chewing raw foo compared to humans `<1` cooked food
    - Expanded what could be consumed as cooking eased consumption of otherwise difficult to consume food
    - Killed parasites and bacteria which decreased food-borne sickness

## The Tree of Knowledge

- 70K-30K yrs ago was the Cognitive revolution where homo sapiens mental capabilities crystalized
    - Shared fictions with language were the lever empowering homo sapiens and accelerating their power in the world
    - Dual realities surfaced:
        - Objective (real world)
        - Fictional (shared understanding above the objective world enabling trust between strangers and massive populations)

## A Day in the Life of Adam and Eve

- The "Stone Age" should really be called the "Wood Age" as most tools were of wood where only stone tools bypassed biodegradation

## The Flood

- Human colonization of new lands coincided with megafauna extinctions all over the world (we're very likely to blame)
- Large animal hunting began ~400K years ago
- In America, giant 20ft tall ground sloths, bear-sized rodents, and native American camel were some of the megafauna to go extinct at the hands of homo sapiens

## History's Biggest Fraud

- The Agricultural Revolution of ~12K yrs ago coincided with mass animal/plant domestication. Though the gains for homo sapiens as a whole were immense, individuals were worse off in comparison to their forager counterparts:
    - Weakened immune system due to less dietary variety
    - Humans as slave to their crops vs. the otherway around
    - Increase in new ailments (back pain, hernias)
- The Agricultural Revolution is a point of no return (without mass life loss) as humans collectively now depend upon this mass production of food

## Memory Overload

- The Summarians invented writing ~5K yrs ago for record keeping (ledger for taxes, debt, and ownership). Their writing system was comprised of two symbol types:
    1. Numbers - 1, 10, 60, 600, 3600, 36000 (combo of base-6 and base-10)
    2. Nouns - People, merchandise, animals, territory, dates, etc.
- Summarians used clay tablets and pre-Columbian Andeans (and later the Inca) used quipus (kee-poo) - knotted threads of varying color
- The Hindu invented numerals where the Arabs built upon them to create "algebra"

## The Arrow of History

- Native Americans didn't have horses until 1492
- The arc of human history is one of a convergence toward unity. Specifically, there are three unifying orders where the world population:
    1. Monetary: is one market, each human a potential customer
    1. Emperial: is one empire, each human a conquerable subject
    1. Religious: is one truth, each human a convertable believer

## The Scent of Money

- Money was and has been the single most useful abstraction for goods and services exchange
    - Humans no longer needed to keep track of various goods/services exchange values and could instead frame the goods/services value in the context of money
- 640BC (~2380 yrs ago), the first coin was created (Lydian coin of Lydia, Anatolia). They had identifying marks which communicated two distinct things:
    1. Precious metal contents amount
    2. Authority of issue guaranteeing its contents
- The coin superceded unmarked silver for four core reasons:
    1. Its value marking removed the prior weighing process for value validation
    2. Its authoritative marking drastically lessoned counterfitting and deception of unmarked silver
    3. It standardized trust
    4. It was compact and non-perishable so traveled easily compared to large and perishable items
- The coin was an extension (and a constant advertisement) of the authority (and his power) providing it
- The modern dinar currency used in various middle eastern countries sources from a copycat lineage of authorities who initially copied the Roman's denarii coin
- Money is the universal language where strangers with differences in religion, culture, sex, age, race, and gender can still cooperate and transact
- The fundamental negative that money embodies is that the trust and convertability it provides lies in money itself and not in the individuals or their culture and values. This is only a negative as humans can develop corrupt incentives and see only coin, not the human part of a transaction.

## Imperial Visions

- Empires are often reflected upon poorly by successive populations due to their conquering and often violent nature. Regardless of this distaste, all populations as of 2500 yrs ago are not "authentic" as their cultural histories are an amalgamation of successive conquerors/empires.

## The Law of Religion

- *Religion*: a system of values and beliefs where the core belief is that they're based on superhuman laws
- The evolution of religion was Animism -> Polytheism -> Monotheism
    - *Animism*: Humans just another animal and each entity (living or not) could contain fictive/magical value
        - Human <=> Entity
    - *Polytheism*: Each god as owner of a domain. Gods are devoid of interests and biases
        - *Dualism*: A special form of polytheism where there is a "good" god and an "evil" god
        - Human <=> Gods
    - *Monotheism*: One God of all domains. God is biased toward its followers
        - Human <=> God
- The Agricultural Revolution, due to the side-effect of property/ownership, was the catalyst for successors of Animism
- Though Christians believe in a single God they have "saint of X" which is essentially a domain god of Polytheism
    - The average Christian believes in the monotheistic god, dualist devil, polythestic saints, and animist ghosts
- *Syncretism*: The world religion which refers to the embodiment of monotheistic, dualistic, polytheistic, and animistic beliefs
- Natural-law religions are instead based on the belief in natural laws vs. gods
    - Buddhism, Confucianism, Daoism, Stoicism are characterized by their disregard of gods

## The Discovery of Ignorance

- Europeans didn't have superior technology compared to the Chinese or other nations, they simply accepted ignorance. Put another way, many cultures assumed they "knew everthing" whereas Europeans admitted ignorance and thus wanted to seek and explore to find out. This "admission of ignorance" birthed the Scientific Revolution.

## The Marriage of Science and Empire 

- The Europeans' "admission of ignorance" manifested as science and the pursuit of knowledge. However, scientific endeavors we're bound tightly with Empire. Funding for exploration was required where such investment was a bet that gained knowledge could then be used to further European nations' understanding and thus control of the world and its peoples.
- Capitalism naturally birthed from this science and empire synergy
- Capitalism : Funding :: Science : Knowledge :: Empire : Application

## The Capitalist Creed

- Current U.S. banking law permits them to loan $10 for every $1 they possess
- A trust in the future combined with  credit enabled the economic engine to produce massive growth
    - Credit existed in past societies and civilizations but their belief in a future that's better than today was lacking in comparison to during the Scientific Revolution
- "The profits of production must be reinvested in production" is the core tenant of capitalism from Adam Smith's The Wealth of Nations released in 1776
- Private enterprise as opposed to nations were more agile and often paid back loans promptly. This in turn created a cycle where investing in enterprise vs. nations resulted both in greater profits, but reliable repayment of debts. Where a nation can be pulled in many directions (wars, its people, societal needs, etc) an enterprise can often remain much more focused on a goal.
- Free-market capitalism was responsible for the Atlantic Slave Trade leveraged by the Americas

## The Wheels of Industry

- Prior to the Industrial Revolution and the invention of the steam engine, the body (human and domesticated animal) was the primary energy converter of fuel (food) to motion (muscle)
- All human activities and industries require 500 exojoules of energy per yr and the sun provides that amount in 90 minutes totalling 3.7M/yr
    - We don't need more energy, we need better tools
- The Industrial Revolution can be seen as the Second Agricultural Revolution as it supercharged the efficiency and scale of agricultural production
- From steam engine to combustion engine, locomotives via tracks and later cars via roads empowered mass transportation of goods and humans 

## A Permanant Revolution

- A side-effect of machines and industrial-scale productions was the importance of time-keeping as assembly lines could literally stop production and downstream dependencies resulting in financial and efficiency losses. From the assembly line, other businesses, schools, public transportation, and society at large adopted time-keeping as an integral part of their function
