# GraphQL

## [Create Deep Dive #3: GraphQL by Nick Thomas](https://www.youtube.com/watch?v=iJULWNCaLrY)

### Overview

- Is a "level zero" API (where GitLab's REST API is currently "level four")
	- always `post`
	- single endpoint
	- single type of response (JSON) where REST's origins are a many type response (which the web didn't end up really needing/wanting)
- Optimizes for flexible queries served efficiently
- In REST, *projections*/*views* are the means to a payload subset, but they have design and maintenace issues. GraphQL's approach results in the same gain without the pain
- Due to client queries, instrumentation can make visible what is a deprecation target
- Lazy evaluation is built-in

### Technical

- "Everything is a field" (functions, types, etc.) and a field is synonomous with a calculation
- The json response is the *graph* from a `query` that defines the desired graph response
- The schema is generated :)
- Queries are a mini language (SQL-esque but lighter)
- Single query over multiple with lighter payloads:
	- GraphQLSQL - SQL *is generated* to facilitate the client's request
	- REST - often multiple queries are needed as the SQL is a static SQL query
- Value of caching goes way down as request can be so custom
	- Client's build their own caches
- `edges` abstract resource collections and a GUID for `cursor`s and these mechanisms allow rich pagination
- A `fragment` is a user-defined and named query shape for DRY queries where the fragment is `...theFragment` spread
	- This in combo with named queries allows static query shapes that take `$someVar` dynamic args, again for DRY-sake
- Subscriptions are the mechanism for websocket-like long-lived connections
- Frontend is responsible for creating static `.graphql` files

## [GraphQL & Apollo in Vue by Natalia Tepluhina](https://www.youtube.com/watch?v=-9L_1MWrjkg)

- `vue-apollo` Vue integration with GraphQL
- Apollo is a replacement to Vuex that additionally manages queries with GraphQL

## [Apollo state management in Vue application](https://dev.to/n_tepluhina/apollo-state-management-in-vue-application-8k0)

- Vue-Apollo is a Vue-specific lib faciliating GraphQL that additionally replaces Vuex
  - One or the other is preferred to ensure an SSOT and if using Apollo, might as well ditch Vuex
- Apollo's `cache` is Vuex's `data` and is the state SSOT
- Unlike Vuex a schema is used to define the data shape, subsequently this schema is used by GraphQL to facilitate jit-generated SQL queries
- `gql` is a template-literal-to-GraphQL schema and query util
  ```javascript
  import gql from 'graphql-tag';

  export const typeDefs = gql`
    type Item {
      id: ID!
      text: String!
      done: Boolean!
    }
  `;
  ```
- `Mutations` in Apollo are more verbose compared to Vuex due to the `cache.readQuery`|`writeQuery` as opposed to having the store state injected dynamically
- Too early to tell, but not convinced this is better than Vuex (though I get the SSOT argument)

## [GraphQL w/Apollo and Vue](https://hasura.io/learn/graphql/vue/introduction/)

| Requirement        | REST         | GraphQL         |
| ------------- |:-------------:|:-------------:|
| Fetching data objects              | GET              | query              |
| Writing data              | POST              | mutation              |
| Updating/deleting data              | PUT/PATCH/DELETE              | mutation              |
| Watching/subscribing to data              | -              | subscription              |

- GraphiQL is the standard GraphQL API exploration tool that provides autocomplete against the server endpoint's schema response
- Certain GraphQL fields can take *arguments* in fn call syntax. Here are both a basic and more complex example:
    ```graphql
    query {
      todos(limit: 10) {
        id
        title
      }
    }
    ```
    ```graphql
    query {
      users (limit: 1) {
        id
        name
        todos(order_by: {created_at: desc}, limit: 5) {
          id
          title
        }
      }
    }
    ```
- Variable queries are acheived by passing a `query` *and* a `variables` payload in the request.
    ```graphql
    query ($limit: Int!) {
      todos(limit: $limit) {
        id
        title
      }
    }
    ```
    ```javascript
    {
      "limit": 10
    }
    ```
- Mutations have the payload as the argument and the desired response as the field definition. Use query variables to paramaterize your request:
    ```graphql
    # The parameterized GraphQL mutation
    mutation($todo: todos_insert_input!){
      insert_todos(objects: [$todo]) {
        returning {
          id
        }
      }
    }
    ```
    ```javascript
    # As a query variable
    {
      "todo": {
        "title": "A new dynamic todo"
      }
    }
    ```
- Subscriptions allow the client to define the response shape for real-time server-pushed streaming updates by abstracting away websocket connection management
    ```graphql
    subscription {
      online_users {
        id
        last_seen
        user {
          name
        }
      }
    }
    ```
- `apollo-client` abstracts away GraphQL such that you can simply define the `queries`, `mutations`, and `subscriptions` without manually managing the request via `axios`/`fetch`/etc.
    - `ApolloClient`: Apollo GraphQL library
        - `HttpLink`: Connects to GraphQL server
        - `InMemoryCache`: Recommended client-side local cache
    - `VueApollo`: Vue-specific wrapper of Apollo to `Vue.use(VueApollo)`
    - `VueProvider`: The provider instantiated using `VueApollo` w/the `ApolloClient` instance
        - This is used to wire the `new Vue({...}.$mount('#app')` app definition w/Apollo
        - This wires each Vue instance's options API with an `apollo` identifier
            - Each definition not prefixed w/`$` is a "smart query" as it auto dispatches on the component's `mounted` lifecycle hook
            - `Smart Query`: An obj w/`loading`, `throttle`, `debounce`, and others are available automatically ;) as a UX hook
                - `<div v-if="$apollo.queries.todos.loading">Loading...</div>`
    - `gql`: util for defining a query as a string that'll map to a GraphQL query
- Mutations have an `update` callback for updating the local cache via `writeQuery` to keep app store state in sync w/server state
    - `readQuery` & `writeQuery` are the cache APIs for read/write
    ```javascript
    this.$apollo.mutate({
      mutation: ADD_TODO,
      variables: {
        todo: title,
        isPublic: isPublic
      },
      update: (cache, { data: { insert_todos } }) => {
        // Read the data from our cache for this query.
        // eslint-disable-next-line
        console.log(insert_todos);
        try {
          if (this.type === "private") {
            const data = cache.readQuery({
              query: GET_MY_TODOS
            });
            const insertedTodo = insert_todos.returning;
            data.todos.splice(0, 0, insertedTodo[0]);
            cache.writeQuery({
              query: GET_MY_TODOS,
              data
            });
          }
        } catch (e) {
          console.error(e);
        }
      },
    });
    ```
- `optimisticResponse` is used to optimisitally update the local cache as the mutation's server response time will make the UI look less responsive. This API looks ugly imo :(
    ```javascript
    this.$apollo.mutate({
      mutation: TOGGLE_TODO,
      variables: {/*...*/},
      update: () => {/*...*/},
      optimisticResponse: {
        __typename: 'Mutation',
        update_todos: {
          __typename: 'todos',
          id: todo.id,
          is_completed: !todo.is_completed,
          affected_rows: 1
        },
      }
    });
    ```
- Subscriptions require `HttpLink` to be replaced with `WebSocketLink` and use the `apollo.$subscribe` API of the `apollo` options API provided via the `ApolloProvider`

## [graphql-ruby Tutorial](https://www.howtographql.com/graphql-ruby/0-introduction/)

- The *schema definition* is where the core development occurs for a GraphQL server
    - Define types and queries/mutations for them
    - Implement *resolvers* to handle these types and their fields
    - Repeat for new requirements
- `gem` is the package manager for Ruby
- `Resolvers` "are functions that the GraphQL server uses to fetch the data for a specific query"
    - "Each field of your GraphQL types needs a corresponding resolver function"
    - The field maps to a model class and inherits from classes such that they become database records automatically
    - The mutation (the resolver) uses the instantiates or edits the model which maps to a particul GraphQL field
- `GraphQL::ExecutionError` gem is a error utility for handling malformed queries
- `SearchObject::Plugin::GraphQL` gem is a search/filter utility for advanced GraphQL search/filter resolvers
