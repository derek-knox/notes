# Traversal Algorithms

General notes about traversal algorithms

## Graph

A connected (`edge(s)`) network (`vertices`) of node(s) (`vertex`)
- A *tree* is a special but common graph type where:
    1. only one "root node"
    2. each node has only one parent (acyclic vs. cyclic)
- *Breadwise* (bread-first) and *Depthwise* (depth-first) traversal are searching algorithms often used for finding the shortest distance between a target node from a given start node
    - Lean Breadthwise when the target is expected to be *close* (edgewise not geometrically) to the starting node
    - Lean Depthwise when the target is expected to be *far* (edgewise not geometrically) from the starting node

### Breadthwise Traversal (breadth-first search)

- Can start at any node (vertex) of a tree (vertices)
- Process:
    1. *target* - determine a target node
        - `cursor` - current queue index
    2. *visit* - enqueue the target node
        - `isVisited` = node in queue and `!isProcessed`
    3. *explore* - enqueue target node's adjacent node(s)
        - `isExplored` = node in queue and `isProcessed`
    4. *process* - dequeue the target node and advance the `cursor` based on the queue
        - `isProcessed` = desired processing of node's data/state
- Rules:
    1. All the target node's adjacent nodes must be visited before advancing the queue cursor
    2. Updates must be managed by a single queue
    3. Once all nodes are visited and explored the tree has been exhausted

### Depthwise Traversal (depth-first search)

- Can start at any node (vertex) of a tree (vertices)
- Process:
    1. *target* - determine a target node
        - `cursor` - current queue index
    2. *visit* - enqueue the target node
        - `isVisited` = node in queue and `!isProcessed`
    3. *stack* - enstack the current target node
        - `isSuspended` = node in stack
    4. *explore* - loop 2-3 until at a leaf node
        - `isLeaf` = node lacks adjacent node(s)
    5. *process* - dequeue and destack the target node then advance the `cursor` based on the stack
        - `isProcessed` = desired processing of node's data/state
