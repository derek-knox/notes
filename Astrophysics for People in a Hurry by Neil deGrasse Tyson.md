# Astrophysics for People in a Hurry by Neil deGrasse Tyson

## The Greatest Story Ever Told

Four forces in nature
1. *weak* - controlling radioactive decay
1. *strong* - binding the atomic nucleus
1. *electromagnetic* - binding molecules
1. *gravity* - binding bulk matter

- `E = mc²` - recipe for how much matter your energy is worth and how much energy your matter

## On Earth as in the Heavens

Conservation laws (the amount of some measured quantify remains unchanged *no matter what*):
1. Mass and energy
1. Linear and angular momentum
1. Electric charge

- *Dark matter* refers to that which we cannot perceive outside its gravitational impact on the matter we can

## Let There be Light

- As photons cool, they walk down the spectrum
- Visible light photons are a 1/1000th as energetic since the big bang. They continue to degrade/cool and will eventually make the *cosmic microwave background* the *cosmic radiowave background*.
- Photons : Astrological-time :: Carbon dating : Geological-time
    - Spectroscopy : Physicist :: Carbon dating : Geologist

## Between the Galaxies

- Generally speaking, there are ~100B+ stars per galaxy and ~100B+ galaxies.
- Dwarf galaxies are those with smaller star cluster counts and thus are more difficult to percieve

## Dark Matter

- Through calculation dark matter exists though we cannot see it as some other force plays counter to gravity's impact on celestial bodies
    - It should really be called *dark gravity*
    - It is unique in that it doesn't interact with the particles we know of and it doesn't interact/bind with itself either (otherwise becoming matter)

## Dark Energy

- *General relativity* refers to the mathematics that describe how everything in the universe moves under the influence of gravity
    - "Matter tells space how to curve and space tells matter how to move" - John Archibald Wheeler
- *dark energy* is "virtual particles" (undiscovered particle and antiparticle pairs who collectively we can calcualate exist in the context of GR)
- mass-energy breakdown: 5% matter, 27% dark matter, 68% dark energy

## The Cosmos on the Table

- Hydrogen, Helium, and Lithium are the initial "big bang" elements where all subsequent elements were forged out of the three and subsequent atomic interation
- Carbon and oxygen are required for life as we know it, but silicon-based lifeforms are seen as technically possible

## On Being Round

- The abundance of spherical objects at galactic scale are due to the interplay of the gravitational and electromagnetic forces:
    - Gravity - as collapsing of matter in all directions
    - Electromagnetic - as molecular bonds strong enough to counter gravity
- Discs : Spheres :: Fast-rotation : Slow-rotation

## Invisible Light

- Electromagnetic spectrum: low-frequency/energy -> high-frequency/energy
    - radio
    - micro 
    - infrared
    - visible (ROYGBIV)
    - ultra-violet
    - x-ray
    - gamma ray
- Light differences refer simply to wave frequency change and map to the above spectrum
    - "Photon energy is the energy carried by a single photon. The amount of energy is directly proportional to the photon's electromagnetic frequency and thus, equivalently, is inversely proportional to the wavelength" - Wikipedia

## Between the Planets

- The sum of the celestial bodies of our solar system enclosed in a sphere encompassing the outer most planet's (Neptune's) orbit are ~one-trillionth the enclosed space
- Earth orbits the Sun at 30km/sec
- Earth is 4.6B yrs old and has thus taken 4.6B "trips" around the Sun
- If we could see magnetic fields w/the naked eye, Jupiter would look 5x the full moon in the sky
- Communications satallites orbit at 23,000mi and match Earth's revolution speed such that they "hover"
    - I *believe* Musk's Starlink tradeoff is that they're much closer to Earth (low earth orbit) at ~340mi, but require explicit counter force "boosts" to stay on the proper orbital path. Signal travel distance reduction (latency) as the "gain" for explicit force boost (thus satallite lifetime) as the "loss" in the tradeoff. 
        - They'll be intentionally deorbitted vs. having a "force boost" to maintain orbital trajectory and this is in fact the loss (lifespan)

## Exoplanet Earth

- Currently it's estimated that there are ~40B Earth-like exoplanets in our galaxy alone
- *Panspermia* refers to the notion that life on Earth (or any life-bearing planet for that matter) could have sourced from another planet through astroid impact where the astroid harbored life or elements enabling it
- The *multiverse* refers to the notion that our universe is just one of many galaxy containers that share the same scale-relationship in size and/or count as planets to stars, stars to galaxy, and galaxy to galaxies
