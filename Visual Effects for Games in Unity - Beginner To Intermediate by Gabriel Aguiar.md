# Visual Effects for Games in Unity - Beginner To Intermediate by Gabriel Aguiar

- Five VFX types:
    1. *Particle* - quad instances
    2. *Mesh* - 3D model (quad technically fits too, but differentiated from particle)
    3. *Flipbook* - texture sheet/atlas
    4. *Shader* - shader-based
    5. *Hybrid* - any combo of 2+ of the previous types
- Lifespan of an effect:
    1. *Anticipation*
    2. *Climax*
    3. *Dissipation*
- Anatomy of an effect - communicates its purpose and focal point(s) via shape, color, and contrast throughout its lifespan

## Shader Graph

In the Graph Inspector of a Shader Graph you need to select the "Active Target" to determine if the shader is for Universal (Particle System) of Visual Effect (VFX Graph)

### Nodes

- *Vertex Color* - useful for providing mesh or particle color data as an input as opposed to having an explicit Color property
- *Multiply* - useful for combining two inputs into a single output
- *Split* - useful for converting a multiplied output into its RGBA so the single value output of the *Multiply* can become two outputs (1. for Fragment "Base Color" 2. for Fragment "Alpha")
- *SampleTexture2D* - useful for sampling a texture
- *Power* - useful for amplifying colors (blacks specifically for grayscale input)
- *Time* - useful for animating (often for scrolling *UV* nodes)
- *UV* - useful when combined with the *Time* node for scrolling textures (along a billboard or mesh)

## Particle System

- Use "Sub Emitters" to nest particle systems that should spawn as a result of the original particle system (ex. crack dissipation following the same crack shape's climax)
  - Use "Renderer > Order in Layer" to ensure desired order
- Use "Emission > Rate over Time" for time-based emission or "Bursts" for instant particle spawning
- Use "Shape" to designate the spawn volume (cone, circle, hemisphere, mesh, etc.)
- Use "Velocity|Force|Color|Size|Rotation over Lifetime" as the main hooks for particle changes over time
- Use "Collision" for collision-based particles (VFX Graph can't do this and is where Particle Systems shine in comparison)

## Performance

- There are three core performance bottlenecks:
  1. Triangle count (vertex count)
     - Combat via less vs. more vertices
  2. Overdraw count (transparent rendering stacks)
     - Combat via less particles (or transparent overlapping quads/objects) drawn per frame
  3. Draw calls (batch calls)
     - Combat via texture sheets and less vs. more materials (thus less vs. more shader instances)
     - Use "Texture Sheet Animation" to ensure a particle systems uses a specific graphic from texture sheet (via the "Tiles" and a constant "Frame over Time" to match the index of the sheet cell)
     - Make sure that the "Order in Layer" value matches for each particle system that you want to have batched
     - When using a mesh effect, it cannot be batched with other particle systems so ensure its "Order in Layer" differs from the rest of the batched systems as the batch optimization breaks otherwise
- Ultimately you need to find the balance between them based on what you expect in a frame at a given time
 - Use the "Frame Debugger" and scene "Stats" to measure perf
