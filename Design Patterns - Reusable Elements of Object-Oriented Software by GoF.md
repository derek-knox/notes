# Design Patterns - Reusable Elements of Object-Oriented Software by GoF

My personal notes follow this summary from https://en.wikipedia.org/wiki/Design_Patterns:

- Behavioral: Most of these design patterns are specifically concerned with communication between objects.
  - _Chain of responsibility_ - delegates commands to a chain of processing objects.
  - _Command_ - creates objects that encapsulate actions and parameters.
  - _Interpreter_ - implements a specialized language.
  - _Iterator_ - accesses the elements of an object sequentially without exposing its underlying representation.
  - _Mediator_ - allows loose coupling between classes by being the only class that has detailed knowledge of their methods.
  - _Memento_ - provides the ability to restore an object to its previous state (undo).
  - _Observer_ - is a publish/subscribe pattern, which allows a number of observer objects to see an event.
  - _State_ - allows an object to alter its behavior when its internal state changes.
  - _Strategy_ - allows one of a family of algorithms to be selected on-the-fly at runtime.
  - _Template_ - method defines the skeleton of an algorithm as an abstract class, allowing its subclasses to provide concrete behavior.
  - _Visitor_ - separates an algorithm from an object structure by moving the hierarchy of methods into one object.
- Creational: Creational patterns are ones that create objects, rather than having to instantiate objects directly. This gives the program more flexibility in deciding which objects need to be created for a given case.
  - _Abstract factory_ - groups object factories that have a common theme.
  - _Builder_ - constructs complex objects by separating construction and representation.
  - _Factory_ - method creates objects without specifying the exact class to create.
  - _Prototype_ - creates objects by cloning an existing object.
  - _Singleton_ - restricts object creation for a class to only one instance.
- Structural: These concern class and object composition. They use inheritance to compose interfaces and define ways to compose objects to obtain new functionality.
  - _Adapter_ - allows classes with incompatible interfaces to work together by wrapping its own interface around that of an already existing class.
  - _Bridge_ - decouples an abstraction from its implementation so that the two can vary independently.
  - _Composite_ - composes zero-or-more similar objects so that they can be manipulated as one object.
  - _Decorator_ - dynamically adds/overrides behaviour in an existing method of an object.
  - _Facade_ - provides a simplified interface to a large body of code.
  - _Flyweight_ - reduces the cost of creating and manipulating a large number of similar objects.
  - _Proxy_ - provides a placeholder for another object to control access, reduce cost, and reduce complexity.

## Behavioral

### Chain of Responsibility

Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the objects and pass the request along the chain until an object handles it.

- `Handler`
  - `ConcreteHandler`

> A client issues a request and the request propagates along the chain until a `ConcreteHandler` object takes responsibility for handling it.

### Command

Encapsulates a request as an object, letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

- `Command`
  - `ConcreteCommand`
- `Invoker`
- `Receiver`

> A client creates a `ConcreteCommand` and specifies its `Receiver`. An `Invoker` stores the `ConcreteCommand` object and issues a request by calling the `ConcreteCommand`'s `Execute()` method. `ConcreteCommand` stores state to support undo and invokes operations on its `Receiver` to carry out the request.

### Interpreter

Given a language, define a representation for its grammar along with an Interpreter that uses the representation for interpretation.

- `AbstractExpression`
  - `TerminalExpression`
  - `NonterminalExpression`
- `Context`

> A client initializes the `Context` which is composed of `TerminalExpression`s and `NonterminalExpression`s. The `AbstractExpression`'s `Interpret()` method implemented by one of the two concretions is called so the `Context` can store and access the state of the Interpreter.

### Iterator

Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

- `Iterator`
  - `ConcreteIterator`
- `Aggregate`
  - `ConcreteAggregate`

> A `ConcreteIterator` keeps track of the current object in the aggregate and can compute the succeeding object in the traversal.

### Mediator

Encapsulates how a set of objects interact.

- `Mediator`
  - `ConcreteMediator`
- `Colleague`

> `Colleague`s send and receive requests from a `Mediator` object. The `Mediator` implements the cooperative behavior by routing requests between the appropriate `Colleague`s.

### Memento

Without violating encapsulation, capture and externalize an object's internal state so that the object can be restored to this state later.

- `Memento`
- `Originator`
- `Caretaker`

> A `Caretaker` requests a `Memento` from an `Originator`. The `Originator` may revert its state at a later time by requesting or retrieving the `Memento` from the `Caretaker`.

### Observer

Define a one-to-many dependency between objects so than when one object changes state, all its dependents are notified and updated automatically.

- `Subject`
  - `ConcreteSubject`
- `Observer`
  - `ConcreteObserver`

> A `ConcreteSubject` notifies its observers of its own state change. The `ConcreteObserver`s update themselves accordingly if desired.

### State

Allow an object to alter its behavior when its internal state changes.

- `Context`
- `State`
  - `ConcreteState`

> A client leverages a `Context` that encapsulates a `ConcreteState`. The `ConcreteState` will continue to use the state API where each `ConcreteState` implements its own work.

### Strategy

Define a family of algorithms, encapsulate each one, and make them interchangeable.

- `Context`
- `Strategy`
  - `ConcreteStrategy`

> A `Context` and `Strategy` receive and request communications which then delegate to a specific `ConcreteStrategy`. A client only needs to communicate with the `Context`.

### Template Method

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses.

- `AbstractClass`
  - `ConcreteClass`

A `ConcreteClass` relies on `AbstractClass` to implement the invariant steps of the algorithm.

### Visitor

Represent an operation to be performed on the elements of an object structure without changing the elements' classes.

- `ObjectStructure`
- `Visitor`
  - `ConcreteVisitor`
- `Element`
  - `ConcreteElement`

> A client leverages a `ConcreteVisitor` to traverse the `ObjectStructure` to visit each `ConcreteElement` which allows it to directly communicate with the `ConcreteVisitor`.

## Creational

### Abstract Factory

Provides an interface for creating families of related or dependent objects without specifying their concrete classes.

- `AbstractFactory`
  - `ConcreteFactory`
    - `AbstractProduct`
      - `ConcreteProduct`

> A client object may create/instantiate classes/objects through the `AbstractFactory` interface, which depending on a condition (like the operating system), leverages the correct `ConcreteFactory`. In turn creating the correct `ConcreteProduct`(s).

### Builder

Separate the construction of a complex object from its representation so the same construction process can create different representations.

- `Builder`
  - `ConcreteBuilder`
- `Product`
  - `ConcreteProduct`
- `Director`

> A `Director` leverages the `Builder` interface for object construction. The `ConcreteBuilder` assembles and returns an `AbstractProduct` or `ConcreteProduct`.

### Factory Method

Define an interface for creating an object, but let subclasses decide which class to instantiate.

- `Product`
  - `ConcreteProduct`
- `Creator`
  - `ConcreteCreator`

> Provides an interface (and potentially default objects) in `Product` and `Creator` classes. Then override respective methods in `ConcreteProduct` and `ConcreteCreator` classes.

### Prototype

Specify the kinds of objects to create using a prototypical instance and create new objects by copying this prototype.

- `Prototype`
  - `ConcretePrototype`
- `Client`

> A client object leverages a prototype interface which is implemented by a `ConcretePrototype` resulting in a cloned `Prototype` object/class.

### Singleton

Ensure a class only has one instance with a globally accessible point of access.

- `Singleton`

> A client accesses the `Singleton` directly where only a single instance's existence is enforced in a given program.

## Structural

### Adapter

Convert the interface of a class into another interface clients expect.

- `Target`
  - `Adapter`
    - `Adaptee`

> A client leverages a `Target` class/object as normal. The `Target` leverages an `Adapter` object which is responsible for translating between the `Target` interface and the `Adaptee` interface.

### Bridge

Decouple an abstraction from its implementation so that the two can vary independently.

- `Abstraction`
  - `RefinedAbstraction`
- `Implementor`
  - `ConcreteImplementor`

> A client leverages an `Abstraction` where a `RefinedAbstraction` inherits. The `Abstraction` uses an `Implementor` that then uses `ConcreteImplementor`s.

### Composite

Compose objects into tree structures to represent part-whole hierarchies. `Composite` lets clients treat individual objects and compositions of objects uniformly.

- `Component`
  - `Leaf`
  - `Composite`

> A client leverages a `Component`. `Leaf` inherits from the `Component` as does a `Composite`. `Composite` aggregates more than one `Leaf` so the same API works for individuals as well as `Composite`s (groups of `Leaf`s).

### Decorator

Attach additional responsibility to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

- `Component`
  - `ConcreteComponent`
- `Decorator`
  - `ConcreteDecorator`

> `Decorator` forwards requests to its `Component` object. It may optionally perform additional operations before and after forwarding the request.

### Facade

Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

- `Facade`
  - `Subsystems`

> Clients access the `Facade` instead of the subsystem objects directly.

### Flyweight

Use sharing to support large numbers of fine-grained objects efficiently.

- `Flyweight`
  - `ConcreteFlyweight`
  - `UnsharedConcreteFlyweight`
- `FlyweightFactory`

Clients obtain `ConcreteFlyweight`s and `UnsharedConcreteFlyweight`s via the `FlyweightFactory` which essentially pools concretions.

### Proxy

Provide a surrogate or placeholder for another object to control access to it.

- `Proxy`
  - `Subject`
  - `RealSubject`

> `Proxy` forwards requests to `RealSubject` when appropriate depending on the kind of `Proxy`. The `Subject` is the common interface between the two.
