# Musk Method

His design and manufacturing method

- https://twitter.com/TrungTPhan/status/1425476793327259651?s=20
- https://docs.google.com/document/d/153a1A4aivv26ql0zJdBtm0OMckJ42Kg5YWDB2SxJ-pw/edit

1. Make your requirements less dumb
    - Question the need of each requirement/constraint
    - Ensure each is taken responsibility of by an individual not a department
2. Seek to delete the part or process
    - Err toward deletion, if you're not adding back, you're not deleting enough to begin with
    - "In case" arguments are great deletion candidates
3. Simplify or optimize
    - The first two steps ensure you don't optimize something that shouldn't exist
4. Accelerate cycle time
    - Go faster, "debt snowball" for bottlenecks
5. Automate
    - As processes solidify, ensure they're fast, robust, and don't have regressions
