# The Innovator's Dilemma by Clayton Christensen

- There are two types of innovations:
    1. *Sustaining* - advancements that improve mainstream market share that eventually lead to a product/service that exceeds market demand
    2. *Disruptive* - advancements whose weaknesses compared to the mainstream market desires are in fact its strengths
- *Resource Dependence* is the theory where external factors (customers, investors, and other organizations) control and influence decision making within an organization much more than internal forces (execs, managers, etc.)
    - The theory also posits that internal hierarchy, process, and structure organizes in such a way as to reward sustaining innovation and not disruptive innovation
- Markets for disruptive innovations cannot be forecast so managers should focus on learning and discovery as opposed to planning and executing
- A product whose performance exceeds market demands suffers commodity-like pricing, while disruptive products that redefine the basis of competition command a premium
- Windermere Associates of SF, CA define customer buying hierarchy in the order of performance, reliability, convenience, and price. A new product that shifts the basis of competition and progression is typically a disruptive innovation/technology
- Customers have a long track record of paying premiums for convenience
- Historically, disruptive technologies involve no new technologies but instead they consist of components built around proven technologies put together in n a novel product architecture that offers customers a set of attributes not previously available 
