# Clean Architecture by Robert C. Martin

## Introduction

- Programs have two core values:
    1. Behavior
    2. Architecture
        - Bob argues that you should always fight for the architecture as the proper architecture enables flexibility and the ability to go fast. His related mantra is "to go fast, is to go well". Once an architecture lacks flexibility, the removal, additions, and changes to behavior becomes painful and bug-ridden. As such, fight for the architecture.
- *system architecture* - is defined by the boundaries that:
    1. Separate high-level policy from low-level detail
        - *high-level policy* - embodies all the business rules and procedures (core value of system)
        - *low-level details* - everything else that doesn't impact the behavior of the policy (IO, databases, servers, frameworks, protocols, etc.)
    2. Follow the Dependency Rule
        - *Dependency Rule* - dependencies cross architectural boundaries in one direction, toward more abstract entities
- an architect strives to separate policy from details and make decisions that keep options open for as long as possible

## Programming Paradigms

- There are three paradigms in programming, where each imposes discipline:
    1. *Structured* - on *direct transfer of control*
        - aka the subset of `goto` scenarios abstracted as `if/then/else` and `do/while` were OK, but unrestrained `goto` not OK
    2. *Object-oriented* - on *indirect transfer of control*
        - aka the benefits of:
            1. *encapsulation* - of data and function as an object (only the "as an object" was new compared to C)
            2. *inheritence* - ability for one data structure to masquerade as another (due to being a superset, C could only hack this)
            3. *polymorphism* - ability for an object structure to masquerade as another by abstracting away function pointer management (this was very error prone in C)
        - combined, OO allows the power of a plugin architecture to be used anywhere
    3. *Functional* - upon *assignment*
- Programming is composed of three control structures:
    1. *Sequence* - execution order
    2. *Selection* - execution forking
    3. *Iteration* - execution repetition
- Programming is a science as it lacks proofs where math has proofs. In other words, we can only *falsify* correctness of our program (via writing tests) we cannot *prove* correctness.
- OO was invented as a direct result of Dahl and Nygaard moving the function call stack frame to the heap. A side effect of this was long-living references and state that otherwise were short-lived on the stack.
- *dependency inversion* refers to source code control flow (higher levels delegating to lower levels) pointing down where the source code dependency (inheritence relationship) points up
    - The true power manifests as flexibility, swappability, and independent developability of system parts in isolation (assuming proper interface adherence)
- *event sourcing* is a functional approach where the *transactions* are stored as opposed to the state whereby transactions are iterated to sum the state
    - This is how `git` works
    - For businesses, this approach can often manifest as a glorified cron job whereby a new source state is calculated and persisted so the transactions can be flushed. This is only required as infinite storage and compute don't exist.

## Design Principles

- **S**RP: *Single Responsibility Principle*
    - each module should have only one reason to change
        - solved via: diligently ensuring modules do a constrained set of related work 
        - alternative lens: a module should be responsible to one, and only one, user/stakeholder/persona/actor
        - a function differs in that it should *do only one thing* (produce a certain type of output)
- **O**CP: *Open-Closed Principle*
    - code should be open for extension, but closed for modification
        - solved via: higher level modules calling upon implemented interface methods of subclasses such that new subclasses can be added and the existing system doesn't need to be changed
- **L**SP: *Liskov's Substitution Principle*
    - subclasses should be substitutable with other subclasses and with their superclass(es)
        - solved via: the open-closed principle, but focused on the ability to completely swap subtypes
- **I**SP: *Interface Segregation Principle*
    - concrete implementations should implement just the interfaces they need and nothing more
        - solved via: more thin interfaces and less-to-no fat ones
- **D**IP: *Dependency Inversion Principle*
    - the module hierarchy resulting in a program's control flow direction should be opposite the inheritence/dependency direction
        - solved via: source code dependencies leaning toward referring to abstractions (high-level policies) not concretions (low-level details)
        - Specific practices:
            - Don't refer to volatile concrete classes
            - Don't derive from volatile concrete classes
            - Don't override concrete functions
            - Never mention the name of anything concrete and volatile

## Component Principles

- Bob's use of *component* here is really a *package* (aka reusable distriputable)
- There are three principles of **component cohesion**:
    1. REP: *The Reuse/Release Equivalence Principle*
        - The granule of reuse is the granule of release
            -  aka public packages should encapsulate work thematically and be versioned
    2. CCP: *The Common Closure Principle*
        - Group classes/modules that change at the same times for the same reasons separately from those that change at different times for different reasons
            - aka SRP for packages to ease release, revalidation, and redeployment
    3. CRP: *The Common Reuse Principle*
        - Don't force users of a package to depend on things they don't need
            - aka ISP vibe but for other packages and behavior as opposed to solely iterface definitions
    - There is a tension between REP (group for reusers), CCP (group for maintenance), and CRP (mitigate noisy release cycle) that the architect(s) must balance
- There are three principles of **component coupling**:
    1. *The Acyclic Dependencies Principle*
        - Allow no cycles in the component dependency graph
            - aka we want a DAG (directed acyclic graph)
    2. *The Stable Dependencies Principle*
        - Depend in the direction of stability
    3. *The Stable Abstractions Principle*
        - A component should be as abstract as it is stable

## Levels

- *levels* - defined by the distance from the inputs and outputs
    - *high-level component* - is further from details and should change less often, but the change is of heavier consequence
    - *low-level component* - is closer to details and can change more often, but the change is of lesser consequence
- low-level components depend on the high-level components (typically by "pointing up" via `import`/`using`/etc. statements)

