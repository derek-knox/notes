# Practical Deep Learning for Coders 2022

## Jupyter Notebook Shortys

- `shift|ctrl+enter` run cell shortcut
- `!` prefix to code is `bash` command
- `enter` enter *edit mode*
- `esc` enter *command mode*
- `esc+a|b` add cell above|below
- `esc+d+d` delete cell
- `esc+arrows` move cell focus up|down
- `?function-name` Shows the definition and docstring for that function
- `??function-name` Shows the source code for that function
- `%run 'path/to/some/file-name.py'` run a python file

## Lesson 1

### Core fastai Objects

- `DataLoader` - fastai abstraction encapsulating:
  1. *training set*
  2. *validation set*
- `DataBlock` - fastai generic container to quickly build `Datasets` and `DataLoaders`
  - `blocks` - tuple of `DataBlock` types (first:input type::second:output type)
  - `get_items` - function to get items from source
  - `get_y` - function to get labels from items
  - `splitter` - function to split items into training and validation sets
  - `item_tfms` - function to transform items
- `Learner` - fastai abstraction for learning using `DataLoaders` and a `Model` type 
  - `visual_learner` - is a common learner type for computer vision
  - `'resnet18'` is commonly used model, but fastai supports TIMM which is a large open source model set
  - `fine_tune` - allows fastai to tweak preset weights when using a pre-trained model like `'resnet18'`

### Model Types

- *classification* - is the process of predicting a label from a set of possible labels
- *detection* - is the process of both predicting labels and the corresponding object's position
- *segmentation* - is pixelwise classification
- *regression* - is the process of predicting a numeric quantity
- *collaborative filtering* - is the process of predicting recommendation(s) using aggregate user preferences

### Anatomy

1. Classic program: inputs -> program -> output
     - ```typescript
       const output = program(inputs)
       ```
2. Machine Learning program: inputs -> model -> output
   - training
      ```typescript
      const trainingEpochs = 5
      const labels = getLabels()
      let weights = initRandomWeights()
      let loss
      for (let i = 0; i < trainingEpochs; i++) {
        output = model(inputs, weights)
        loss = cost(output, labels)
        weights = updateWeights(weights, loss)
      }
      const bakedModel = bakeWeightsInModel(model, weights)
      ```
   - testing/production
     - ```typescript
       const output = bakedModel(inputs) // weights are baked into model
       ```

### Terminology

- *tensor* - an N-dimensional array of scalers
  - 0-rank = scalar (single number special case that's not an array)
  - 1-rank = vector (1D array of numbers)
  - 2-rank = 2D matrix (2D array of numbers)
  - 3-rank = 3D matrix (3D array of numbers)
- *universal approximation theorem* - a proof of a function that can solve any problem to any level of accuracy (of which a neural network qualifies)
- *stochastic gradient descent* - is a method of minimizing a function by iteratively moving in the direction of steepest descent as defined by the negative of the gradient
- *label* - the data that we're trying to predict, such as "dog" or "cat"
  - *architecture* - the _template_ of the model that we're trying to fit; the actual mathematical function that we're passing the input data and parameters to
- *model* - the combination of the architecture with a particular set of parameters
- *parameters* - the values in the model that change what task it can do, and are updated through model training
- *fit* - update the parameters of the model such that the predictions of the model using the input data match the target labels
- *train* - a synonym for _fit_
- *pretrained model* - a model that has already been trained, generally using a large dataset, and will be fine-tuned
- *fine-tune* - update a pretrained model for a different task
- *epoch* - one complete pass through the input data
- *loss* - a measure of how good the model is, chosen to drive training via SGD
- *metric* - a measurement of how good the model is, using the validation set, chosen for human consumption
- *validation set* - a set of data held out from training, used only for measuring how good the model is
- *training set* - the data used for fitting the model; does not include any data from the validation set
- *overfitting* - training a model in such a way that it _remembers_ specific features of the input data, rather than generalizing well to data not seen during training
- *cnn* - convolutional neural network; a type of neural network that works particularly well for computer vision tasks
- *transfer learning* - is the process of using a pre-trained model to solve a new problem
  - fastai replaces the output layer when using a pre-trained model with a new randomly weighted layer(s)
  - fastai's `fine_tune(n)` defines the epochs during training of a pre-trained model where `fit` is used for training from scratch
  - mapping non-visual problems into images can be a powerful technique for transfer learning using existing ResNet-like vision models
- *model head* - refers to the layer(s) replacing the output layer of a pre-trained model

### Questions Exercise

<details>
  <summary>Toggle Q&A</summary>

1. Do you need these for deep learning?
     - Lots of math T / F
       - *A: False*
     - Lots of data T / F
       - *A: False*
     - Lots of expensive computers T / F
       - *A: False*
     - A PhD T / F
       - *A: False*
1. Name five areas where deep learning is now the best in the world.
    - *A: NLP (natural language processing), self-driving, recommendation systems (collaborative filtering), early detection in medical imagery, satellite imagery analysis* 
1. What was the name of the first device that was based on the principle of the artificial neuron?
    - *A: perceptron*
1. Based on the book of the same name, what are the requirements for parallel distributed processing (PDP)?
    - *A: the same steps that outline how a modern NN model architecture performs*
1. What were the two theoretical misunderstandings that held back the field of neural networks?
    - *A: 1. that a single hidden layer was insufficient for a NN to achieve universal approximation theorem equivalence (without simply adding a layer to make a deep network) 2. don't recall...*
    - **Correct: 1. was mostly correct, but also trigger of first AI winter 2. insight overlooked that adding layers improved performance is trigger of second AI winter**
1. What is a GPU?
    - *A: graphics processing unit, they are designed for parallel processing computer graphics and thus lend themselves extremely well to NN processing*
1. Open a notebook and execute a cell containing: `1+1`. What happens?
    - *A: the python interpreter evaluates and prints the result*
1. Follow through each cell of the stripped version of the notebook for this chapter. Before executing each cell, guess what will happen.
    - *A: ✔*
1.  Complete the Jupyter Notebook online appendix.
    - *A: ✔*
1. Why is it hard to use a traditional computer program to recognize images in a photo?
    - *A: because all the features and procedures needed to explicitly identify and then code are many, brittle, non-exhaustive, and time-consuming to create*
1. What did Samuel mean by "weight assignment"?
    - *A: the values aka parameters accompanying the inputs of a model and their respective updating over each training epoch*
1. What term do we normally use in deep learning for what Samuel called "weights"?
    - *A: parameters* 
1.  Draw a picture that summarizes Samuel's view of a machine learning model.
    - *A: ✔*
1.  Why is it hard to understand why a deep learning model makes a particular prediction?
    - *A: because the sheer magnitude of neuron count and their connection relationships are virtually impossible fo a human to comprehend* 
1. What is the name of the theorem that shows that a neural network can solve any mathematical problem to any level of accuracy?
    - *A: universal approximation theorem* 
1. What do you need in order to train a model?
    - *A: input data* 
    - **Correction: input data, architecture informed by input data, often labels, loss function, weight optimizer**
1. How could a feedback loop impact the rollout of a predictive policing model?
    - *A: bias reinforcement* 
1. Do we always have to use 224×224-pixel images with the cat recognition model?
    - *A: IIRC yes for this model due to historical reasons (likely just a simple hard-coding)* 
    - **Correction: no, the historical reasons simply informed the default**
1. What is the difference between classification and regression?
    - *A: classification predictions are a single label of an input image where regression predictions identify many labels at a per-pixel level* 
    - **Correction: regression is focused on predicting a numeric quantity (had a mental typo swapping segregation for regression)**
1. What is a validation set? What is a test set? Why do we need them?
    - *A: a validation set is used to validate a model's accuracy using inputs it hasn't seen before where a test (or training) set is accompanied by labels which allow a NN to learn. They are both needed so a model can be iterated upon before potentially converging on a set of weights that can be useful in a production setting*
    - **Correction: I incorrectly conflated the test set with the training set, you want (in order) a training set (training), validation set (training validation), and test set (evaluation)**
1. What will fastai do if you don't provide a validation set?
    - *A: IIRC it defaults to auto plucking `.2` percent of the training set as the validation set* 
1. Can we always use a random sample for a validation set? Why or why not?
    - *A: We could, but then we wouldn't be able to properly compare model versions after they are updated after training set increases*
    - **Correction: I was partially right but the goal is simply to have representative vs. random data**
1. What is overfitting? Provide an example.
    - *A: overfitting occurs during training when too many epochs with too small a training set result in a model that over optimizes against the training set resulting in a poorer production model*
1. What is a metric? How does it differ from "loss"?
    - *A: a metric is intended as a human readable value useful for gauging model improvement/degradation over time. the loss can be one such metric, but the loss is intended for the architecture at large to enable the learning steps*
1. How can pretrained models help?
    - *A: they can jump start custom models as each model already has some level of pattern recognition that can be expanded upon for a certain new task. they additionally save time and money*
1. What is the "head" of a model?
    - *A: the head refers to the layer(s) that replace the output layer of a pre-trained model when training a new model*
    - **Correction: Apparently I misread the output layer as it technically can include output-n deepest layer(s)**
1. What kinds of features do the early layers of a CNN find? How about the later layers?
    - *A: basic visual patterns like diagonal lines and color gradients whereas deeper layers can aggregate shallower feature detections into more complex feature detections such as common anatomical structures like eyes and ears*
1. Are image models only useful for photos?
    - *A: no, non-visual data can often be mapped to 2D pixel values thus resulting in image models being applicable to traditionally non-visual data problems*
1. What is an "architecture"?
    - *A: an architecture is the conceptual container encompassing the model, but also the hyperparameters*
1. What is segmentation?
    - *A: segmentation is a computer vision model type where each pixel is assigned a label unlike classification where the image at large is predicted as a single label*
1. What is `y_range` used for? When do we need it?
    - *A: IDK guessing it is related to tabular analysis*
    - **Correction: is used to limit prediction values in a range, for the 1-5 movie rating the `y_range` is `0.5- 5.5`**
1. What are "hyperparameters"?
    - *A: the meta parameters of a model architecture (ex. epochs count, initial weight assignment, activation function, cost function, nn layer counts, model type, etc.)*
1. What's the best way to avoid failures when using AI in an organization?
    - *A: minimizing training set bias, shared understanding of what it is and is not capable of, and incremental rollouts*

</details>

## Lesson 2

### Notes

- Running a model in prod (server or embedded) is simply:
  - ```python
    learner = load_learner('model.pkl')
    img = PILImage.create('some-image.jpg')
    result = learner.predict(img)
    ```
- a `.pkl` file is a serialized format of a model where pickle : serialize :: unpickle : deserialize

### Terminology

- *out-of-domain data* - refers to production data that differs too greatly from the model's training data
- *data augmentation* - refers to the process of modifying training data (rotations, filters, etc.) to increase the training set
- *synthetic data* - refers to training data that was created via data augmentation 

<details>
  <summary>Toggle Q&A</summary>

1. Provide an example of where the bear classification model might work poorly in production, due to structural or style differences in the training data.
    - *A: If the classifier's input image is top-down (assuming the training set lacked these perspectives) then the model could easily mis-classify*
1. Where do text models currently have a major deficiency?
    - *A: Don't recall*
      - **Correction: _factually correct_ responses vs. plausible sounding ones**
1. What are possible negative societal implications of text generation models?
    - *A: Believable bots on social media and propogating existing biases*
1. In situations where a model might make mistakes, and those mistakes could be harmful, what is a good alternative to automating a process?
    - *A: Keep a human in the loop. Use the model to flag such that human(s) can focus on the flagged scenarios*
1. What kind of tabular data is deep learning particularly good at?
    - *A: Don't recall*
      - **Correction: natural language, or high cardinality categorical columns**
1. What's a key downside of directly using a deep learning model for recommendation systems?
    - *A: Recommendations previously purchased/consumed*
1. What are the steps of the Drivetrain Approach?
    - *A: Define the desired outcome, determine which levers increase outcome likliehood, determine what data you can actually access, build and iterate*
1. How do the steps of the Drivetrain Approach map to a recommendation system?
    - *A: They map fine with the caveat that a non-model post-processing step is likely required for optimal UX*
1. Create an image recognition model using data you curate, and deploy it on the web.
    - *A: ✔*
1. What is `DataLoaders`?
    - *A: Abstraction encapsulating the training and validation data, data augmentation(s), the independent and dependent variables, and their related mechanisms of labeling*
1. What four things do we need to tell fastai to create `DataLoaders`?
    - *A: Training/validation split, data augmentation(s), path(s)/means to data, and means for labeling*
1. What does the `splitter` parameter to `DataBlock` do?
    - *A: Define training/validation split*
1. How do we ensure a random split always gives the same validation set?
    - *A: Using a `seed`*
1. What letters are often used to signify the independent and dependent variables?
    - *A: independent : x :: dependent : y*
1. What's the difference between the crop, pad, and squish resize approaches? When might you choose one over the others?
    - *A: crop crops, pad fills, and squish skews*
1. What is data augmentation? Why is it needed?
    - *A: The process by which training data is increased such that original data is accompanied by synthetic data (original data transformations as new data)*
1. What is the difference between `item_tfms` and `batch_tfms`?
    - *A: IIRC the former is per item (one img) and the latter per batch (many imgs)*
      - **Correction: former runs on CPU and latter on GPU**
1. What is a confusion matrix?
    - *A: A fasi.ai utility for visualizing validation accuracy in a 2D plot*
1. What does `export` save?
    - *A: The model as a `.pkl` file*
1. What is it called when we use a model for getting predictions, instead of training?
    - *A: Predicting, running, executing, inference, production, live, etc.*
1. What are IPython widgets?
    - *A: Don't recall, but believe they're UI components (component lib) for accelerating basic GUIs*
1. When might you want to use CPU for deployment? When might GPU be better?
    - *A: If parallel processing is required: GPU: yes, CPU: no*
1. What are the downsides of deploying your app to a server, instead of to a client (or edge) device such as a phone or PC?
    - *A: Latency thus real-time use-cases may not work well*
1. What are three examples of problems that could occur when rolling out a bear warning system in practice?
    - *A: Incorrect classification (false warnings), incorrect classification (weak warnings), or missed classification (no warnings)*
1. What is "out-of-domain data"?
    - *A: Data that wasn't considered in the training phase*
1. What is "domain shift"?
    - *A: Don't recall but guessing that a model overfits to a training set such that it can't properly predict when iterating on the model with updated domain training data*
      - **Correction: training data becomes stale is data at inference changes over time to no longer be representative**
1. What are the three steps in the deployment process?
    - *A: Build -> deploy -> Maintain*
      - **Correction: Manual process w/humans in loop -> Limited scope automated deploy -> Gradual expansion if previous steps work**
</details>

## Lesson 3

### Notes

- The independent variable (training data) and the dependent variable (labels) are interpretted/mapped to numbers. The independent variable numbers become the main arguments to a _loss_ function whose return value is compared to the dependent variable number. This calculation is the _loss_ value. Accompanying the main arguments are the hyperparameters _weight_ and _bias_. When executing the loss function in a loop while tweaking the _weight_ and _bias_ parameters each iteration we learn if the loss improves. Better trends to zero. The result of this process is a mathematical function with tuned _weight_ and _bias_ values. Now, new independent variables (validation and test data) can be passed to this prebaked function at inference time (production) to make predictions.

<details>
  <summary>Toggle Q&A</summary>

1. How is a grayscale image represented on a computer? How about a color image?
    - *A: Grayscale: r, g, and b are the same value, Color: r, g, and b differ*
2. How are the files and folders in the `MNIST_SAMPLE` dataset structured? Why?
    - *A:*
3. Explain how the "pixel similarity" approach to classifying digits works.
    - *A:*
4. What is a list comprehension? Create one now that selects odd numbers from a list and doubles them.
    - *A:*
5. What is a "rank-3 tensor"?
    - *A: A 3D matrix - an array of arrays or arrays*
6. What is the difference between tensor rank and shape? How do you get the rank from the shape?
    - *A:*
7. What are RMSE and L1 norm?
    - *A:*
8. How can you apply a calculation on thousands of numbers at once, many thousands of times faster than a Python loop?
    - *A:*
9. Create a 3×3 tensor or array containing the numbers from 1 to 9. Double it. Select the bottom-right four numbers.
    - *A:*
10. What is broadcasting?
    - *A: Auto-fills a tensor component of tensor A such that its shape can now match tensor B*
11. Are metrics generally calculated using the training set, or the validation set? Why?
    - *A:*
12. What is SGD?
    - *A: Stochastic gradient descent*
13. Why does SGD use mini-batches?
    - *A: To aid generalization and not get stuck in a local maxima*
14. What are the seven steps in SGD for machine learning?
    - *A:*
15. How do we initialize the weights in a model?
    - *A: Typically with random values though there are strategies for doing so in a more optimal way*
16. What is "loss"?
    - *A: The probability of missed prediction such that a loss of zero is perfect*
17. Why can't we always use a high learning rate?
    - *A:*
18. What is a "gradient"?
    - *A:*
19. Do you need to know how to calculate gradients yourself?
    - *A:*
20. Why can't we use accuracy as a loss function?
    - *A:*
21. Draw the sigmoid function. What is special about its shape?
    - *A:*
22. What is the difference between a loss function and a metric?
    - *A:*
23. What is the function to calculate new weights using a learning rate?
    - *A:*
24. What does the `DataLoader` class do?
    - *A:*
25. Write pseudocode showing the basic steps taken in each epoch for SGD.
    - *A:*
26. Create a function that, if passed two arguments `[1,2,3,4]` and `'abcd'`, returns `[(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')]`. What is special about that output data structure?
    - *A:*
27. What does `view` do in PyTorch?
    - *A:*
28. What are the "bias" parameters in a neural network? Why do we need them?
    - *A:*
29. What does the `@` operator do in Python?
    - *A:*
30. What does the `backward` method do?
    - *A:*
31. Why do we have to zero the gradients?
    - *A:*
32. What information do we have to pass to `Learner`?
    - *A:*
33. Show Python or pseudocode for the basic steps of a training loop.
    - *A:*
34. What is "ReLU"? Draw a plot of it for values from `-2` to `+2`.
    - *A:*
35. What is an "activation function"?
    - *A:*
36. What's the difference between `F.relu` and `nn.ReLU`?
    - *A:*
37. The universal approximation theorem shows that any function can be approximated as closely as needed using just one nonlinearity. So why do we normally use more?
    - *A:*
</details>

## Lesson 4

### Notes

- Though the `RandomSplitter` is useful in that it doesn't allow you to shoot yourself in the foot, it is ideal to explicitly define your training and validations sets. This is because it decreases the chances of overfitting. This happens because explictly defined training and validation samples are more likely to be representative of a wide range of examples than a random selection is.
- There are four core libs for ML/AI and data science:
  1. `NumPy`: high-level n-dimensional mathematical objects and functions
  1. `Matplotlab`: visualization plotting lib (of which `NumPy` is an extension)
  1. `pandas`: data analysis and manipulation lib specializing in numerical tables and time series
  1. `PyTorch`: ML framework useful for computer vision and NLP problems (among others)
- [Hugging Face model hub](https://huggingface.co/models) has even more models than [TIMM](https://timm.fast.ai/)
