# Ant Encounters - Interaction Networks and Colony Behavior by Deborah Gordon

## The Ant Colony as a Complex System

- 11K+ ant species where the first ants evolved ~140mya
- Colony as organism (emergent complex system) composed solely of females where one or an extreme minority are fertile (queens)
- Males live a very short life, have wings, and fly to mate with other queens before succumbing to death shortly after

## Colony Organization

- Red harvester ant individuals carry out one of four tasks at a time:
    1. Foraging - explore and collect resources (food)
    2. Patrolling - manage the mound and set the foraging direction
    3. Nest maintenance - removal of dirt and refuse from the nest
    4. Midden work - manipulate and sort refuse
- Each day an ant continues the same task as before unless external conditions warrant a shift. In these situations there seems to be a ruleset guiding changes:
    - If foraging needed, all others can switch to foraging
    - If patrolling needed, nest maintenance workers can switch to patrolling
    - If nest maintenance needed, young are recruited from inside the nest
    - Once a forager always a forager
    - Midden worker recruitment cycle is unknown but represents the smallest group by number
- Generally speaking an ant's task is a function of its age thus task maturation is generally nest maintenance (internal) followed by foraging and patrolling (external). The fact task maturation is a function of age may in fact be a function of location as an ant emerges from its pupal case deep in the nest (internal) where eventually it can reach externally.
    - "age polytheism" : fn of age :: "forage-for-work" : fn of location
- *olfaction* - sense of smell (when odors/chemicals bind to receptors)
    - It's the primary sense influencing an individual's behavior (sensing vibration is another core sense)
    - Many, but not all, ant species use "trail pheromone" to signal paths for other ants
- Each ant has ~15 glands for chemical excretion. We have a very poor understanding the "excreted meaning" of chemicals and their combos

## Interaction Networks

- Each ant colony has a unique scent profile (via cuticular hydrocarbons) such that each ant can identify/validate a nestmate
    - Each ant active in a particular task adorns a modified profile resulting in a more specific profile/id
- Ants seem to have long-term memory in that patrollers and/or foragers can retrace trails over a winter having not left the nest
- *myrmecology* - branch of entemology focused on ants

## Colony Size

- Young colonies (smaller in count) seem to have less "lazy ants" and higher frequencies of task switching compared to mature colonies
    - As a percentage, forager and nest maintenance counts don't grow linearly with colony count resulting in a portion of ants that seem to fall into a "lazy" category and/or take on a new role we are unaware of

## Relations with Neighbors

- Young colonies in proximity to mature colonies have increased survival pressure as resources are more likely to be accounted for by the mature foragers. This fact is even more apparent after a dry season where resources are typically less than average.
- If neighboring colony foragers interact both tend to use different trails the following day
    - The patrollers seem to get notified and signal new trail directions the following day
    - If a patroller encounters another patroller of a different colony, it doesn't signal the direction it traveled when it returns to the nest (assumingly the other patroller signals are used instead)
        - This raises the hypothetical, what if each patroller met a neighboring patroller? Would they forage? And if so, in what directions?

## Ant Evolution

- Ant behavior and social structures have been enfluenced by wasps
- Ants coevolved with plants where the high-level relationship is:
    - Ants attack herbivores that would eat/hurt the plant
    - Ants attack (precisely in fact) neighboring plants that would otherwise consume resources detrimental to their partner
    - In return, the ants feed on plant excrement and plants increase their size and distribution, thus providing more excrement
- In some species, fertility isn't a specific ability granted at birth. Environmental conditions and nestmate interactions can render a once sterile female fertile.
- In harvester ants (and it's assumed in many other species), a colony is comprised of at least two lineages, a male of:
    1. same lineage -> offspring are female reproductives
    2. different lineage - offspring are sterile female workers
- Queens lay eggs of each lineage type in proportion to the # of males in each lineage mated, but growth doesn't occur until colony thresholds (size and/or food supply) are met
    - Workers can apparently distinguish between the two egg types and selectively nurture them to control how and when reproductives are released

## Modeling Ant Behavior

- Behavior and ecology are tightly intertwined in modeling a colony
    - Behavior as internal influence (interaction network)
    - Ecology as external influence (environmental changes)
