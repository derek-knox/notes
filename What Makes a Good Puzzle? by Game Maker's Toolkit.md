# What Makes a Good Puzzle? by Game Maker's Toolkit

https://www.youtube.com/watch?v=zsjC6fa_YBg

### Part 1: Mechanics

- Rules (what you can do)
  - Feedback loop is a must
  - Rules with tradeoffs vs one-way are ideal
- Constraints (what you can't)
- "Cleverness" inform number and difficulty of puzzle count in game
- Temporary tools can be used to augment a mechanic or be the mechanic itself
- Goal = exit point or collectable (something else as a goal?)
- The player shouldn't be figuring out _what to do_ just _how to do it_

### Part 2: Catch

- A logical contradiction where two things are seemingly in direct conflict
  - Rethinking step sequence
  - Rethinking spatial positioning
  - Rethinking strategy
- Be careful that it doesn't feel like a "trick" instead of a revelation and make sure it's not a one-off

### Part 3: Revelation

- The "aha"/discovery/epiphany of using the mechanics in an interesting way
  - Likely a stepping stone approach for solving more intricate puzzles during game progression

### Part 4: Assumption

- Often a subtle demonstration of a mechanic's use that mis-directs the players (they assume that's how to solve)
  1. Benefit is that the player isn't overwhelmed to start the puzzle
  2. They get to form a mental model through feedback loops
  3. They likely don't solve 1st try
  4. Focuses the player on the "catch"
- Then they need to question their assumption to reach the aha

### Part 5: Presentation

- Minimalist with no extraneous elements (only with selective "assumption" lure)
- Small with very little moving parts (elegance)
  - Otherwise it's too complicated and thus unnecessarily frustrating (user can feel tricked if elements have nothing to do with puzzle solving but suggest that they do)
- Ensure timing puzzles are obviously unsolvable without speed boost or other mechanic use (not with base abilities)

### Part 6: Curve

- Build on top of puzzles prior
- Difficulty ramp cadence (macro is linear where micro provides "breathing"/difficulty-dip sections)
  1. Solutions count
  2. Steps count
  3. Options per moment
  4. Mechanic familiarity

---

# Puzzle types

Personal translation and edit of Designing the Puzzle by Bob Bates

- *Ordinary object use* - primary object function (candle light to see the key) 
- *Unusual object use* - non-primary object function (candle's heat to reshape the key)
- *Building* - configuration to create a new object (dry wax in shape of new key)
- *Information* - learn new application (back not front of key unlocks different doors)
- *Codes* - arbitrary patterns or secrets (engraving in key as code for other puzzle)
- *Cause-and-effect* - chain of events (key triggers false floor to remove obstacle)
- *Timing* - conditional result (key works at certain time periods)
- *Diversion* - lure movement (dropping key from height clangs and lures guard)
- *Sequence* - required order (key must open doors in a certain order)
- *Logic* - deducing required input (key in hot/cold room modifies its state for certain door)
- *Configuration* - required configuration (key as stick shift)
- *Riddles* - word play
- *Trial and error* - just a shitty puzzle
- *Mazes* - typically a shitty puzzle