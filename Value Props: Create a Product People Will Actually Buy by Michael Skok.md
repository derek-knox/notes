# [Value Props: Create a Product People Will Actually Buy by Michael Skok](https://www.youtube.com/watch?v=q8d9uuO1Cf4)

## Value Prop Recipe

1. For (target customer - your Minimum Viable Customer - MVS)
    - with (BLAC problem or need—ideally Blatant and Critical on the 2x2 graph)
      - Blatant
      - Latent
      - Aspirational
      - Critical
    - that is (4U)
      - Unworkable (What happens if you don't solve the problem?)
      - Unavoidable (Can't avoid and have to comply?)
      - Urgent (Urgency is relative, how does a 10x gain mitigate the pain?)
      - Underserved (Does the MVS have solutions already? Is there 10x improvement potential?)
2. Who are dissatisfied with (the current Unworkable and Underserved alternative)
3. Our product is a (3D new product - MVP)
   - Disruptive (new technology, new business model, etc.)
   - Discontinuous (something you could not do before this product)
   - Defensible (patent(s), network effects, switching costs, distribution channel, favorable location, favorable long-term or government contracts, proprietary access, etc.)
4. That provides (compelling problem-solving capability - Gain)
   - that overcomes (switching cost — Pain to Gain ratio > 10x)
5. Unlike (the unworkable product alternative that underserves the need and opportunity)

## Value Prop Statement

- For (target customer segments)
- dissatisfied with (existing solution)
- due to (key unmet needs)
- (Venture name) offers a (product category)
- that provides (key benefits of your solution)
