# OpenAI Assistants API

## Summary

The Assistants API allows the creation of purpose built Assistants. Each Assistant uses a LLM model, a system prompt, tool(s), and a conversation context to solve one or more tasks/prompts. The LLM model, system prompt, and tool(s) are statically defined at creation time but can also be overridden at runtime to further faciliate certain tasks/prompts. A Thread—which is just a unique list of Messages—facilitates task/prompt completion. The Thread's Messages can come from one or more Assistants, the developer, and/or the end user(s). The Thread and its Messages provide the ultimate context for the LLM model(s) to fulfill a given task/prompt.

## Anatomy

- **Assistant** - purpose built AI using OpenAI models, tools, files, and Threads
- **Thread** - conversation session between Assistant(s) and a user
  - Stores Messages (100K Message limit)
  - Automatically handle truncation to model's context window
  - Developer often appends messages on user's behalf
- **Message** - A message which can include text, images, and other files
  - Updates context window
  - Created by a specific Assistant (`assistant_id`), user, or the developer
- **Run** - An Assistant's invocation on a Thread resulting in appended Message(s)
  - The Assistant's configuration (model, system prompt, etc.), model, tools, and existing Thread Messages inform the response Messages
- **Run Step** - A detailed list of an Assistant's Run steps
  - Tool calls and/or Message appends result

## Overview

The API allows building AI assistants that can be embedded in apps.

- An Assistant aids or completes tasks via three primary means:
  1. **models** - LLM inference models (tradeoffs of speed and reliability of response)
     - Each model can be tuned with static or dynamic "system prompt"/`instruction`s to imbue personality and capability
  2. **tools** - augmentations enabling agency
     1. **Code Interpreter** - augments code execution ability (OpenAI hosted)
     2. **File Search** - augments knowledge from outside of the model (OpenAI hosted)
     3. **Function calling** - augments ability to interact with external tools and systems (3rd-party hosted)
  3. **files** - custom documents and files used by the File Search tool
- One or more Assistants can be used together during task solving while using an SSoT called a **Thread**
- Assistant customization occurs via:
  - **`model`** - which LLM
  - **`instructions`** - which system prompt
  - **`tools`** - which tools
    - up to 128
  - **`tool_resources`** - which files
    - up to 20 for `code_interpreter`
    - up to 10,000 @ 512mb/ea max for `file_search` using `vector_store` objects

## Function calling

### Lifecycle

![Function calling lifecycle](https://cdn.openai.com/API/docs/images/function-calling-diagram.png)

### Steps Overview

Function calling imbues an Assistant with 3rd-party defined capability (think API, SDK, and internal app method calls). This requires:
  1. Function definitions (defined as `tools` definitions within an Assistant—manual via Dashboard or dynamic via code)
```
...
tools: [
{
    ...
    function:
    {
        name: ...,
        description: ...,
        parameters: ...
    }
    ...
}
],
...
```
  2. Thread with Message(s)
    - Assistant uses `model` and knowledge of `tools` existence to infer if `tool`(s) will be of use in responding and fulfilling task
  3. Run (using above Thread)
    - A Run updates to `status: "requires_action"` and provides a `required_action` payload for the developer to parse, execute (using function signature definitions from step 1), and return to the Assistant

### Steps Detail

Returning tool call payloads to an assistant looks like:
  1. `if (run.status === "requires_action")`
  2. `await handleRequiresAction(run)`
  3. `const toolOutputs = run.required_action.submit_tool_outputs.tool_calls.map((tool) => ...)`
  4. `run = await client.beta.threads.runs.submitToolOutputsAndPoll(thread.id, run.id, { tool_outputs })`
  5. `return handleRunStatus(run)`
  6. loop back to step 1 or `if (run.status === "completed") ...`

### Structured Outputs

Feature that allows OpenAI to pre-process supplied schemas (on first request) resulting in an artifact the `model` uses to constrain to (resulting in `parameters`—and their arguments—to always match schemas)
- Though `parameters` shape matches the schema the contents are still susceptible to `model` hallucinations
- `strict: true` and `additionalProperties: false` are required to signal function calling adheres to Structured Outputs (and thus should use schemas)

## Nuggets

- `model`s that support vision can use the `purpose: vision` syntax along with an `image_url` or `image_file` id within a Message
- `token` - varying length word chunks which aggregate and impact the context window
  - each `model` has varying token limits
    - `gpt-4o` - 128K context window and 4K max output
    - `gpt-4o-2024-08-06` - 128K context window and 16K max output
- the Assistants API automatically manages context window truncation based on the `model` where a Run's lifecycle can be further customized:
  - `last_messages` - specify last `n` messages to use
  - `max_prompt_tokens` - prompt token threshold
  - `max_completion_tokens` - completion token threshold
- the Run terminates with `incomplete` (see `incomplete_details`) when `max_completion_tokens` is exceeded
- `annotations` can accompany Assistant Messages (`file_citation` or `file_path`) which a developer must enumerate to replace message content placeholders with proper file info
- a Run can have _overrides_ that differ from the original Assistant's definition used during Thread creation:
  - `assistant_id`
  - `model`
  - `instructions`
  - `tools`
- when not using `streaming` retrieve the Run object via the [Polling Helpers (`createAndPoll`, etc.)](https://github.com/openai/openai-node?tab=readme-ov-file#polling-helpers)
- when a Run is `in_progress` (and not in a terminal state) the Thread is locked
  - No new Messages
  - No new Runs
- use `response_format` for `model` responses to adhere to a structured output but use Function calling when the structured output is a part of `tools` use

## Run & Run Step Lifecycles

- Run
![OpenAI Run Lifecycle](https://cdn.openai.com/API/docs/images/diagram-run-statuses-v2.png)
- Run Step
![OpenAI Run Step Lifecycle](https://cdn.openai.com/API/docs/images/diagram-2.png)