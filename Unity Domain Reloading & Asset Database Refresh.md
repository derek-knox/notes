# Unity Domain Reloading & Asset Database Refresh

- [Unity Domain Reloading docs](https://docs.unity3d.com/6000.0/Documentation/Manual/domain-reloading.html)
- [Asset Database Refresh docs](https://docs.unity3d.com/6000.0/Documentation/Manual/AssetDatabaseRefreshing.html)

## Unity Domain Reloading

In Unity, "domain" refers to the **AppDomain**, a concept from .NET that represents a logical and isolated environment for running managed code (aka the runtime). It provides a boundary within which the code executes, and it encapsulates the runtime state, including static fields, event listeners/delegates, and loaded assemblies.

- During a domain reload:
  1. static fields are reset
  2. event listeners are cleared
  3. assemblies reload
- Time of reload increases with script number and complexity
- Regardless of enabled setting, "Unity still refreshes the scripting state when performing an automatic or manual refresh of the asset database"
- static fields and static event handlers do not reset if domain reloading is disabled
  - `[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]` is handy for fields
  - `[RuntimeInitializeOnLoadMethod]` is handy for listeners
- For Editor scripts such as custom Editor windows or Inspectors that use statics, you must use the `[InitializeOnEnterPlayMode]` attribute to reset static fields and event handlers.

## Asset Database Refresh

- *Asset Database Refresh* - reconciles the on-disk state (in your Assets folder) with the internal database maintained by Unity, ensuring that the Artifact Database is up-to-date.
- *Artifact Database* - Unity's internal system for caching and managing imported assets and their metadata.
- Asset Database batching can be optimized via `AssetDatabase.Start|StopAssetEditing`
  - This can be handy generally, but also in situations where FBX imports result in further Texture extractions which otherwise restart the refresh loop

### Asset Database Refresh Situations

1. When the Unity Editor regains focus (if you have enabled Auto-Refresh in the Preferences window)
2. When you select *Assets > Refresh* from the menu
3. When you call `AssetDatabase.Refresh` from C#

### Asset Database Refresh Steps

1. It looks for changes to the Asset files, and then updates the source Asset Database
2. It imports and compiles code-related files such as .dll, .asmdef, .asmref, .rsp, and .cs files.
3. It then reloads the domain, if Refresh was not invoked from a script.
4. It post-processes all of the Assets for the imported code-related files
5. It then imports non-code-related Assets and post-processes all the remaining imported Assets
6. It then hot reloads the Assets
   - It first stores all serializable variable values in all loaded scripts, reloads the scripts, then restores the values. All of the data stored in non-serializable variables is lost during a hot reload.
   - This affects all Editor windows and all `MonoBehaviour`s in the project. Unlike other cases of serialization, Unity serializes private fields by default when reloading, even if they don’t have the `SerializeField` attribute.
   - Unity imports assets imported by the built-in `DefaultImporter` first, and then script assets, so it does not call any script-defined PostProcessAllAssets for default assets.

### Asset Database Refresh Detailed Steps

Unity restarts the Asset Database refresh loop under the following conditions:

- If, after the import, a file that the importer used has changed on disk.
- If, in `OnPostProcessAllAssets`, you call any of the following:
    - `ForceReserializeAssets`
    - `AssetImporter.SaveAndImport`
    - Any AssetDatabase API that queues an additional Refresh, such as `MoveAsset`, `CreateAsset` and `ImportAsset`
- If the timestamp of the file being imported changes while it is being imported, the file is queued for re-import
- When an importer creates a file in the middle of an import (for example, FBX models can restart a Refresh by extracting their Textures from the model).
