#  Crafting Interpreters by Bob Nystrom

[https://craftinginterpreters.com/](https://craftinginterpreters.com/)

## A Map of the Territory

![Parts of a Language](https://craftinginterpreters.com/image/a-map-of-the-territory/mountain.png)

Overview:
- `source`: string literal source code
- `lex`: process of breaking source into series of tokens
- `token`: object including the meaningful character sequences (*lex*emes), token *type*, etc. of the language
- `tokens`: array of token objects
- `parse`: process of breaking particular token sequences into meaningful units
- `ast`: object graph resulting from parsing
- `transpile`: process of converting ast graph into new source code

Detail:

- `source`: string literal source code
- `lex`: process of breaking source into series of tokens
  - aka *scanning* that simply records substring matches against token types
  - `lexer.lex(source)`/`scanner.scan(source)` loops the source consuming substring patterns as `token` objects
  - a `var start, current = 0, line = 1;` helps us track cursor pos during the scan loop
  - during the scan, `advance()` : input :: `addToken()` : output
    - `advance()` is a look ahead util using the cursor pos helpers used to determine a full lexeme and thus `token` match -> `addToken()`
    - `match()` aka a *conditional* `advance()` is for one-or-two-char lexemes
      - ex. `case '!': addToken(match('=') ? BANG_EQUAL : BANG); break;`
    - `peek()` is like `advance()` but doesn't consume the character
    - for code comments, we can not call `addToken()` to strip them but still update the cursor pos helpers
    - *maximal munch*: ensures when multiple lexeme matches occur we take the one with the most characters as this allows reliable parsing later and thus reliable runtime execution
    - identifiers are of type `IDENTIFIER` unless a reserved word match is found instead and thus its type is that reserved type (`IF` for example)
- `token`: object including the meaningful character sequences (*lex*emes), token *type*, etc. of the language
  - Common properties:
    - `lexeme`: string literal
      - determined via regex aka the *lexical grammar*/*regular language* (the rules for how characters/alphabet get grouped into tokens)
    - `literal`: object 
    - `line`: LoC info (can be a char range)
    - `type`: token type
      - single-char (`DOT`, `MINUS`, `PLUS`, `SEMICOLON`, etc.)
      - one-or-two-char (`BANG_EQUAL`, `EQUAL`, `GREATER`, `GREATER_EQUAL`, etc.)
      - literals (`IDENTIFIER`, `STRING`, `NUMBER`, etc.)
      - keywords (`CLASS`, `IF`, `TRUE`, `RETURN`, etc. are just special `IDENTIFIER`s)
- `tokens`: array of token objects
- `parse`: process of breaking particular token sequences into meaningful units
- `ast`: object graph resulting from parsing
  - each object in the graph is an expression:
    - `name` (Binary, Grouping, Literal, Unary, etc.)
    - unique fields for each type
- `transpile`: process of converting ast graph into new source code

```javascript
var source = 'var x = 1;'           // string                                   (noun)
function lex(source) { }            // convert to tokens                        (verb)
var tokens = lex(source);           // array                                    (noun)
function parse(tokens) { }          // convert to abstract syntax tree          (verb)
var ast = parse(tokens);            // object                                   (noun)
function transpile(ast) { }         // convert to high-level language           (verb)
var newSource = transpile(ast);     // string                                   (noun)
```

## OOP vs. Functional

| OOP        | Functional         |
| ------------- |:-------------:|
| ![OOP](https://craftinginterpreters.com/image/representing-code/rows.png) | ![Functional](https://craftinginterpreters.com/image/representing-code/columns.png) |

> Each style has a certain “grain” to it. That’s what the paradigm literally means—an object-oriented language wants you to *orient* your code along the rows of types. A functional language instead encourages you to lump each column’s worth of code together into *functions*.
- The visitor design pattern in OOP is a means to get functional-like behavior where each concrete object implementation overrides an `apply(BaseVisitor visitor)` but calls `visitor.visitMyConcreteType(this)` 

## Random Notes

Psuedo Parser (Pipeline) Algorithm:

```javascript
function FilterRuby(contents) { /* cleanup the contents based on ruby and return update contents */ }
function FilterMarkdown(contents) { /* cleanup the contents based on markdown and return update contents */ }
function FilterKramdown(contents) { /* cleanup the contents based on kramdown and return update contents */ }

var filters = [
  RubyFilter,
  MarkdownFilter,
  KramdownFilter
]
var contents = rawFileString
var reducer = (filter, contents) => { filter(contents) }
var output = filters.reduce(reducer, contents)
```

parser.js
- /filters
    - FilterKramdown.js
    - FilterMarkdown.js
    - FilterRuby.js

### Additional Definitions

- `synchronization`: is the process of reporting an error without reporting cascading errors by essentially skipping past a series of tokens until the parser can properly close the current rule being processed where the initial error occurred