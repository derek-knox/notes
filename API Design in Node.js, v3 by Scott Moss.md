# API Design in Node.js, v3 by Scott Moss

## Overview

- _REST_ - An API design that allows applications to describe desired actions using:
  1. DB resources
  1. Route paths
  1. HTTP verbs
- REST is a poor solution for complex data models (Graphs) but great for basic ones that are typically flat and relational
- _Node.js_ - Useful for highly concurrent APIs that are not CPU intensive
  - Keep everything async
- _Express_ - The Node.js-based web framework (server) that manages tedious tasks like managing sockets, route matching, error handling, controller delegation, etc.
  - _Controller_ - The function that handles the client request whose signature is `(req, res, next) => {}` where `next` facilitates middleware
    - DRY route paths via:
      ```javascript
      router.route('/users')
        .get(req, res)
        .post(req, res)
      router.route('/users/:id')
        .get(req, res)
        .put(req, res)
        .delete(req, res)
      ```
- _MonboDB_ - Go-to non-relational document-based distributed database that works seamlessly with Node.js
  - _Mongoose_ - Makes creating schemas (model instructions) for your schemaless database simple and thus model generation simple
- _Insomnia/Postman_ - Popular GUIs for interacting w/server APIs
- _Middleware_ - Simply a list of functions that execute, in guaranteed order, before controllers. You can think of them as "interceptors". Common uses include:
  - authentication
  - CORS
  - request transformation
  - tracking
  - error handling
  - logging

## Directory

```
src
  config
  resources
    item
      item.controllers.js
      item.model.js
      item.router.js
    user
      user.controllers.js
      user.model.js
      user.router.js
  utils
  index.js
  server.js
```

## Models

Common model methods are:

- C: `model.create()`, `new model()`
- R: `model.find()`, `model.findOne()`, `model.findById()`,
- U: `model.update()`, `model.findOneAndUpdate()`, `model.findByIdAndUpdate()`
- D: `model.remove()`, `model.findOneAndRemove()`, `model.findByIdAndRemove()`

## Auth

- You can never truly protect an API, but auth makes it much safer and harder to bypass
- _Authentication_ - is about controlling whether an incoming request can proceed or not
- _Authorization_ - is about controlling whether an authenticated request has the correct permissions to access a resource
- _Identification_ - is determining who the requester is
- _JWT authentication_ - (JSON Web Tokens) tokens passed in every request to check auth on server
  - A bearer token strategy that allows the API to be stateless w/user auth
  - `express-jwt` is a great JWT middleware module
