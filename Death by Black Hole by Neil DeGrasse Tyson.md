# Death by Black Hole by Neil DeGrasse Tyson

## Prologue

- String theory is an attempt to reconcile the fact that quantum mechanics and Einstein's theory of gravity conflict in predicting phenomena where their rules overlap
    - A desired "theory of everything" or "universal theory" or Eric Weinstein's "geometric unity" attempt to reconcile this as well

## The Information Trap

- The *Roche Limit* refers to the radius where one celestial body's tidal force is stronger than the gravity of another body such that the latter's matter shrapnels into smaller pieces that eventually orbit the former body as a flat ring 

## Journey from the Center of the Sun

- It takes 500s for a photon to leave the Sun's surface and hit Earth, but 1M years to travel from the Sun's core to its surface
- 4H -> 1He + Energy = Sun's thermonuclear fusion reactor core
    - 15M deg K = ~27M deg F = Temp at Sun's core where hydrogen atom vibrations (heat) is so strong their natural repulsion is overcome and they collide creating helium and photons
- light-speed = 186,282mps (671Mmph)
- lightyear = distance light travels (6 trillion miles) in 1 year
    - The Universe's diameter is ~93B lightyears

## Vagabonds of the Solar System

- *AU* and *astrological unit* is the average distance from the Sun to Earth (~93M miles)
- *Differentiation* process of separting different constituents of a planetary body as a consequence of their physical or chemical properties/behavior (dense sink to center and layers form based on said properties/behavior)
- *albedo* is the proportion of light that is reflected off a surface
- *spectral analysis* - As astrophysicists understand the electromagnetic spectrum, how light absorbs and reflects in different materials, they're able to deduce the material makeup of distant objects in space
- *Asteroids* are metal and rocky where *comets* are typically frozen gases, frozen water, dust, and rocky. The former form near a star where the latter form far from a star. Over a long period they are one in the same, just in different states due to their composition.
- *Aerogel* is a synthetic porous ultralight material (solid w/low density and conductivity) whose liquid portion has been replaced with a gas such that the gel structure remains intact. It has been used to collect particle samples of surrounding comets.

## The Five Points of Lagrange

- Orbital points of two large near co-orbiting bodies where gravitational forces cancel out such that a small object can stay in equilibrium relative to the gravity of each large body
    - This equilibrium makes these points great "fuel stops" in space travel
    - L1, L2, L3 are unstable (lie along a line connecting the masses) where L4 and L5 are stable (apex of equilateral trianges) 

## Antimatter Matters

- Particles -> Antiparticles are essentially clones w/opposite charge
    - electron -> positron
    - proton -> antiproton
    - neutron -> antineutron
        - neutron: 3 quarks @ (-1/3, -1/3, +2/3) charge
        - antineutron: 3 quarks @ (1/3, 1/3, -2/3) charge
- `E = mc²` recipe for how much matter your energy is worth and how much energy your matter
    - "If a pair of gamma rays has sufficiently high energy they can interact and spontanously transform into an electron-positron pair thus converting a lot of energy into a little bit of matter"
        - E -> M: the gamma ray kicked out an electron to create an electron and an electron "hole"
        - M -> E: when a particle and antiparticle collide, they annihilate by refilling the hole resulting in gamma rays
- Strong magnetic fields are how nuclear physicists prevent particle and antiparticle collisions during experiment

## The Importance of Being Constant

- Einstein's *theory of special relativity* is an expansion of Newton's gravitational law where Newton assumed the mass in `F = ma` was constant. Einstein's theory accounts for the fact that a mass near the speed of light increases its resistance to acceleration that manifests as an increase in mass
- The force of gravity is directy proportional to the mass of obj1 times the mass of obj2 and inversely proportional to the square of their distance
    - If the strength of gravitational force is `F` at distance `d`, it is `1/4F` at `2d` and `1/9F` at `3d`
- An object's temperature is a measure of its atomic and molecular vibrations (kinetic energy) averaged
    - Photons of varying frequency emit and when they enter the visible spectrum we percieve them as light
    - The electromagnetic spectrum again refers to a photon's "jiggle" (amplitude of 360deg oscillation) which then maps to a particular temperature that can be measured
- Planck's constant `h` refers to energy in direct proportion to its frequency
    - *quanta* are the indivisible units comprising *quantized* energy
- *uncertainty principle* refers to the trade-offs in measurement of the below physcial attributes where measurement precision gain of one results in precision loss of the other. This is most noticible in quantum physics as Planck's constant `h` is so small.
    1. location <=> speed
    1. energy <=> time

## Going Ballistic

- Low-Earth orbit (LEO) is 18Kmph perpendicular to Earth @ 1.5hr/orbit

## Forged in the Stars

- *Supernova*s occur in 1/1000 stars that are 7x the mass of our Sun. Unlike the common star that degrades "peacefully" through gradual gas shedding, a supernova explodes due to internal chemical chain reactions.
- We are stardust because neutrons can join a nucleus (due to neutral charge) but then due to instability can (due the weak force) split into the below which results in a cascade of atomic creation. Along this cascade, energy/temperature increases occur that overcome repulsion forces causing further temperature increases and particle fusion. This walk goes from hydrogen to iron (as energy is now only absorbed vs. emitted) until the star explodes in a supernova. From here many new elements that were forged are now spat into space and further attract and repel due to gravity and electromagnetism resulting in larger units of matter.
    - beta-minus decay: proton, electron, and antineutrino
    - beta-plus decay: proton, positron, and neutrino

## Send in the Clouds

- Pockets of gas in a star don't exist as a uniform distrubution of particle and atom collections. As such, these pockets can "escape" a star and enter an orbit. These pockets can become planets and other celestial matter, birthing a solar system. Eventually such planets can birth the most complex chemistry, biology.

## Goldilocks and the Three Planets

- *Drake equation* refers to an estimated number of civilizations in a galaxy. Roughly:
    1. `((((numStarsInGalaxy * fractionOfStarsWithPlanets) * fractionOfPlanetsInHabitableZone) * fractionOfPlanetsWithLife) * fractionOfPlanetsWithIntelligentLife) * fractionWithInterstellarCommunicationTechnology` against a star formation rate and expected lifetime of such interstellar communication technology `= ~numCivilizationsInGalaxy`
- The thermophiles of the extremophiles suggest that Earth without its Sun would still contain life as the energy comes from the core and not the star. Under this premise, life could be much more abundant in our solar system and the universe.
    - Thus Drake's `fractionOfPlanetsInHabitableZone` could be `100%` as the qualification of a habitable zone is mute

## Living Space

- In order, the most abundant atoms are hydrogen, helium, oxygen, carbon, and nitrogen
    - As temperatures drop, they begin to bind into molecules:
        - Two atoms:
            - carbon monoxide
            - hydrogen
        - Three atoms:
            - water
            - carbon dioxide
        - Four atoms:
            - ammonia
- 75% of "molecular species" contain carbon (aka of all the molecular combos, 75% contain carbon)
- Notable complex molecules for life as we know it include:
    - adenine (one of the bases of DNA)
    - glycine (protien precursor)
    - glycoaldehyde (carbohydrate)
- Extremophiles live in extreme conditions such as nuclear dump sites, acid-laden geysers, iron-saturated acidic rivers, chemical-belching vents on the ocean floor, submarine volcanoes, permafrost, slag heaps, etc and thus the inital thought that life requires water (to survive, we still believe its required to manifest) is less strong.

## Life in the Universe

- Hydrogen, oxygen, and carbon comprise 95% of the human body and all lifeforms on Earth
    - The carbon atom is special because it readily bonds with many other atoms including itself. Organic chemistry is the study of non-basic carbon-based molecular binding.
    - These three most abundant chemically active atoms in the universe also happen to be the three most active life ingredients

## Coming Attractions

- Life formed within a 200M yr time period on Earth
    - `4.6Byr (Earth's age) - 600Myr (Earth too hot for carbon-based chemical bonds) - 3.8Byr (Oldest fossil's age) = 200Myr`

## Death by Black Hole

- *Tidal force* is the gravitational difference between the nearest and farthest points of matter on the object relative to the other body
    - For Earth, the Sun's gravity and the Moon's gravity make Earth's water-as-one-unit bulge toward each. The Earth then rotates within and thus one side is "entering" and the other "exiting" the opposing bulges.
- A *black hole* is a region of space with a gravitational force so great not even light can escape
    - A black hole's mass and diameter are linearly related where smaller diameter black holes have a stronger tidal force. This means matter rips apart the atoms of matter at the near-end before matter at the far-end sooner thus literally deconstructing matter into its subatomic particles before being consumed. In larger black holes this tidal force is too weak so the atomic bonds remain as the object as a whole gets consumed vs being "spaghettified".
    - Black hole's are the center of galaxies and typically are 1Bx the Sun's mass with a size (spherically) that of our solar system
- *Event horizon* is the spherical volume within a black hole from which light cannot escape and thus we cannot visualize     

