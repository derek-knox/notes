# Understanding the SSH Encryption and Connection Process by Justin Ellingwood

*Notes from https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process*

## Symmetrical Encryption

Single key approach where the private key is used to both encrypt and decrypt messages
  - multi-purpose key
  - multi-directional
  - nothing is public

```javascript
// server
function scramble(message, privateKey) { /* return scrambled message */ }

var privateKey = '...' // shared private key
var message = 'hello world'
var encryptedMessage = scramble(message, privateKey)
server.post(encryptedMessage)

// client
function unscramble(encryptedMessage, privateKey) { /* return unscrambled message */ }

var privateKey = '...' // shared private key
client.onPost(encryptedMessage => {
  var message = unscramble(encryptedMessage, privateKey))
  console.log(message)
}
```

## Asymmetrical Encryption

Dual key approach where one public key and one private key facilitates encryption where just the private key decrypts
  - two single-purpose keys
  - uni-directional
  - only public key is public

```javascript
// server
import { generateKeys } from 'os' // ssh-keygen for example

function unscramble(encryptedMessage, privateKey, publicKey) { /* return scrambled message  */ }

var { publicKey, privateKey } = generateKeys()
server.post(publicKey) // only publicKey shared

// ... awaiting client response

server.onPost(encryptedMessage => {
  var message = unscramble(encryptedMessage, privateKey, publicKey)
  console.log(message)
})

// client
function scramble(message, publicKey) { /* return scrambled message */ }

var message = 'hello world'
client.onPost(publicKey => {
  var enryptedMessage = scramble(message, publicKey)
  client.post(encryptedMessage)
})
```

## SSH

1. Both the server and client each use asymmetrical encryption to generate their own temporary `privateKey` and `publicKey` pairs.
2. Each exchanges only their `publickKey` so that the other can encrypt messages.
3. A private session of communication is then setup after both have decrypted the other's message containing a new shared private key. The private session is now using symmetrical encryption.

### SSH key-based authentication

1. Client generates `privateKey` and `publicKey` pair
2. Client uploads `publicKey` to remote server (via https site authentication for example)
3. Server stores it in `~/.ssh/authorized_keys`
4. After initial SSH setup (temp asymmetric to symmetric encripted session) the server encrypts a challenge message using the *uploaded* `publicKey`
5. Client properly decrypts since it has the original `privateKey` used to decrypt using the uploaded `publicKey`
6. Client uses private session to confirm to the server it is the owner of the `privateKey` associated with the `publicKey`
 
## Hashing

SSH uses cryptographic hash functions for creating succint *signature*s that:
- Are never meant to be reversed
- Cannot influence predictably
- Practically unique
- Using the same hashing function and message should produce the same hash

Message authentication codes (MAC) algorithms confirm an `encryptedMessage` is intact and unmodified. As a result a MAC is part of the communication payload (outside symmetrically encrypted portion) to verify packet integrity. The MAC is calculated from the:
- Symmetrical shared secret
- Packete sequence number of the message
- Actual message content

### SSH Stages

An SSH session is established in two stages:
1. Negotiation - Agree upon and establish encryption to protect future communication
2. Authentication - Authenticate user so server can conditionally grant access
 
#### Negotiation

*Steps copied from above link*

1. Both parties agree on a large prime number, which will serve as a seed value.
1. Both parties agree on an encryption generator (typically AES), which will be used to manipulate the values in a predefined way.
1. Independently, each party comes up with another prime number which is kept secret from the other party. This number is used as the private key for this interaction (different than the private SSH key used for authentication).
1. The generated private key, the encryption generator, and the shared prime number are used to generate a public key that is derived from the private key, but which can be shared with the other party.
1. Both participants then exchange their generated public keys.
1. The receiving entity uses their own private key, the other party’s public key, and the original shared prime number to compute a shared secret key. Although this is independently computed by each party, using opposite private and public keys, it will result in the same shared secret key.
1. The shared secret is then used to encrypt all communication that follows.
 
#### Authentication

*Steps copied from above link*

1. The client begins by sending an ID for the key pair it would like to authenticate with to the server.
1. The server check’s the authorized_keys file of the account that the client is attempting to log into for the key ID.
1. If a public key with matching ID is found in the file, the server generates a random number and uses the public key to encrypt the number.
1. The server sends the client this encrypted message.
1. If the client actually has the associated private key, it will be able to decrypt the message using that key, revealing the original number.
1. The client combines the decrypted number with the shared session key that is being used to encrypt the communication, and calculates the MD5 hash of this value.
1. The client then sends this MD5 hash back to the server as an answer to the encrypted number message.
1. The server uses the same shared session key and the original number that it sent to the client to calculate the MD5 value on its own. It compares its own calculation to the one that the client sent back. If these two values match, it proves that the client was in possession of the private key and the client is authenticated.

