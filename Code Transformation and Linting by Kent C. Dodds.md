# Code Transformation and Linting by Kent C. Dodds

[https://frontendmasters.com/courses/linting-asts/](https://frontendmasters.com/courses/linting-asts/)

- Codemods can automate transpilation upgrades of framework versions, API refactors, language versions, etc.
  - As opposed to creating custom regex to update a codebase, a codemod may exist to automate the process
- JavaScript AST visualizers
  - Tree & Json = https://astexplorer.net/
  - Node Graph = https://resources.jointjs.com/demos/javascript-ast
    - The "Transform" toggle allows real-time transpiling/conversion of the source to a target output
- When creating an eslint rule (use astexplorer.net) you simply define a method name that matches the AST node types of interest. This way you query via their API (and the Visitor pattern) without manually having to do so. For example:

```javascript
//...
create(context) {
  return {
    IfStatement(node) { // `IfStatement` allows you the hook into the AST for all nodes of that type
      console.log(node);
    }
  }
}
```

- eslint plugin dev uses a `node` object where babel dev uses a rich `path` object that has a `node` object
- `jscodeshift` is a CLI tool for running codemods [https://github.com/facebook/jscodeshift](https://github.com/facebook/jscodeshift) for a rich and automated find/replace/refactor over a codebase, files, or file
- ASTs enable mass transformation with greater ease and power
