# Vue Test Utils

Goals:
- mount components in isolation, mocking the necessary inputs (props, injections and user events)
- asserting the outputs (render result, emitted custom events)

Notes:
- `const wrapper = mount(TheComponent)` provides a wrapper w/API to ease testing
  - `const wrapper = shallowMount(TheComponent)` is preferred over `mount` as it doesn't render child components
- > Anytime you make a change (in computed, data, vuex state, etc) which updates the DOM (ex. show a component from v-if or display dynamic text), you should await the nextTick function before running the assertion.
  - Use `.trigger` in `async` functions
- > W recommend writing tests that assert your component's public interface, and treat its internals as a black box.
- Not how, just what the input and output are
- Use the `propsData` when `mount`|`shallowMount`ing and/or `wrapper.setData`|`setProps` to update state mid-test
- Use `.spy` functions to assert function calls