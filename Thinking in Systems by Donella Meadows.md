# Thinking in Systems by Donella Meadows

## The Basics

- A *system* is an interconnected set of elements that are coherently organized in a way that achieves something
- A *system* consists of:
    1. elements 
    2. interconnectedness
    3. function/purpose
- Purposes are deduced from behavior, not from rhetoric or stated goals
- An important function of almost any system is to ensure its own perpetuation
- A *stock* is the foundation of any system. Stocks are the elements you can see, feel, count, or measure at any given time
- Stocks change over time through the actions of a *flow*. Thus a stock is the present memory of the history of changing flows within the system.
- The *dynamics* of stocks and flows is their behavior over time
- *Dynamic equilibrium* is when the inflow of a systems offsets the outflow so the stock remains constant
- Basic principles of complicated systems:
    1. stock increases if sum of inflows exceeds sum of outflows
    2. stock decreases if sum of outflows exceeds sum of inflows
    3. stock at dynamic equilibrium when sum of outflows equals sum of inflows
- A stock takes time to change because flows take time to flow
- A *feedback loop* is a closed chain of casual connections from a stock, through a set of decisions or rules or physical laws or actions that are dependent on the level of the stock, and back again through the flow to change the stock
- *Balancing feedback loops* are equilibrating or goal-seeking structures in systems and are both sources of stability and resistance to change
- *Reinforcing feedback loops* are self-enhancing, leading to exponential growth or to runaway collapse over time. They are found whenever a stock has the capacity to reinforce or reproduce itself
- *Doubling time* is the time it takes for an exponentially growing stock to double in size, which is approximately `70 / growthRate`
    ex. $100 @ 7% interest/yr = 70/7 = 10 yrs to double

## Systems Zoo

- Complex behaviors of systems often arise as the relative strengths of feedback loops shift, causing first one loop then another to dominate behavior. This is called *shifting dominance*
- Questions for testing the value of a model:
    1. Are the driving factors likely to unfold this way?
    2. If they did, would the system react this way?
    3. What is driving the driving factors?
- Systems with similar feedback structures produce similar dynamic behaviors, even if the outward appearance of these systems is completely dissimilar (stock w/one reinforcing loop and one balancing loop = populations and industrial economy)
- Delays are pervasive in systems and they are strong determinants of behavior. Changing the length of a delay may (or may not, depending on the type of delay and the relative lengths of other delays) make a large change in the behavior of a system
- Balancing feedback loops with delays are inherently oscillatory (economics)
- Non-renewable resources are stock-limited
- Renewable resources are flow-limited

## Why Systems Work so Well

- Systems work well due to three characteristics:
    1. *resilience* - the ability to recover, restore, and repair themselves
    2. *self-organization* - the ability to structure themselves or create structure, to learn, diversify, and complexify
    3. *hierarchy* - the ability for subsystems to aggregate into large subsystems continuously

## Why Systems Surprise
 
- Everything we think we know is a model and are never the "real" world
- Our models usually have strong congruence with the "real" world
- We make mistakes and are surprised still as models still fall short
- At any given time, the input that is most important is the one that is most limiting

## System Traps and Opportunities

*Policy Resistance*

- Trap: When various actors pull a system state toward various goals, the result can be policy resistance
- Opportunity: Let go. Seek mutually satisfactory ways for all actor goals to be realized or redefine more important goals that everyone can rally behind

*Tragedy of Commons*

- Trap: Commonly shared resource where each user benefits but shares the cost of abuse
- Opportunity: Educate and exhort, privatize resources so each user feels the direct consequence of abuse, or regulate access

*Drift to Low Performance*

- Trap: Allowing performance standards to weaken under poor performance
- Opportunity: Keep standards absolute or enhance by moving the carrot forward

*Escalation*

- Trap: One-upping leading to eventual collapse as exponential growth can't maintain forever 
- Opportunity: Don't participate, refuse to compete, or negotiate a new system w/balancing loops

*Success to the Successful*

- Trap: If winners are systematically rewarded with the means to win again uninhibited, winners eventually take all and eliminate losers
- Opportunity: Diversification, get out and start a new game, anti-trust practices

*Shifting the Burden to the Intervener*

- Trap: A solution to a systemic problem reduces/disguises the symptoms, but does nothing to solve the underlying problem
- Opportunity: Don't get in, take focus off short-term in favor of long-term, or enhance the system's own ability to solve its problems

*Rule Beating*

- Trap: Perverse behavior masked as rule-following but actually distorts the system instead
- Opportunity: Design, or redesign, rules to release creativity not in direction of beating them, but of achieving the purpose of the rules

*Seeking the Wrong Goal*

- Trap: Inaccurate or incomplete goals produce unintended or undesired results
- Opportunity: Don't confuse effort with result or you'll get a system that produces effort, not result

## Places to Intervene in a System

Generally, greater leverage is attained when information and control changes are possible when compared to physical changes.

There are twelve targets ranking from most (1) to least (12) impactful in leverage:

1. *Transcending Paradigms* - Recognizing there are multiple paradigms where adoption of one has tradeoffs compared to others
2. *Paradigms* - The mind-set and belief system that influences the system goals, rules, delays, parameters, etc. What determines the goal(s) to begin with?
3. *Goals* - The meta-rule(s) for which lower-ranking leverage point optimizations are informed by
4. *Self-organization* - CRUD for stocks/entities in a system
5. *Rules* - Incentives, punishments, and constraints. To understand system malfunction(s), seek to understand the rules and who makes/influences them
6. *Information flows* - Visibility and exposure frequency have great impact. Typically, the masses are in favor and the elites anti.
7. *Reinforcing feedback loops* - Sources of growth, explosion, erosion, and collapse
8. *Balancing feedback loops* - Feedback strength/robustness relative to correction impact
9. *Delays* - Time lengths relative to system change rates. When mutable they can have a great impact, but often they're natural and immutable. In software, these are high leverage points if not constrained by real-world modeling requirements
10. *Stock-and-flow-structures* - Physical systems and their intersection nodes (when optimized) can greatly impact stock and flow. In physical systems, it's important to get this right in the design phase as physical refactoring is costly and slow.
11. *Buffers* - The sizes of stabilizing stocks relative to their flows
12. *Numbers* - Constants and parameters counterintuitively make less of an impact than intuition suggests unless they impact one of the higher-ranking leverage points

## Living in a World of Systems

Approaches for system understanding:
- *Take time to observe* and verify as heresay and/or existing data can be wrong where the truth is the opposite of common knowledge. Only after observation, can more meaninful intervention and/or solution proposal be considered (without observation and understanding solutions can solve the wrong problems to begin with).
- *Expose your mental models* and thoughts in tangile form (visual, written, verbal, etc). Only from here can shared understanding be truly met. Seek many, not your preferred, solution as there is almost always more than one. The optimal one changes as the particular tradeoffs are optimized for. This approach additionally helps you detach your ideas from your identity.
- *Seek system transparency*. Systems degrade typically as a result of biased, late, incorrect, or missing information. Be very weary of those that distort, delay, and withold information.
- *Use language respectfully*. Aim to use language as concretely, truthfully, and meaninfully as possible.
- *Pay attention to quality, not just quantity*
- *Make feedback policies for feedback systems* so learning is an inherent part of the system to feed improvement
- *Locate responsibility* and identify triggers within the system as a means to err toward internal tweaking for self-correction as opposed to introducing changes sourced externally
- *Embrace error and celebrate complexity* as the world and ourselves are a product of both. It's a mistake to ignore this reality.
- *Expand time horizons* as many of our perceived problems are a direct result of systems we've created that incentivize short-term thinking and behavior
- *Expand caring and maintain goodness* to help ensure a more prosperous future
