## Chemistry Basics

- *matter* - anything having mass that occupies space
  - all matter is composed of atoms
- *atom* - a composition of particles (unit building block of matter)
  - *particle* - portion of matter with a positive, negative, or neutral electric charge
    - *proton* - positive electrically charged particle
    - *neutron* - neutral electrically charged particle
    - *electron* - negative electrically charged particle
  - atoms with a distinct proton count are of a particular type
    - *neutral* - a particular atom type of equal proton and electron count (normal)
    - *isotope* - a particular atom type where neutron count varies (version)
    - *ion* - a particular atom type where electron count varies (charge)
  - anatomy
    - *nucleus* - proton and neutron cluster
    - *shell* - electron orbit of nucleus
      - *valence shell* - outermost electron shell
- *element* - substance composed of a particular type of atom
- *molecule* - composition of two or more atoms (not necessarily different atom types)
- *compound* - composition of two or more elements (two distinct element types)
- common bonds
  - *ionic* - bonds between metals and non-metals (tranferred electrons)
  - *covalent* - bonds between non-metals (shared electrons)
- *periodic table of elements* 
  - the atomic number of each element defines its atom's proton/electron count
  - row - each of seven rows defines the electron shell count for each element in the row where the amount of elements in each row signifies each shell's maximum electron capacity
  - column - grouping/family of elements sharing similar physical or chemical characteristics of the valance shell

## Biology Basics

- *cell* - The basic structural and functional unit of all known living organisms. Building block of life.
    - function: contains the hereditary information (recipes) necessary for regulating cell functions and for transmitting information to the next generation of cells
- *gene* - A hereditary unit consisting of a sequence of DNA, occupying a specific location on a chromosome. (protein recipe)
    - function: determines a particular characteristic in an organism (when genes undergo mutations, their DNA sequence changes)
- *chromosome* - An organized strand of DNA and associated proteins
    - function:  carries the genes and functions in the transmission of hereditary information
- *molecule* - The smallest particle of a substance that retains the chemical and physical properties of the substance 
    - function: //
- *enzyme*  - A protein that catalyzes.
    - function: To catalyze (modifies the rate of chemical reaction)
        1.	Inducible enzymes [+] – formed only in the presence of their substrates in the medium (expressed when induced) 
        2.	Constitutive enzymes [-] – formed regardless of the nature of the medium (always expressed)
- *DNA* - A nucleic acid that carries the genetic information in the cell, it is capable of self-replication and synthesis of RNA.
    - function: used in the development and functioning of all known living organisms and some viruses
- *RNA* - A polymeric ingredient of all living cells and many viruses. It consists of a long, usually single-stranded chain of alternating phosphate and ribose units
    - function: involved in protein synthesis (process where cells build proteins) and sometimes in the transmission of genetic information

## Lac Operon Terminology

- *lac operon* - The region of DNA which, in order, consists of a regulatory gene, a promoter, an operator, three  structural genes, and the operon’s terminator.
    - function: facilitate gene expression by regulating the transcription of the structural genes into one polycistronic mRNA
- *polycistronic mRNA* - A single mRNA molecule that embodies the genetic information of several genes
    - function: encodes for several proteins
- *regulatory gene* - A gene involved in controlling the expression of one or more other genes
    - function: encodes for the primary structure of the repressor protein
- *structural genes* - The genes that are co-regulated by the operon
    - function: encodes for any RNA or protein product other than a regulatory factor
- *promoter* - A segment of DNA that is a binding site which aids the transcription of a particular gene
    - function: indicates which genes should be used for messenger RNA creation and which proteins the cell manufactures.
- *operator* - A segment of DNA that is a binding site which a repressor or activator binds to.
    - function: the repressor protein physically obstructs the RNA polymerase from transcribing the genes
- *repressor* - A DNA-binding protein that binds to an operator
    - function: hinders transcription of an operon and the enzymes for which the operon encodes
- *activator* - A DNA-binding protein that binds to an operator
    - function: promotes transcription of an operon and the enzymes for which the operon encodes

### Lac Operon Process Summaries

Lac Operon encodes three enzymes (proteins):
  1. β-galactosidase (Beta-galactosidase)
  2. β-galactoside permease (Permease)
  3. Galactoside O-acetyltransferase (Transacetylase)

### Basic Principles of Genetics

  1. *Complementation* — If you have a mutation, can you ‘complement’ it with a good copy of the gene
  2. *Dominant and Recessive Effects* - If you have two mutations, which is dominant, which is recessive.  EX:  LacIs > LacI > LacI-  (the arrows denote order of dominance, LacIs ‘trumps’ any other repressor state if operators are normal.  But LacOc is dominant over LacIs. 
  3. *Trans and Cis Activity* - Which components cannot diffuse (operators are ‘cis’ since they are just a DNA sequence and only influence genes they are adjacent to, an trans like LacI which is a protein and can diffuse in the cell and bind to any good LacO+)
