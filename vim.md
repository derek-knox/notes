# Vim

- `:cq` to exit vim and not continue (exit out of rebase for example)
- multi-line word replacement
  1. `ctrl+v`
  2. arrows movement for selection
  3. `c` (deletes selection)
  4. type replacement
  5. `esc` (edited)
