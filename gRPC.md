# gRPC

- Google's specific implementation of an RPC (remote procedure call) pattern
- Invented to abstract away specific client library implementations (having to code in n number of languages) such that a single abstract schema standard can be used to generate the client libraries in the languages of choice
- The RPC pattern generally is useful when async cross-process|machine communication is needed
  - It basically standardizes a message format (shape) that can be posted by a client and parsed by a server. This is important because cross-processes|machines don't share the same memory space and thus can't directly call methods. Instead, a command pattern must be used that *describes the intent* such that a client *delegates the execution* to a server to do work on its behalf. To the client (from a DX perspective), it's as if the server is running in the same memory space but with async execution.
- The aforementioned *message format* in gRPC is protocol buffers. The format is language independent

# Implementation

- Define `message`s (request and response payloads) and `service`s (rpc function definitions) as language-independent Protocol Buffers (aka `.proto` files)
  - These are the SSoT from which language-specific client and server code is generated
  - My noob gut call is that these `.proto` files simply define a map between desired types (`string`, `number`, etc) and transmission-friendly types (integers -> binary). The generated code then does the serialization and deserialization on your behalf so we just have to work with the desired types in the `.proto`s
    - I imagine there are tools to generate these `.proto` files too such that we don't manually have to define the mapped integers
- Terminology-wise, a `stub` is a client and a `service` is a server. `service`s can also have `stub`s. The main goal is simply to identify who is a sender and who is a receiver in a particular proto req/res cycle
- Four types of API in gRPC:
    1. *Unary* - classic 1 client req -> 1 server res
    2. *Server Streaming* - 1 client req -> n server res
    3. *Client Streaming* - n client req -> 1 server res
    4. *Bi-directional Streaming* - n client req -> n server res
    
    ```proto
    service GreetService {
      // Unary
      rpc Greet(GreetRequest) return (GreetResponse) {};

      // Streaming Server
      rpc GreetManyTimes(GreetManyTimesRequest) returns (stream GreetManyTimesResponse) {};

      // Streaming Client
      rpc LongGreet(stream LongGreetRequest) returns (LongGreetResponse) {};

      // Bi-directional Streaming
      rpc GreatEveryone(stream GreetEveryoneRequest) return (stream GreetEveryoneResponse) {};
    }
    ```
 
